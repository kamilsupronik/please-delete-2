gma.mpattr <- function(stdt=g.date.idx( ,T,-1, cal="mnth.beg"), endt=g.date.idx( ,T,-1)) {
    g.plot.init()
    mtext(g.p("MP Performance Attribution"), line=0.5, cex=1.5)
    mtext(paste(g.fmt(c(stdt,endt), "d"), collapse=" - "), line=-.5, cex=1)
    Nd <- length(stdt %usd8% endt)
    g.plot.disclaimer()

    itms <- c("rtn","hld","trd","car")
    kl1 <- data.frame(items=itms, typ="n", scl=1e4, dec=0, hdr=c("Rtn","Hld","Trd","Car"))
    kl2 <- data.frame(items=c("grp","side"), typ="c", scl=1, dec=0, hdr=c("Group","LS"))
    fmt1 <- list(n=1, klist=as.list(rbind(kl1, kl2)), ll="", lr="", bars="rtn")

    combine <- function(y) {
        if (is.null(y$newsym)) return(y)
        yc <- data.frame(symbol=sort(unique(y$newsym)))       # Assumes there's a "newsym" column
        x <- tapply(y$side, y$newsym, function(v) g.dflt(setdiff(unique(v), "Z"), "Z"))
        yc$side <- ifelse(sapply(x, length)==1, sapply(x, "[[", 1), "M")
        for (i in c("rtn","hld","trd","car")) yc[[i]] <- tapply(y[[i]], y$newsym, sum)
        yc
    }
    addgroup <- function(y) {                                           # Group, order, and total
        if (is.null(y$grp)) return(g.add.totals(y[order(-y$rtn), c("symbol","side", itms)]))
        if (any(is.na(y$grp))) stop("Undefined Groups")
        ytot <- g.add.totals(y, rb=FALSE);  ytot$grp <- "All"
        yl <- list()
        for (gg in sort(unique(y$grp))) {
            ya <- subset(y, grp==gg)
            ya <- ya[order(-ya$rtn), ]                  # In each group, order from best to worst
            yl[[gg]] <- g.add.totals(ya, label=gg %&% " Total")
        }
        yl <- yl[order(-sapply(yl, function(x) x$rtn[1]))]                         # Order groups
        y <- do.call(rbind, c(list(ytot), yl));  rownames(y) <- NULL
        y
    }

    for (port in c("COM","CUR_G10","PREROLL","VOL","CMDTY_STR","FI_STR","MP_SWAPS")) {
        if (port=="DIV_FUT") next                                              # Until it returns
        z <- sandAttrib(port, stdt, endt, currencies=(port=="VOL"))
        if (!nrow(z)) next
        if (attr(z,"maxerr") > 5e-4 || abs(attr(z,"cumerr")) > 5e-4 * max(1, sqrt(Nd/63)))
          stop("Large err in ", port)
        truRtn <- g.fmt(sum(attr(z,"true")$dret), scl=1e4, dec=0, plus=T)
        y <- aggregate(z[itms], z["sec_id"], sum)
        y$side <- tapply(z$wt, z$sec_id, function(x)
                  if (all(x==0)) "Z" else if (all(x>=0)) "L" else if (all(x<=0)) "S" else "M")
        y$symbol <- z$symbol[match(y$sec_id, z$sec_id)]

        ## y <- data.frame(symbol=attr(a,"syms"), side=sid, aa)         # From g.trdatt.R line 30
        ## names(y) <- c("symbol","side","rtn","hld","div","trd","car")

        y$newsym <- switch(port,        # Defines how to combine rows, and becomes the new symbol
                           COM=, PREROLL=substr(y$symbol, 1,
                                                ifelse(substr(y$symbol,1,2)=="LM",4,2)),
                           CUR_G10=, CUR_EM=substr(y$symbol, 1,3),
                           VOL=      g.parse(y$symbol, "-")$base,
                           CMDTY_STR=substr(y$symbol, 1, 2),                            # Was 1,4
                           FI_STR=   sub("-.*","", y$symbol),
                           NULL)
        if (port=="VOL") {y$newsym <- sub("SPXW","SPX", sub("WSX5E.","SX5E", y$newsym))
                          x <- substr(y$newsym, 1,2); q <- x %in% c("ES","MF","Z_")
                          y$newsym[q] <- x[q]}
        y <- combine(y)                        # Combine same "newsym"s, which becomes the symbol

        if (port %in% c("COM","CMDTY_STR")) {
            info <- db.sql("select * from gma..cmdty_info")
            info$grp <- sub("Agriculture", "Ag", info$grp)          # Bobe wanted the longer name
            uly <- sub("_", "", g.dflt(y$newsym, y$symbol))
            for (i in c("name","grp"))
              y[[i]] <- g.dflt(info[[i]][match(uly, info$cmdty)], "Unknown")
            y$name <- sub("^LME ", "", sub("Hard Red ", "", y$name))      # Hard Red Winter Wheat
        }
        if (port=="VOL") {
            map <- c(ES=1, MF=1, "Z_"=1, SPX=2, SX5=3, WSX=3, UKX=3, SMI=3, NKY=4, HSI=4, AS5=4)
            grpn <- map[match(substr(g.parse(y$symbol)$base, 1,3), names(map))]
            y$grp <- c("Hedge","OptUS","OptEur","OptAsia")[grpn]
            y$grp[nchar(y$symbol)==3 | substr(y$symbol,4,6)=="USD"] <-"Curr"
            y$grp[y$symbol=="MSUSVXHE"] <- "Swap"
        }
        y <- y[intersect(c("symbol","name","grp","side", itms), names(y))]
        y <- addgroup(y)                        # Add Total row and Group summaries, order by rtn

        if (port=="VOL") for (i in c("US","Eur","Asia")) {
            cmp <- switch(i, US="SPX", Eur=c("SX5E","UKX"), Asia=c("AS51","HSI","NKY","SMI"))
            y$grp[y$grp==g.p("Opt$i Total")] <- g.fmt(sum(VOL.WTS.EQ[cmp]), scl=100, dec=0, pct="%")
        }
        if (port=="VOL") y <- subset(y, grp != "Curr")

        nscr <- regexpr("Total$", y$symbol) > 0
        nscr[g.lag(regexpr("Total$", y$symbol) > 0, -1, FALSE)] <- 2
        fmt <- c(fmt1, list(nscr=nscr, title=g.p("$port [$truRtn]")))
        pos <- switch(port, COM=c(0,1), CUR_G10=c(.45,1), CUR_EM=c(.45,.75), PREROLL=c(.45,.50),
                      VOL=c(.71,1), CMDTY_STR=c(0,.37), FI_STR=c(.71,.48), DIV_FUT=c(.10,.15),
                      MP_SWAPS=c(.40,.65))
        g.rpt.plot(y, fmt, pos, cex=.6, xsep=1.8, ysep=1.9)
    }
}
