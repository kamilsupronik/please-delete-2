mp.hldg.sum <- function(base="MP", today=g.date8()) {
    require(sand); require(gma)
    yest <- g.date.idx(today,T,-1)
    tna <- sandTna(base, yest)
    par(oma=c(2, 1, 2, 1), mar=c(0, 1, 2, 1), xpd=NA)
    g.plot.init(xaxs="i", yaxs="i")
    attach(gma.info(), name="info"); on.exit(detach(info)) # bdinfo, cminfo, eqinfo, fxinfo, curs

    for (acls in c("BD","CM","EQ","FX")) {
        if (base=="UC" && acls=="CM") next
        plist <- switch(acls, BD=list(c("BDVAL","BDCAR","CDCAR","BDMOM","BDDEF","BDLIQ"),
                                      "BDVOL"),
                              CM=list(c("CMVALBC","CMVALSD","CMCAR","CMMOM","CMDEF","CMLIQ"),
                                      "CMVOL"),
                              EQ=list(c("EQCAR","EQMOM"),
                                      "EQVOL","EQLIQ","EQVAL","EQDEF"),
                              FX=list(c("FXVAL","FXCAR","FXMOM","FXDEF"),
                                      "FXVOL"))
        ports <- base %&% plist[[1]]
        h <- sandHoldings(ports, today)[c("sandid","symbol","shares","mkt_val_usd")]
        h$wt <- h$mkt_val_usd / tna
        xs <- tapply(h$shares,  h$symbol, sum)
        xn <- tapply(    h$wt,  h$symbol, sum)
        xg <- tapply(abs(h$wt), h$symbol, sum)
        y <- data.frame(symbol=names(xs), shares=as.vector(xs), net=as.vector(xn),
                        gross=as.vector(xg))
        if (FALSE && length(ports) > 1) for (port in ports) {
            nm <- substr(port, 5,7)
            hp <- subset(h, sandid == sandId(port))
            y[[nm]] <- Z(hp$wt[match(y$symbol, hp$symbol)])
        }

        if (acls=="BD") {
            q <- substr(y$symbol, 1,4) %in% c("CDX.","ITRX");  yb <- y[!q, ];  yc <- y[q, ]
            idx <- match(sub("_","", substr(yb$symbol, 1, nchar(yb$symbol)-2)), bdinfo$bond)
            for (i in c("country","cur","name")) yb[[i]] <- bdinfo[[i]][idx]
            yb$name <- bdinfo$name[idx]
            yb$reg <- g.region(substr(yb$country, 1,2), canada=T, uk=F, japan=F)
            yb$grp <- ifelse(grepl("30Y|10Y|USTN|ULTRA|TLT", yb$name), "LongTerm",
                      ifelse(grepl("5Y", yb$name), "MedTerm",
                      ifelse(grepl("2Y", yb$name), "ShortTerm", "Unknown")))
            ybn <- Z(tapply(yb$net,   yb[c("reg","grp")], sum))
            ybg <- Z(tapply(yb$gross, yb[c("reg","grp")], sum))
            mn <- g.fmt(ybn, scl=100, dec=1, pct="%", plus=T)
            mg <- g.fmt(ybg, scl=100, dec=1, pct="%")
            for (i in 1:length(mn))  mn[i] <- paste(mn[i], mg[i], sep=" / ")
            y <- data.frame(Region=intersect(c("US","Can","Europe","Asia"), rownames(ybg)))
            for (j in intersect(c("ShortTerm","MedTerm","LongTerm"), colnames(ybg)))
              y[[j]] <- mn[y$Region, j]
            if (nrow(y)) g.rpt.plot(y, list(title="Bonds", n=1), c(.02,1))

            yc$reg <- ifelse(substr(yc$symbol,1,3)=="CDX", "US","Europe")
            yc$grd <- ifelse(grepl("IG|EUR", yc$symbol), "Investment","HighYield")
            ycn <- Z(tapply(yc$net,   yc[c("reg","grd")], sum))
            ycg <- Z(tapply(yc$gross, yc[c("reg","grd")], sum))
            mn <- g.fmt(ycn, scl=100, dec=1, pct="%", plus=T)
            mg <- g.fmt(ycg, scl=100, dec=1, pct="%")
            for (i in 1:length(mn))  mn[i] <- paste(mn[i], mg[i], sep=" / ")
            y <- data.frame(Region=intersect(c("US","Europe"), rownames(ycg)))
            for (j in intersect(c("Investment","HighYield"), colnames(ycg)))
              y[[j]] <- mn[y$Region, j]
            if (nrow(y)) g.rpt.plot(y, list(title="Credit", n=1), c(.02,.7))
        } else if (acls=="FX") {
            y$symbol <- substr(y$symbol, 1,3)
            y$grp <- ifelse(y$symbol %in% G10, "G10","EM")
            yfn <- Z(tapply(y$net,   y[c("grp")], sum))
            yfg <- Z(tapply(y$gross, y[c("grp")], sum))
            mn <- g.fmt(yfn, scl=100, dec=1, pct="%", plus=T)
            mg <- g.fmt(yfg, scl=100, dec=1, pct="%")
            for (i in 1:length(mn))  mn[i] <- paste(mn[i], mg[i], sep=" / ")
            y <- data.frame(Group=c("G10","EM"))
            y$fx <- mn[y$Group]
            g.rpt.plot(y, list(title="FX", n=1), c(.02,.4))
        }
    }
    titl <- base %&% " Holdings at Close of " %&% g.fmt(yest,"d")
    title(titl)
}
