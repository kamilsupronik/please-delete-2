mf.hldg.rpt <- function(fn=tempfile(), today=g.date8()) {
    require(mt)                                                                  # require(chalk)
    yest <- g.date.idx(today, T, -1)
    m.load("m.secref", dt=today)
    y <- g.holdings("MANFUT")                                               # chalkHoldings("MF")
    if (today < g.date8())
      y <- db.sql("select symbol, shares, mkt_value, w_eq as wt from hist..holdings_hist",
                  " where fund='MANFUT' and date='$yest'")
    idx <- match(y$symbol, m.secref$symbol)
    for (i in c("sec.name","sub.type")) y[[i]] <- m.secref[[i]][idx]
    y <- y[order(y$sub.type, -y$wt), c("sub.type","symbol","shares","mkt.value","wt","sec.name")]

    z <- g.sql("select * from xref..currency where stop_dt >= getdate()")
    if (any(q <- y$sub.type=="CURRFWD")) y$sec.name[q] <- 
      sub("New | \\(NEW\\)", "", z$curncy_name[match(substr(y$symbol[q], 1,3), z$iso_char)])

    yy <- NULL                                                # A new table with Gross & Net rows
    for (i in unique(y$sub.type)) {
        yi <- subset(y, sub.type==i)
        ya <- data.frame(sub.type=i, symbol=c("Gross","Net"), shares=NA,
                         mkt.value=c(sum(abs(yi$mkt.value)), sum(yi$mkt.value)),
                         wt=c(sum(abs(yi$wt)), sum(yi$wt)), sec.name="")
        yy <- rbind(yy, yi, ya)
    }
    ya <- data.frame(sub.type="All", symbol=c("Gross","Net"), shares=NA,
                     mkt.value=c(sum(abs(y$mkt.value)), sum(y$mkt.value)),
                     wt=c(sum(abs(y$wt)), sum(y$wt)), sec.name="")
    yy <- rbind(yy, ya)
    nscr <- pmin(2, 1*(g.lag(yy$symbol,-1)=="Gross") + 2*(yy$symbol=="Net"))
    yy$sub.type[duplicated(yy$sub.type)] <- ""                          #  & yy$symbol != "Gross"
    kl <- list(items=c("sub.type","shares","mkt.value","wt","sec.name"),
               typ=c("c",rep("n",3),"c"), dec=c(0,0,0,2,0), scl=c(1,1,1,100,1),
               hdr=c("Type","Shares","MktVal ($)","Wt (%)","Name"))
    font <- matrix(0, nrow(yy), ncol(yy))
    font[yy$symbol %in% c("Gross","Net"), ] <- 2
    titl <- "MANFUT Holdings " %&% g.fmt(g.date8(),"d")
    if (today < g.date8()) titl <- "MANFUT Holdings at Close of " %&% g.fmt(yest,"d")
    g.rpt.tex(yy, list(title=titl, n=1, klist=kl, nscr=nscr, orient="P", font=font), file=fn)
    g.rpt.xls(yy, list(title=titl, n=1, klist=kl, nscr=nscr), file=sub(".pdf$","",fn))
}

mf.trd.rpt <- function(fn=tempfile()) {
    require(mt)                                                                  # require(chalk)
    m.load("m.secref")
    info <- db.sql("select * from gma..curncy_info")

    yt <- g.orders("MANFUT", fields=c("symbol","side","fill","ave_fill_price","fx","multiplier"))
    if (!nrow(yt)) return()
    yt$dshs <- yt$fill * ifelse(yt$side %in% c("B","C"), 1, -1)
    q <- nchar(yt$symbol)==15 & substr(yt$symbol, 4,6)=="USD" &
         info$invert[match(substr(yt$symbol, 1,3), info$curncy)]==1
    yt$ave_fill_price[q] <- 1 / yt$ave_fill_price[q]
    yt$tdol <- yt$dshs * yt$multiplier * yt$ave_fill_price * yt$fx

    ## yt <- g.readDf("/gcm/quantres/rdata/Chalk/latest/MF.fund")
    ## yt$tdol <- -yt$ddol
    ## yt <- subset(yt, type %in% c("B","S"), c("symbol","dshs","tdol"))

    idx <- match(yt$symbol, m.secref$symbol)
    for (i in c("sec.name","sub.type")) yt[[i]] <- m.secref[[i]][idx]
    yt <- yt[order(yt$sub.type, -yt$tdol), c("sub.type","symbol","dshs","tdol","sec.name")]
    ya <- data.frame(sub.type="Gross", symbol="", dshs=NA, tdol=sum(abs(yt$tdol)), sec.name="")
    yt <- rbind(yt, ya)
    nscr <- which(!duplicated(yt$sub.type))[-1] - 1
    yt$sub.type[duplicated(yt$sub.type)] <- ""
    kl <- list(items=c("sub.type","dshs","tdol","sec.name"),
               typ=c("c",rep("n",2),"c"), dec=c(0,0,0,0), scl=c(1,1,1,1),
               hdr=c("Type","Trd Shs","Trd $","Name"))
    g.rpt.tex(yt, list(title="MANFUT Trades " %&% g.fmt(g.date8(),"d"), n=1, klist=kl,
                       nscr=nscr, orient="P"), file=fn)
}

mf.univ.rpt <- function() {
    require(gma)
    yest <- g.date.idx( ,T,-1)
    rconn <- g.connect.sql("ciq", "gcmquant", "Plusm1nusSQL", global=F)
    gconn <- g.connect.sql(global=F)

    txt <- paste(sQuote(MF.UNIV.CM), collapse=",")
    y <- db.sql("select * from gma..cmdty_info where cmdty in ($txt)", conn=gconn)
    g.rpt.tex(y, list(title="Commodity Universe", n=1, orient="P"))

    txt <- paste(sQuote(MF.UNIV.FX), collapse=",")
    y <- db.sql("select * from gma..curncy_info where curncy in ($txt)", conn=gconn)
    g.rpt.tex(y, list(title="Currency Universe", n=1, orient="P"))

    txt <- paste(sQuote(MF.UNIV.EQ), collapse=",")
    y <- db.sql("select * from res..mfeq_info where eqty in ($txt)", conn=rconn)
    g.rpt.tex(y, list(title="Equity Universe", n=1, orient="P"))

    txt <- paste(sQuote(MF.UNIV.BD), collapse=",")
    y <- db.sql("select * from res..mfbd_info where bond in ($txt)", conn=rconn)
    g.rpt.tex(y, list(title="Bond Universe", n=1, orient="P"))


    txt <- paste(sQuote(MF.UNIV.CM), collapse=",")
    ycm <- db.sql("SELECT cmdty, ticker, price, dprc, volume FROM gma..cmdty WHERE cmdty in ($txt)",
                  " AND cnum=2 AND date='$yest' order by cmdty", conn=gconn)
    txt <- paste(sQuote(MF.UNIV.EQ), collapse=",")
    yeq <- db.sql("SELECT eqty, ticker, price, dprc, volume FROM res..mfeq WHERE eqty in ($txt)",
                  " AND cnum=1 AND date='$yest' order by eqty", conn=rconn)
    txt <- paste(sQuote(MF.UNIV.BD), collapse=",")
    ybd <- db.sql("SELECT bond, ticker, price, dprc, volume FROM res..mfbd WHERE bond in ($txt)",
                  " AND cnum=1 AND date='$yest' order by bond", conn=rconn)
}

if (FALSE) {
    pgSql("update holdings_eod set price_usd=$x, mkt_value_usd=100*$x, w_eq=100*$x/4965815",
          " where fund='MANFUT' and symbol='QCH7' and date='20170208'", x = 1527.27 * .113177)
    pgSql("update holdings_eod set price_usd=$x, mkt_value_usd=100*$x, w_eq=100*$x/4974737",
          " where fund='MANFUT' and symbol='QCH7' and date='20170209'", x = 1535.55 * .112277)
    pgSql("select * from holdings_eod where fund='MANFUT' and symbol='QCH7'")
}

mf.attrib <- function(stdt=g.date.idx( ,T, cal="mnth.beg", -1),                    # Was 20170201
                      endt=g.date.idx( ,T,-1), type="f") {
    require(chalk); require(gma); require(report)
    ## g.printer(FALSE, margins=c(-2,-2,1,-1,0), file="mf.attrib.pdf")
    g.plot.init()
    mtext(g.p("Managed Futures Attribution [type $type]"), 3, cex=1.5)
    itms <- c("","hld.","trd.") %&% "pnl.wt"
    kl1 <- data.frame(items=c(itms,"true"), typ="n", scl=1e4, dec=0,
                      hdr=c("Rtn","Hld","Trd","True"))
    kl2 <- data.frame(items=c("grp","side"), typ="c", scl=1, dec=0, hdr=c("Group","LS"))
    fmt1 <- list(n=1, klist=as.list(rbind(kl1, kl2)), ll="", lr="", bars="pnl.wt")

    a <- switch(type, f =  calcFundPnL(stdt, endt, "MANFUT",         bcost=0),
                      s =  calcFundPnL(stdt, endt, "STRAT.MANFUT_F", bcost=0),
                      c = chalkFundPnL(stdt, endt, "MF"),
                      error("Unknown type"))
    w <- a[ , , "w.eq", drop=FALSE]
    sid <- apply(w, 2, function(x)
                 if (all(x==0)) "Z" else if (all(x >= 0)) "L" else if (all(x <= 0)) "S" else "M")
    aa <- colSums(a[ , , itms, drop=FALSE])                             # Sum over dates
    y <- data.frame(symbol=attr(a, "syms"), side=sid, aa)
    y <- y[order(-y$pnl.wt), ]

    ## By security (incl "Other" if too many):
    n <- 20
    x <- colSums(y[(n+1):(nrow(y)-n), itms])
    ya <- data.frame(symbol="Other", side="");  for (i in itms) ya[[i]] <- x[i]
    ys <- g.add.totals(rbind(y[1:n, ], ya, y[nrow(y)+1-n:1, ]), top=F, adds=itms)
    
    fmt <- c(fmt1, list(nscr=c(n, n+1, 2*n+1),
                        title="MF " %&% paste(g.fmt(c(stdt,endt),"d"), collapse=" - ")))
    g.rpt.plot(ys, fmt, c(.05,1), cex=.8)

    ## By asset type:
    minis <- c("DFW","HU","ID","NO","SW","TMI")         # Mini versions of GX, HI, IB, NK, ST, TP
    y$newsym <- "Unknown"
    q <- nchar(y$symbol) < 15
    y$newsym[!q] <- ifelse(substr(y$symbol[!q], 4,6)=="USD", "Cur", "Com")
    idq <- sub("_", "", sub(".[0-9]*$", "", y$symbol[q]))
    y$newsym[q] <- ifelse(idq %in%   MF.UNIV.CM, "Com",
                   ifelse(idq %in% c(MF.UNIV.EQ, minis), "Eqty",
                   ifelse(idq %in%   MF.UNIV.BD, "Bond", "Unknown")))
    yc <- data.frame(symbol=sort(unique(y$newsym)))
    x <- tapply(y$side, y$newsym, function(v) g.dflt(setdiff(unique(v), "Z"), "Z"))
    yc$side <- ifelse(sapply(x, length)==1, sapply(x, "[[", 1), "M")
    for (i in c("pnl.wt","hld.pnl.wt","trd.pnl.wt")) yc[[i]] <- tapply(y[[i]], y$newsym, sum)
    yc <- g.add.totals(yc, top=F, adds=itms)
    fmt <- c(fmt1, list(title="By Asset Type", nscr=nrow(yc)-1))
    g.rpt.plot(yc, fmt, c(.55,1), cex=.8)

    ## By date (note "true" outperforms every day due to interest):
    yd <- data.frame(date=rownames(a), true=as.double(attr(a, "true")))
    for (i in itms) yd[[i]] <- as.double(rowSums(a[ , ,i, drop=FALSE]))
    yd <- g.add.totals(yd, top=F)
    if (nrow(yd) > 32) yd <- yd[c(1:31, nrow(yd)), ]
    nscr <- 1*(seq(nrow(yd)) %% 5 == 0)
    nscr[nrow(yd)-1] <- 2
    fmt <- c(fmt1, list(title="By Date", nscr=nscr))
    g.rpt.plot(yd, fmt, c(.55, .80), cex=.8)
}

mf.perf <- function(stdt=20170201, endt=g.date.idx( ,T,-1)) {                 # Or endt=month-end
    ## I should probably re-write this to define stdt=20170202...
    require(chalk); require(sand); require(bbdotnet3)
    ## g.printer(F)
    ## chalkHistory("MF", "MANFUT", stdt, endt, "MF (Chalk) vs MANFUT (true)",
    ##              stats=c("tr","sd","ir","dd"))
    dts <- stdt %usd8% endt
    funds <- c("MF","MFINF","MANFUT","MANFUT_F","AQMIX","MPBDMOM","MPCMMOM","MPEQMOM","MPFXMOM")
    tna <- rtns <- g.mk.array(dts, funds, data=0)

    ## Chalk:
    h <- g.readDf(chalkFile("MF", NULL))
    tna [ ,"MF"]    <- h$tna[match(dts, h$date)]
    rtns[ ,"MF"]    <- h$rtn[match(dts, h$date)] - .0008 * h$thru[match(dts, h$date)] # 8bp tcost
    h <- g.readDf(chalkFile("MFINF", NULL))
    tna [ ,"MFINF"] <- h$tna[match(dts, h$date)]
    rtns[ ,"MFINF"] <- h$rtn[match(dts, h$date)] - .0008 * h$thru[match(dts, h$date)]
    rtns[ ,"MFINF"] <- g.dflt(rtns[ ,"MFINF"], rtns[ ,"MF"])

    ## Fund:
    h <- g.fund.perf("MANFUT", stdt, endt, usDates=TRUE)
    tna [ ,"MANFUT"] <- h$tna[match(dts, h$date)]
    rtns[ ,"MANFUT"] <- h$rtn[match(dts, h$date)]

    ## Sand (show CONTRIBUTIONS to MF):
    for (fund in c("MANFUT_F","MPBDMOM","MPCMMOM","MPEQMOM","MPFXMOM")) {
        h <- sandDperf(fund, stdt, endt, date8=T)
        tna [ ,fund] <- stna <- h$tna_usd[match(dts, h$date)]
        rtns[ ,fund] <-         h$dret   [match(dts, h$date)] * stna/tna[ ,"MANFUT_F"]
    }

    ## Bloomberg:
    bb <- getBbHistorical(g.date.idx(stdt,T,-1), endt, , "AQMIX", "Index", pod="DB", useDates=F)
    if(is.null(bb)){
        bb <- getBbHistorical(g.date.idx(stdt,T,-1), endt, , "AQMIX", "Index", pod="GL", useDates=F)
    }
    bbr <- (bb / g.lag(bb) - 1)[-1, , drop=F]
    rtns[ ,"AQMIX"] <- bbr[match(dts, rownames(bbr)), 1]

    ## Note we ignore returns on stdt=2/1, because we got in at the close that day.
    if (stdt != 20170201) {
        ra <- rtns[1, , drop=F]; ra[] <- NA; rownames(ra) <- g.date.idx(stdt,T,-1)
        rtns <- rbind(ra, rtns)
    }
    rtns[1, ] <- NA

    par(mfrow=c(2,1))
    g.plot.perf(rtns[ ,1:5], title="MF (Chalk) with 8 bp tcost vs MANFUT (true)",
                stats= c("tr","sd","ir","dd"), cols=c("cyan","blue","red","green","orange"))
    mtext("MANFUT returns (red) are gross; AQMIX returns (orange) are net", cex=.7)
    ## lines(1 + c(0, cumsum(rtns[-1,1] - rtns[-1,2])), lwd=2)   # Chalk-True
    g.plot.perf(rtns[ ,6:9], title="Sand Subportfolio Contributions",
                stats= c("tr","sd","ir","dd"), cols=c("blue","green","orange","red"))

    if (FALSE) {                                     # Just MANFUT and AQMIX, for SAI consumption
        g.printer(file="~/Projects/PerfMtg/manfut.aqmix.png")
        g.plot.perf(rtns[ ,c("MANFUT","AQMIX")], title="",
                    stats= c("tr","sd","ir","dd"), cols=c("blue","goldenrod"))
        dev.off()

        g.printer(file="~/Projects/PerfMtg/manfut.subportfolios.png")
        g.plot.perf(rtns[ ,6:9], title="", stats= c("tr","sd","ir","dd"),
                    cols=c("blue","green","orange","red"))
        dev.off()
    }
}
