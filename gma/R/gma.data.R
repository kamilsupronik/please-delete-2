add.comdty <- function(stdt=19730102, endt=g.date.idx(, TRUE, -1), comdty,
                       suffixes=1:6, roll.adj.pod, no.roll.adj.pod){
    ## add a commodity to the gma SQL table
    ## Note: Will not add commodities for which we do not have prices
    require(bbdotnet3)
    stdt <- g.read.date(stdt); endt <- g.read.date(endt)
    stopifnot(!is.na(comdty) && length(comdty)==1)

    mydt <- g.date.idx(stdt, TRUE, -3)                                # Start bba earlier

    tick2 <- tick1 <- comdty
    idx <- nchar(comdty) == 1                                            # "S" 
    if(any(idx)) tick2[idx] <- comdty[idx] %&% " "                       # "S "
    ticks <- paste(tick2 %&% suffixes, "Comdty")                         # "S 1 Comdty"
    
    flds <- c("last_trade","open_int","px_volume")
    bbu <- getBbHistorical(stdt, endt, flds, ticks, pod=no.roll.adj.pod)             # Unadjusted
    dimnames(bbu)[[3]] <- flds
    if (is.null(bbu) || !nrow(bbu) || all(is.na(bbu[ , ,"last_trade"])))    # E.g. London holiday
      {cat("No data for", comdty, "\n"); return()}

    ## Tickers:
    bbt <- getBbHistorical(stdt, endt, "fut_cur_gen_ticker",        ticks, pod=no.roll.adj.pod)
    if ((is.null(bbt) || !nrow(bbt)) && stdt==endt && endt==g.date.idx( ,T,-1)) {
        bbt <- getBbHistorical(g.date8(), g.date8(), "fut_cur_gen_ticker", ticks,
                               pod=no.roll.adj.pod)
        rownames(bbt) <- endt
    }
    if (is.null(bbt) || !nrow(bbt)) stop(g.p("Could not retrieve Bb tickers for $comdty"))
    bbt <- sub(" *$", "", bbt)                                                # remove whitespace
    q1 <- substr(bbt, 4,4) %in% 9:9
    q2 <- substr(bbt, 4,4) %in% 0:8
    bbt[q1] <- sub("^(...)(.)$", "\\11\\2", bbt[q1])                 # 5-char, e.g. BOF1 -> BOF11
    bbt[q2] <- sub("^(...)(.)$", "\\12\\2", bbt[q2])                 # Get ready for the 20's!

    ## Expiry yrmo (field used to be "fut_gen_month_yr"):
    bbm <- getBbHistorical(stdt, endt, "current_contract_month_yr", ticks, pod=roll.adj.pod)
    if ((is.null(bbm) || !nrow(bbm)) && stdt==endt && endt==g.date.idx( ,T,-1)) {
        bbm <- getBbHistorical(g.date8(), g.date8(), "current_contract_month_yr", ticks,
                               pod=no.roll.adj.pod)
        rownames(bbm) <- endt
    }
    if (is.null(bbm) || !nrow(bbm)) stop(g.p("Could not retrieve Bb expirations for $comdty"))
    
    ## Adjusted:
    bba <- getBbHistorical(mydt, endt, "last_trade",                ticks, pod=roll.adj.pod)
    if (is.null(bba) || !nrow(bba)) stop(g.p("Could not retrieve adjusted prices for $comdty"))
    
    ## Fill adjusted prices forward, up to 5 days, to make diffs:
    for (j in seq(length.out=ncol(bba))) {
        if (all(is.na(bba[ ,j]))) next
        rng <- do.call(seq, as.list(range(which(!is.na(bba[ ,j])))))
        for (iter in 1:5)
          if (length(idx <- intersect(which(is.na(bba[ ,j])), rng))) bba[idx, j] <- bba[idx-1, j]
    }
    
    dif <- bba - g.lag(bba)                          # Adjusted price differential from yesterday

    # Make everything the same shape (same dates):
    idx <- g.intersect(rownames(bbu), rownames(bbt), rownames(bbm), rownames(dif))
    bbu <- bbu[idx, , ,drop=FALSE]
    bbt <- bbt[idx, ,drop=FALSE]
    bbm <- bbm[idx, ,drop=FALSE]
    dif <- dif[idx, ,drop=FALSE]
    stopifnot(dim(bbu)[1:2] == dim(bbt), dim(bbt) == dim(bbm),  dim(bbm) == dim(dif))    
    
    ## Create a date x curncy x item array "z":
    dts <- g.read.date(idx)            # Always weekdays, some holidays (eg CO traded on 2/16/09)
    zu <- zo <- zv <- zd <- zt <- zm <- g.mk.array(date=dts, cmdty=tick1, cnum=suffixes)
    for (j in seq(along=tick1)) for (k in as.character(suffixes)) {
        colm <- tick2[j] %&% k %&% " Comdty"
        zu[ ,j,k] <- bbu[ , colm, "last_trade"]
        zo[ ,j,k] <- bbu[ , colm, "open_int"]
        zv[ ,j,k] <- bbu[ , colm, "px_volume"]
        zd[ ,j,k] <- dif[ , colm]
        zt[ ,j,k] <- bbt[ , colm]
        zm[ ,j,k] <- bbm[ , colm]
    }

    ## Turn into ragged array:
    au <- g.array.to.ragged(zu, item="price")
    ao <- g.array.to.ragged(zo, item="openint")
    av <- g.array.to.ragged(zv, item="volume")
    ad <- g.array.to.ragged(zd, item="dprc")
    at <- g.array.to.ragged(zt, item="ticker")
    am <- g.array.to.ragged(zm, item="expiry")
    a <- cbind(au, ad[ ,4,drop=F], at[ ,4,drop=FALSE], am[ ,4,drop=FALSE],
               ao[ ,4,drop=FALSE], av[ ,4,drop=FALSE])
    a <- subset(a, !is.na(price))

    a$date <- g.Date(a$date)
    a <- a[order(a$cmdty, a$cnum, a$date), ]; rownames(a) <- NULL
    
    a
}

## add a commodity to the gma SQL table
add.lme.comdty <- function(stdt=19730102, endt=g.date.idx(, TRUE, -1), comdty,
                           suffixes=c("DY", "DS03", "DS15"),
                           roll.adj.pod, no.roll.adj.pod){
    
    require(bbdotnet3)
    stdt <- g.read.date(stdt); endt <- g.read.date(endt)
    stopifnot(is.atomic(comdty), length(comdty)==1L, !is.na(comdty))

    mydt <- g.date.idx(stdt, TRUE, -3)                                # Start bba earlier

    tick2 <- tick1 <- comdty
    idx <- nchar(comdty) == 1                                           # "S" 
    if(any(idx)) tick2[idx] <- comdty[idx] %&% " "                      # "S "
    ticks <- paste(tick2 %&% suffixes, "Comdty")                        # "S 1 Comdty"
        
    flds <- c("last_trade","open_int","px_volume")
    bbu <- getBbHistorical(stdt, endt, flds, ticks, pod=no.roll.adj.pod)             # Unadjusted
    dimnames(bbu)[[3]] <- flds
    if (is.null(bbu) || !nrow(bbu)) {cat("No data (London closed?):", comdty, "\n"); return()}
    bba <- getBbHistorical(mydt, endt, "last_trade", ticks, pod=roll.adj.pod)

    bbt <- g.slice(bbu, , ,"last_trade", drop=c(F,F,T))
    bbt[ , ] <- rep(sub(" .+$", "", colnames(bbt)), each=nrow(bbt))               # Generic names

    bbm   <- bbt
    mnths <- substr(colnames(bbm),7,8) ## contract expiration dates
    mnths[grep("C", mnths)] <- 0       ## set spot contracts to "0" cnum 
    mnths <- as.numeric(mnths)

    ## convert dates to MTH YY format
    dts <- g.date.idx(g.Date(rownames(bbm)), TRUE, mnths, cal="cal.mnth.end")
    dts.fmt <- toupper(format(dts, "%b %y"))
    bbm[ , ] <- dts.fmt
    
    ## Fill adjusted prices forward, up to 5 days, to make diffs:
    for (j in seq(along=suffixes)) {
        if (all(is.na(bba[ ,j]))) next
        rng <- do.call(seq, as.list(range(which(!is.na(bba[ ,j])))))
        for (iter in 1:5)
          if (length(idx <- intersect(which(is.na(bba[ ,j])), rng))) bba[idx, j] <- bba[idx-1, j]
    }
    
    dif <- bba - g.lag(bba)  # Adjusted price differential from yesterday

    # Make everything the same shape (same dates):
    ## idx <- g.intersect(rownames(bbu), rownames(bbt), rownames(bbm), rownames(dif))
    idx <- intersect(intersect(rownames(bbu), rownames(bbt)),
                     intersect(rownames(bbm), rownames(dif)))
    bbu <- bbu[idx, , ,drop=FALSE]
    bbt <- bbt[idx, ,drop=FALSE]
    bbm <- bbm[idx, ,drop=FALSE]
    dif <- dif[idx, ,drop=FALSE]
    stopifnot(dim(bbu)[1:2] == dim(bbt), dim(bbt) == dim(bbm),  dim(bbm) == dim(dif))
    
    ## Create a date x curncy x item array "z":
    dts <- g.read.date(idx)            # Always weekdays, some holidays (eg CO traded on 2/16/09)
    zu <- zo <- zv <- zd <- zt <- zm <- g.mk.array(date=dts, cmdty=tick1, cnum=mnths)
    for (j in seq(along=tick1)) for (k in 1:length(mnths)) {
        colm <- tick2[j] %&% suffixes[k] %&% " Comdty"
        zu[ ,j,k] <- bbu[ , colm, "last_trade"]
        zo[ ,j,k] <- bbu[ , colm, "open_int"]
        zv[ ,j,k] <- bbu[ , colm, "px_volume"]
        zd[ ,j,k] <- dif[ , colm]
        zt[ ,j,k] <- bbt[ , colm]
        zm[ ,j,k] <- bbm[ , colm]
    }

    ## Turn into ragged array:
    au <- g.array.to.ragged(zu, item="price")
    ao <- g.array.to.ragged(zo, item="openint")
    av <- g.array.to.ragged(zv, item="volume")
    ad <- g.array.to.ragged(zd, item="dprc")
    at <- g.array.to.ragged(zt, item="ticker")
    am <- g.array.to.ragged(zm, item="expiry")
    a <- cbind(au, ad[ ,4,drop=FALSE], at[ ,4,drop=FALSE], am[ ,4,drop=FALSE],
               ao[ ,4,drop=FALSE], av[ ,4,drop=FALSE])
    a <- subset(a, !is.na(price))

    a$date <- g.Date(a$date)
    a <- a[order(a$cmdty, a$cnum, a$date), ]; rownames(a) <- NULL
    
    a
}

## loads the entire historical commodity database
get.comdty.prices <- function(stdt, endt){

    stdt <- g.read.date(stdt); endt <- g.read.date(endt)
    sql <- g.p("SELECT a.cmdty 'comdty', b.[date], b.cnum, b.price, b.dprc, b.ticker, b.expiry",
               "  FROM gma..cmdty_info a LEFT JOIN gma..cmdty b ON a.cmdty=b.cmdty",
               " WHERE ((cnum IN (0,3) AND LEN(b.cmdty)=4)",
               "     OR (cnum IN (1,2) AND LEN(b.cmdty) IN (1,2)))",
               "   AND date between '$stdt' and '$endt'",
               " ORDER BY a.cmdty, b.[date]")
    dta <- db.sql(sql, Date=TRUE)

    z  <- g.ragged.to.array(dta, keys=c("date","comdty","cnum"), item=c("price","dprc"))
    m  <- apply(Z(z[ , , ,"dprc"]), 2:3, cumsum)
    z  <- abind(z, adjprc=sweep(m, 2:3, Z(m[nrow(m), , ] - z[nrow(z), , ,"price"])))
    res <- g.array.to.ragged(z, keys=c("date","comdty","cnum"), item=c("price","dprc","adjprc"),
                             expand.last=TRUE)
    res <- na.omit(res)
    res$date <- g.Date(res$date)
    res <- subset(res, g.Date(stdt) <= date & date <= g.Date(endt)) ## subset out requisite dates

    res <- merge(res, dta[, c("comdty", "date", "cnum", "ticker", "expiry")],
                 by=c("comdty", "date", "cnum"))
    idx <- nchar(res$comdty) == 1
    res$comdty[idx] <- res$comdty[idx] %&% " "
    
    res
}

getETFData <- function(aDate, symbol, pod) {
    ## Get unadjusted price and total return data for ETF 'symbol' between
    ## 'stdt' and 'endt'
    ##
    ## Args:
    ##   stdt: the first date to get data
    ##   endt: the last date to get data
    ##   symbol: 1 or more ETF symbols
    ##   pod: a bbDotNet pod
    ##
    ## Returns:
    ##   A data.frame with columns 'date', 'symbol', 'price', and 'tret',
    ##   or NULL. Excludes non-US business days and records with no prices.
    ##   Prices are unadjusted for splits or dividends. Total returns are in
    ##   decimal form.
    require(bbdotnet3)

    ## Error Checking
    aDate <- g.Date(g.read.date(aDate))
    dm1   <- g.date.idx(aDate, TRUE, -1)

    kFields   <- c(price="PX_LAST",
                   tret="DAY_TO_DAY_TOT_RETURN_GROSS_DVDS",
                   yield="EQY_DVD_YLD_IND")
    
    ## Pull each field individually from Bb. Total return d/n work when 
    ## 'stdt' == 'endt' so always pull at least 2 bus days.
    price <- getBbHistorical(aDate, aDate, kFields["price"], symbol, "EQUITY", pod,
                             output="table")[[3]]
    tret  <- getBbHistorical(dm1, aDate, kFields["tret"],
                             symbol, "EQUITY", pod, output="table")[2,3, drop=TRUE]
    yield <- getBbHistorical(aDate, aDate, kFields["yield"], symbol, "EQUITY", pod,
                             output="table")[[3]]

    ## Create and populate the result data frame.
    res <- data.frame(date=aDate, symbol=sec, stringsAsFactors=FALSE)
    res$price <- g.dflt(price, NA_real_)
    res$tret  <- g.dflt(tret, NA_real_) / 100
    res$yield <- g.dflt(yield, NA_real_) / 100

    invisible(res)
}
