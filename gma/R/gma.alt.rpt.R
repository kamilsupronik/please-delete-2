gma.alt.rpt <- function() {
    suppressMessages({library(gma); library(sand); library(report); library(bbdotnet3)})
    options(stringsAsFactors=FALSE, warn=1)
    ddir <- m.rdd.path(g.date8(), GMA.DATA.DIR)
    for (i in c("gma.holdings","option","index")) load(g.p("$ddir/$i.RData"))

    par(oma=c(2, 1, 2, 1), mar=c(0, 1, 2, 1), xpd=NA)         # Assumes g.printer has been called
    g.plot.init(xaxs="i", yaxs="i")
    mtext("GC5ALT Holdings Report", side=3, cex=1.5)
    mtext("Period Ending " %&% g.fmt(g.date.idx( ,T,-1),"d"), side=3, line=0, adj=.9)
    x <- vol.hld("MPCMVOL", "None")
    g.rpt.plot(x$rpt, x$fmt, c(.5,.99), c(.5,1), cex=.7, xsep=1.2)
    
    g.plot.init(xaxs="i", yaxs="i")                                                        # Pg 2
    mtext("Period Ending " %&% g.fmt(g.date.idx( ,T,-1),"d"), side=3, line=0, adj=.9)
    x <- vol.hld("MPBDVOL", "None")
    g.rpt.plot(x$rpt, x$fmt, c(.5,.99), c(.5,1), cex=.7, xsep=1.2)

    g.plot.init(xaxs="i", yaxs="i")                                                        # Pg 3
    mtext("Period Ending " %&% g.fmt(g.date.idx( ,T,-1),"d"), side=3, line=0, adj=.9)
    x <- vol.hld("MPFXVOL", "None")
    pos <- g.rpt.plot(x$rpt, x$fmt, c(.5,.99), c(.5,1), cex=.7, xsep=1.2)

    ##############################  Managed Futures:  ###########################################

    m.load("m.secref")
    h <- g.holdings("MANFUT")
    h$sub.type <- m.secref$sub.type[match(h$symbol, m.secref$symbol)]
    cls <- c("Comdty","Curncy","Equity","Bond")
    map <- c(COMDTYFUT=1, COMDTYFWD=1, CURRFWD=2, INDEXFUT=3, TREASFUT=4)
    h$class <- cls[map[h$sub.type]]

    y <- data.frame(Class=c(cls,"Total"))
    y$Long  <- c(tapply(pmax(h$mkt.value, 0), h$class, sum)[cls], sum(pmax(h$mkt.value, 0)))
    y$Short <- c(tapply(pmin(h$mkt.value, 0), h$class, sum)[cls], sum(pmin(h$mkt.value, 0)))
    y$Gross <- c(tapply( abs(h$mkt.value),    h$class, sum)[cls], sum( abs(h$mkt.value)   ))
    y$Net   <- c(tapply(     h$mkt.value,     h$class, sum)[cls], sum(     h$mkt.value    ))
    fmt <- list(title="Managed Futures", n=1, nscr=c(4,5), bars="Class")
    posm <- g.rpt.plot(y, fmt, c(.10, pos[3]-.075), cex=.7)

    ###################################  Performance:  ##########################################

    ports <- c("MP", "MP_" %&% c("BD","CM","EQ","FX"),
               "MP__" %&% c("VAL","VOL","CAR","MOM","LIQ"),
               "MANFUT_F", "MPBDMOM","MPCMMOM","MPEQMOM","MPFXMOM",
               "JUNK_" %&% c("GC5COM","GC5VOL","MANFUT"))
    gma.perf.rpt(ports, yest, pctof=sandTna("MP"), nscr=c(1,5,10,15,18),
                 pos=c(posm[2]+.07, pos[3]-.075), cex=.7, xsep=1.2, ysep=1.8)

    ###################################  PreRoll:  ##############################################

    h <- sandHoldings("MPCMLIQ", type="i")
    rpt <- data.frame(side=c("Long","Short"),
                      nctr=c(sum(pmax(h$shares     ,0)), sum(pmin(h$shares,     0))),
                      dols=c(sum(pmax(h$mkt_val_usd,0)), sum(pmin(h$mkt_val_usd,0))))
    kl <- list(items=names(rpt), typ=c("c","n","n"), scl=rep(1,3), dec=rep(0,3),
               hdr=c("Side","# Ctr","MktVal"))
    fmt <- list(title="PreRoll", klist=kl, n=1)
    g.rpt.plot(rpt, fmt, c(.20, posm[3]-.05), cex=.7, xsep=1.2)

    if (nrow(h)) {
        g.plot.init(xaxs="i", yaxs="i")
        mtext("Period Ending " %&% g.fmt(g.date.idx( ,T,-1),"d"), side=3, line=0, adj=.9)
        h <- h[ ,c("symbol","shares","mkt_val_usd","w_eq")]
        tot <- data.frame(symbol="Abs Total")
        for (i in setdiff(names(h), names(tot))) tot[[i]] <- sum(abs(h[[i]]))
        rpt <- rbind(h, tot)
        nscr <- c(rep(0, nrow(rpt)-2), 1, 1)
        kl <- list(items=names(h), typ=c("c",rep("n",3)), scl=c(1,1,1,100), dec=rep(0,4),
                   hdr=c("Symbol","Shs","Value ($)","Wt (%)"))
        fmt <- list(title="MPCMLIQ", klist=kl, n=1, nscr=nscr)
        g.rpt.plot(rpt, fmt, c(.50, 1), c(.5,1), cex=.7, xsep=1.5)
    }
}
