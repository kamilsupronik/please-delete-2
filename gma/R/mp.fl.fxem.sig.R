##########
### generate daily signals for equity factor library
### 1. nowdate <- g.date.idx(,T,): today's date
##########

mp.fl.fxem.sig <- function(nowdate) {

    endt <- g.date.idx(nowdate, T, -1)   # end of the day yesterday
    stdt <- 19921231                     # initial date
    dts <- dates.usd[g.inorder(stdt, dates.usd, endt)]

######### fx
    curs <- c('USD', EM)
    txt <- paste(sQuote(curs), collapse=",")
    y <- db.sql("SELECT date, curncy, fx, i1, i2, ppp ",
                "FROM gma..curncy WHERE curncy in ($txt) ",
                "AND date between '19901231' and '$endt'",
                conn=gconn)     # i1 is for interest return
    y[(y$curncy=="TRY") & (y$date<20020101), 3:6] <- NA    # remove periods when hard peg to usd
    y[(y$curncy=="BRL") & (y$date<20020101), 3:6] <- NA
    y[(y$curncy=="RUB") & (y$date<20000101), 3:6] <- NA
    fxd <- g.ragged.to.array(y, c("date","curncy"),
                             c('fx','i1','i2','ppp'))        # date x curncy x item
    ccurs <- dimnames(fxd)[[2]]
    for (j in ccurs) fxd[ ,j,"i1"] <- g.dflt(fxd[ ,j,"i1"],
                                             fxd[ ,j,"i2"])  # fill missing "i1" with "i2"
    fxd <- fxd[rownames(fxd) %in% dates.usd,
               ccurs, c("fx","i1","ppp")]                    # US trade dates only
    info <- db.sql("select * from gma..curncy_info",
                   conn=gconn)                               # find fx need inverse
    invert <- as.logical(info$invert[match(ccurs, info$curncy)])
    for (j in which(invert)) fxd[ ,j,"fx"] <- 1 / fxd[ ,j,"fx"]   # inverse fx to be USD
    rt <- fxd[,,"i1"]          # fix data issue
    rt[which(rt < -100)] <- rt[which(rt < -100) - 1]
    fxd[,,"i1"] <- rt
    fxd[,,"fx"] <- na.locf(fxd[,,"fx"], na.rm=F)                  # fill missing data
    fxd[,,"i1"] <- na.locf(fxd[,,"i1"], na.rm=F)
    fxd[,,"ppp"] <- na.locf(fxd[,,"ppp"], na.rm=F)
    fxir <- sweep(fxd[,,"i1"], 1, fxd[,'USD','i1'], '-')/100/252
    fxir[is.na(fxir)] <- 0
    fxr <- fxd[2:nrow(fxd),,'fx'] / fxd[1:(nrow(fxd)-1),,'fx'] - 1 + fxir[-1, ]
    fxd <- fxd[,setdiff(colnames(fxd),'USD'),]
    fxr <- fxr[,setdiff(colnames(fxr),'USD')]
    ## country names match
    ctry <- c(BRL="BR",COP="CO",CZK="CZ",HUF="HU",IDR="ID",ILS="IL",
              INR="IN",KRW="KR",MXN="MX",MYR="MY",PLN="PL",RUB="RU",
              SGD="SG",THB="TH",TRY="TR",TWD="TW",ZAR="ZA")
    ctry3 <- c(BRL="BRA",COP="COL",CZK="CZE",HUF="HUN",IDR="IDN",ILS="ISR",
               INR="IND",KRW="KOR",MXN="MEX",MYR="MYS",PLN="POL",RUB="RUS",
               SGD="SGP",THB="THA",TRY="TUR",TWD="TWN",ZAR="ZAF")

######### raw data, signals, and weights
    sig <- ctry; sig[] <- NA;
    sig <- as.numeric(sig); names(sig) <- names(ctry)	    # set up sig variable


    
###### option related
    fxem.opt.iv.lvl <- sig; fxem.opt.iv.lvl[] <- NA
    fxem.opt.iv.crv.mom <- sig; fxem.opt.iv.crv.mom[] <- NA
    if (endt > 20031231) {		 # options data limit
        ## implied vol level - current over past one month 1M IV
        imp <- db.sql("Select * from res..ctry_iv_bb where ",
                      "assetclass='FX' AND date <= '$endt'", conn=rconn)
        imp.vol <- g.ragged.to.array(imp, c('date', 'name', 'maturity'), c('data'))
        alf <- imp.vol[nrow(imp.vol),,'m1'] /
            colMeans(imp.vol[max(1, nrow(imp.vol)-20):nrow(imp.vol),,'m1'], na.rm=T)
        alf[alf==1] <- NA
        fxem.opt.iv.lvl[match(names(alf),
                              names(fxem.opt.iv.lvl))] <- -alf   # long fx with low IV

        ## implied vol curve momentum - 3M and 12M IV curve one month momentum
        alf <- (imp.vol[nrow(imp.vol),,'m12'] - imp.vol[nrow(imp.vol),,'m3']) -
            (imp.vol[nrow(imp.vol)-20,,'m12'] - imp.vol[nrow(imp.vol)-20,,'m3'])
        fxem.opt.iv.crv.mom[match(names(alf),
                                  names(fxem.opt.iv.crv.mom))] <- alf  # long fx with bwd cv
    }



###### cross assets
    ## equity momentum - three-month equity index momentum spillover
    fxem.xat.eqt <- sig; fxem.xat.eqt[] <- NA    
    idx <- c(BRL="IBOV",COP="COLCAP",CZK="PX",HUF="BUX",IDR="JCI",ILS="MXIL",
             INR="NIFTY",KRW="KOSPI2",MXN="MEXBOL",MYR="FBMKLCI",PLN="WIG20",
             RUB="IMOEX",SGD="SIMSCI",THB="SET",TRY="XU100",TWD="TWSE",ZAR="TOP40")
    txt <- paste(sQuote(idx), collapse=",")
    y <- db.sql("SELECT date, idx, price FROM res..eqidx WHERE ",           # index prices
                "idx in ($txt) AND date <= '$endt'", conn=rconn)
    z <- g.ragged.to.array(y, c('date', 'idx'), c('price'))                # to array form 
    ixd <- g.nafill(z)             # fill NA
    ixr <- ixd[2:nrow(ixd), ] / ixd[1:(nrow(ixd)-1), ] - 1                 # index returns
    colnames(ixr) <- names(idx)[match(colnames(ixr), idx)]
    ixr.mom <-  apply(1+ixr[(nrow(ixr)-62):nrow(ixr), ], 2, cumprod) - 1  # 3M mom
    fxem.xat.eqt[match(colnames(ixr.mom),
                       names(fxem.xat.eqt))] <- ixr.mom[nrow(ixr.mom), ]  # long fx high eqr
    fxem.xat.eqt[fxem.xat.eqt==0] <- NA
    
    ## yield curve - 10y over 2y
    fxem.xat.yld.cv <- sig; fxem.xat.yld.cv[] <- NA
    txt <- paste(sQuote(ctry), collapse=",")
    y <- db.sql("SELECT date, ctry, yld2y, yld10y FROM res..ctry_macro_yld ",
                "WHERE ctry in ($txt) ",
                "AND date <= '$endt'", conn=rconn)        # yld database
    z <- g.ragged.to.array(y, c('date','ctry'), c('yld2y', 'yld10y'))
    for (n in 1:dim(z)[[3]]) {
        z[,,n] <- g.nafill(z[,,n], maxfill=63)          # fill missing yields for a quarter
    }
    colnames(z) <- names(ctry)[match(colnames(z), ctry)]  # change colnames to fx names
    if (endt > 19991231) {
        yc <- colMeans(z[max(1, nrow(z)-251):nrow(z),,'yld10y'], na.rm=T) -
            colMeans(z[max(1, nrow(z)-251):nrow(z),,'yld2y'], na.rm=T)   # 12M avg dif
        fxem.xat.yld.cv[match(names(yc),
                              names(fxem.xat.yld.cv))] <- -yc     # long fx with neg slp
    }
    
    ## yield momentum - 12M nominal 10Y yield momentum
    fxem.xat.yld.mom <- sig; fxem.xat.yld.mom[] <- NA
    if (endt > 19981231) {
        ym <- (z[nrow(z),,'yld10y'] -
               colMeans(z[max(1, nrow(z)-251):nrow(z),,'yld10y'], na.rm=T)) /
            abs(colMeans(z[max(1, nrow(z)-251):nrow(z),,'yld10y'], na.rm=T)) # 12M mom
        fxem.xat.yld.mom[match(names(ym),
                               names(fxem.xat.yld.mom))] <- ym   # long fx with higher yld
    }

    ## cds level
    fxem.xat.cds.lvl <- sig; fxem.xat.cds.lvl[] <- NA
    txt <- paste(sQuote(ctry), collapse=",")
    y <- db.sql("SELECT * FROM res..ctry_cds a LEFT JOIN res..ctry_cds_info b ",
                "ON a.idx=b.idx WHERE b.ctry in ($txt) AND a.date <= '$endt'",
                conn=rconn)
    cds <- g.ragged.to.array(y, c('date','ctry'), c('cds1y','cds3y','cds5y','cds10y'))
    for (n in 1:dim(cds)[[3]]) {
        cds[,,n] <- g.nafill(cds[,,n], maxfill=63)     # fill missing cds for a quarter
    }
    colnames(cds) <- names(ctry)[match(colnames(cds), ctry)]  # change to fx names
    if (endt > 20001231) {
        alf <- rowMeans(cds[nrow(cds), , ], na.rm=T)
        fxem.xat.cds.lvl[match(names(alf),
                             names(fxem.xat.cds.lvl))] <- alf  # long fx with high cds
    }
    
    ## cds momentum
    fxem.xat.cds.mom <- sig; fxem.xat.cds.mom[] <- NA
    if (endt > 20010630) {
        alf <- rowMeans(cds[nrow(cds), , ], na.rm=T) /
            rowMeans(cds[max(1, nrow(cds)-125), , ], na.rm=T)
        alf[alf==1] <- NA
        fxem.xat.cds.mom[match(names(alf),
                             names(fxem.xat.cds.mom))] <- alf    # long fx with high cds
    }

    ## cds curve
    fxem.xat.cds.cv <- sig; fxem.xat.cds.cv[] <- NA
    if (endt > 20011231) {
        cds.c <- cds[nrow(cds), , 'cds5y'] - cds[nrow(cds), , 'cds1y']  # cds slope
        fxem.xat.cds.cv[match(names(cds.c),
                            names(fxem.xat.cds.cv))] <- cds.c    # long fx with low cds spd
    }

    ## cds curve momentum
    fxem.xat.cds.cv.mom <- sig; fxem.xat.cds.cv.mom[] <- NA
    if (endt > 20011231) {        
        slp <- cds[max(1, nrow(cds)-251):nrow(cds), , 'cds5y'] -
            cds[max(1, nrow(cds)-251):nrow(cds), , 'cds1y']        
        alf <- slp[nrow(slp), ] / colMeans(slp, na.rm=T)   # high slope - less risk
        fxem.xat.cds.cv.mom[match(names(alf),
                                  names(fxem.xat.cds.cv.mom))] <- alf  # long fx
    }



###### macro indicators
    sd.eco <- g.date.idx(endt, T, -300, T, cal='usd')
    dts.eco <- dates.usd[g.inorder(sd.eco, dates.usd, endt)]

    ## cpi - surprise
    fxem.eco.cpi.sup <- sig; fxem.eco.cpi.sup[] <- NA
    if (endt > 19941231) {
        txt <- paste(sQuote(ctry), collapse=",")
        y <- db.sql("SELECT date, ctry, cpi FROM res..ctry_macro_eco ",
                    "WHERE ctry in ($txt) ",
                    "AND date <= '$endt'", conn=rconn)
        z <- g.ragged.to.array(y, c('date','ctry'), c('cpi'))
        cpi.liv <- g.nafill(z, maxfill=63)          # fill for a quarter
        cpi.est.mn <- mp.get.ce(sd.eco, endt, ctry, 'CPI', 'mn')  # mean est
        cpi.est.sd <- mp.get.ce(sd.eco, endt, ctry, 'CPI', 'sd')  # std est
        cpi.est.m <- cpi.est.s <- cpi.liv
        cpi.est.m[] <- cpi.est.s[] <- NA
        cpi.est.m[rownames(cpi.est.m) %in% rownames(cpi.est.mn), ] <-
            cpi.est.mn[rownames(cpi.est.mn) %in% rownames(cpi.est.m),
                       match(colnames(cpi.est.m), colnames(cpi.est.mn))]   # to daily matrix      
        cpi.est.m <- g.nafill(cpi.est.m, maxfill=63)                     # fill data
        cpi.est.s[rownames(cpi.est.s) %in% rownames(cpi.est.sd), ] <-     
            cpi.est.sd[rownames(cpi.est.sd) %in% rownames(cpi.est.s),
                       match(colnames(cpi.est.s), colnames(cpi.est.sd))]   # to daily matrix
        cpi.est.s <- g.nafill(cpi.est.s, maxfill=63)                     # fill data
        cpi.sup <- (cpi.liv - cpi.est.m) / cpi.est.s         # surprise
        cpi.s <- cpi.sup[nrow(cpi.sup), ]
        names(cpi.s) <- names(ctry)[match(names(cpi.s), ctry)]  # change to fx names
        fxem.eco.cpi.sup[match(names(cpi.s),
                               names(fxem.eco.cpi.sup))] <- cpi.s  # long fx with high cpi
    }

    ## gdp - surprise
    fxem.eco.gdp.sup <- sig; fxem.eco.gdp.sup[] <- NA
    if (endt > 19941231) {
        txt <- paste(sQuote(ctry), collapse=",")
        y <- db.sql("SELECT date, ctry, gdp FROM res..ctry_macro_eco ",
                    "WHERE ctry in ($txt) ",
                    "AND date <= '$endt'", conn=rconn)
        z <- g.ragged.to.array(y, c('date','ctry'), c('gdp'))
        gdp.liv <- g.nafill(z, maxfill=63)          # fill for a quarter
        gdp.est.mn <- mp.get.ce(sd.eco, endt, ctry, 'GDP', 'mn')  # mean est
        gdp.est.sd <- mp.get.ce(sd.eco, endt, ctry, 'GDP', 'sd')  # std est
        gdp.est.m <- gdp.est.s <- gdp.liv
        gdp.est.m[] <- gdp.est.s[] <- NA
        gdp.est.m[rownames(gdp.est.m) %in% rownames(gdp.est.mn), ] <-
            gdp.est.mn[rownames(gdp.est.mn) %in% rownames(gdp.est.m),
                       match(colnames(gdp.est.m), colnames(gdp.est.mn))]   # to daily matrix      
        gdp.est.m <- g.nafill(gdp.est.m, maxfill=63)                     # fill data
        gdp.est.s[rownames(gdp.est.s) %in% rownames(gdp.est.sd), ] <-     
            gdp.est.sd[rownames(gdp.est.sd) %in% rownames(gdp.est.s),
                       match(colnames(gdp.est.s), colnames(gdp.est.sd))]   # to daily matrix
        gdp.est.s <- g.nafill(gdp.est.s, maxfill=63)                     # fill data
        gdp.sup <- (gdp.liv - gdp.est.m) / gdp.est.s         # surprise
        gdp.s <- gdp.sup[nrow(gdp.sup), ]
        names(gdp.s) <- names(ctry)[match(names(gdp.s), ctry)]  # change to fx names
        fxem.eco.gdp.sup[match(names(gdp.s),
                             names(fxem.eco.gdp.sup))] <- gdp.s  # long fx with high gdp sup
    }

    ## fixed  investment - surprise
    fxem.eco.inv.sup <- sig; fxem.eco.inv.sup[] <- NA
    if (endt > 19941231) {        
        txt <- paste(sQuote(ctry), collapse=",")
        y <- db.sql("SELECT date, ctry, investment FROM res..ctry_macro_eco ",
                    "WHERE ctry in ($txt) ",
                    "AND date <= '$endt'", conn=rconn)
        z <- g.ragged.to.array(y, c('date','ctry'), c('investment'))
        inv.liv <- g.nafill(z, maxfill=63)          # fill for a quarter
        inv.est.mn <- mp.get.ce(sd.eco, endt, ctry, 'Fixed Investment', 'mn')  # mean est
        inv.est.sd <- mp.get.ce(sd.eco, endt, ctry, 'Fixed Investment', 'sd')  # std est
        inv.est.m <- inv.est.s <- inv.liv
        inv.est.m[] <- inv.est.s[] <- NA
        inv.est.m[rownames(inv.est.m) %in% rownames(inv.est.mn), ] <-
            inv.est.mn[rownames(inv.est.mn) %in% rownames(inv.est.m),
                       match(colnames(inv.est.m), colnames(inv.est.mn))]   # to daily matrix      
        inv.est.m <- g.nafill(inv.est.m, maxfill=63)                     # fill data
        inv.est.s[rownames(inv.est.s) %in% rownames(inv.est.sd), ] <-     
            inv.est.sd[rownames(inv.est.sd) %in% rownames(inv.est.s),
                       match(colnames(inv.est.s), colnames(inv.est.sd))]   # to daily matrix
        inv.est.s <- g.nafill(inv.est.s, maxfill=63)                     # fill data        
        inv.sup <- (inv.liv - inv.est.m) / inv.est.s         # surprise
        inv.s <- inv.sup[nrow(inv.sup), ]
        names(inv.s) <- names(ctry)[match(names(inv.s), ctry)]  # change to fx names
        fxem.eco.inv.sup[match(names(inv.s),
                               names(fxem.eco.inv.sup))] <- inv.s  # long fx with high inv
    }
    
    ## wage - uncertainty zscore
    fxem.eco.wge.unc <- sig; fxem.eco.wge.unc[] <- NA
    if (endt > 19980531) {        
        txt <- paste(sQuote(ctry), collapse=",")
        y <- db.sql("SELECT date, ctry, wage FROM res..ctry_macro_eco ",
                    "WHERE ctry in ($txt) ",
                    "AND date <= '$endt'", conn=rconn)
        z <- g.ragged.to.array(y, c('date','ctry'), c('wage'))
        wge.liv <- g.nafill(z, maxfill=63)          # fill for a quarter
        wge.est.mn <- mp.get.ce(sd.eco, endt, ctry, 'Wages', 'mn')  # mean est
        wge.est.sd <- mp.get.ce(sd.eco, endt, ctry, 'Wages', 'sd')  # std est
        wge.est.m <- wge.est.s <- wge.liv
        wge.est.m[] <- wge.est.s[] <- NA
        wge.est.m[rownames(wge.est.m) %in% rownames(wge.est.mn), ] <-
            wge.est.mn[rownames(wge.est.mn) %in% rownames(wge.est.m),
                       match(colnames(wge.est.m), colnames(wge.est.mn))]   # to daily matrix      
        wge.est.m <- g.nafill(wge.est.m, maxfill=63)                     # fill data
        wge.est.s[rownames(wge.est.s) %in% rownames(wge.est.sd), ] <-     
            wge.est.sd[rownames(wge.est.sd) %in% rownames(wge.est.s),
                       match(colnames(wge.est.s), colnames(wge.est.sd))]   # to daily matrix
        wge.est.s <- g.nafill(wge.est.s, maxfill=63)                     # fill data
        wge.unc <- wge.est.s / abs(wge.est.m)       # uncertainty
        wge.unc.z <- (wge.unc[nrow(wge.unc), ] -               # 6M zscore
                      colMeans(wge.unc[max(1, nrow(wge.unc)-125):nrow(wge.unc), ], na.rm=T)) /
            colStdevs(wge.unc[max(1, nrow(wge.unc)-125):nrow(wge.unc), ], na.rm=T)
        names(wge.unc.z) <- names(ctry)[match(names(wge.unc.z),
                                              ctry)]          # change colnames to fx names
        fxem.eco.wge.unc[match(names(wge.unc.z),
                               names(fxem.eco.wge.unc))] <- -wge.unc.z   # long fx w/ low w
    }
    
    ## interest rate - surprise
    fxem.eco.irt.sup <- sig; fxem.eco.irt.sup[] <- NA
    if (endt > 19980531) {        
        txt <- paste(sQuote(ctry), collapse=",")
        y <- db.sql("SELECT date, ctry, yld3m FROM res..ctry_macro_yld ",
                    "WHERE ctry in ($txt) ",
                    "AND date <= '$endt'", conn=rconn)
        z <- g.ragged.to.array(y, c('date','ctry'), c('yld3m'))
        irt.liv <- g.nafill(z, maxfill=63)          # fill for a quarter
        irt.est.mn <- mp.get.ce(sd.eco, endt, ctry, 'Interest Rate', 'mn')  # mean est
        irt.est.sd <- mp.get.ce(sd.eco, endt, ctry, 'Interest Rate', 'sd')  # std est
        irt.est.m <- irt.est.s <- irt.liv
        irt.est.m[] <- irt.est.s[] <- NA
        irt.est.m[rownames(irt.est.m) %in% rownames(irt.est.mn), ] <-
            irt.est.mn[rownames(irt.est.mn) %in% rownames(irt.est.m),
                       match(colnames(irt.est.m), colnames(irt.est.mn))]   # to daily matrix      
        irt.est.m <- g.nafill(irt.est.m, maxfill=63)                     # fill data
        irt.est.s[rownames(irt.est.s) %in% rownames(irt.est.sd), ] <-     
            irt.est.sd[rownames(irt.est.sd) %in% rownames(irt.est.s),
                       match(colnames(irt.est.s), colnames(irt.est.sd))]   # to daily matrix
        irt.est.s <- g.nafill(irt.est.s, maxfill=63)                     # fill data
        irt.sup <- (irt.liv - irt.est.m) / irt.est.s         # surprise
        irt.s <- irt.sup[nrow(irt.sup), ]
        names(irt.s) <- names(ctry)[match(names(irt.s), ctry)]  # change to fx names
        fxem.eco.irt.sup[match(names(irt.s),
                               names(fxem.eco.irt.sup))] <- irt.s  # long fx with high rate
    }



###### others
    ## volatility - 5Y
    fxem.msc.vlt <- sig; fxem.msc.vlt[] <- NA
    if (endt > 19961231) {
        alf <- apply(fxr[max(1, nrow(fxr)-252*5-1):nrow(fxr), ], 2,
                     sd, na.rm=T)                           # 5Y vol
        fxem.msc.vlt[match(names(alf),
                           names(fxem.msc.vlt))] <- alf   # long fx with high vol
    }

    ## WGI change
    fxem.msc.wgi <- sig; fxem.msc.wgi[] <- NA
    if (endt > 20010928) {
        txt <- paste(sQuote(ctry3),collapse=",")
        y <- db.sql("SELECT * FROM res..ctry_wgi WHERE ctry in ($txt) AND date <= '$endt'",
                    conn=rconn)                    # wgi database
        z <- g.ragged.to.array(y, c('date','ctry','indicator'), c('est','sde'))
        wgi.est <- wgi.est.adj <- fxr
        wgi.est[] <- wgi.est.adj[] <- NA  # average estimates and adjust estimates by se
        est.t <- apply(z[,,,'est'], c(1,2), mean, na.rm=T)    # estimates
        est.adj.t <- est.t / apply(z[,,,'sde'], c(1,2), mean, na.rm=T)     # est over se
        colnames(est.t) <- colnames(est.adj.t) <- names(ctry3)
        wgi.est[rownames(wgi.est) %in% rownames(est.t), colnames(est.t)] <- est.t
        wgi.est <- na.locf(wgi.est, na.rm=F)     # data is annual so fill daily data
        wgi.est.adj[rownames(wgi.est.adj) %in% rownames(est.adj.t),
                    colnames(est.adj.t)] <- est.adj.t
        wgi.est.adj <- na.locf(wgi.est.adj, na.rm=F)
        alf <- wgi.est[nrow(wgi.est), ] /
            colMeans(wgi.est[pmax(nrow(wgi.est)-252*c(1:4), 1), ]) - 1
        fxem.msc.wgi[match(names(alf),
                         names(fxem.msc.wgi))] <- -alf    # long fx with bad governance
    }
    
    ## momentum - price momentum
    fxem.msc.mom.pm <- sig; fxem.msc.mom.pm[] <- NA
    fx.t <- fxd[max(1, nrow(fxd)-20):nrow(fxd), , 'fx']
    fx.t.r <- fx.t[2:nrow(fx.t), ] / fx.t[1:(nrow(fx.t)-1), ] - 1   # return
    fx.t.v <- colStdevs(fx.t.r, na.rm=T)           # vol
    alf <- (fx.t[nrow(fx.t), ] - fx.t[1, ]) / fx.t[1, ] / fx.t.v         # risk adjusted
    alf[alf==0] <- NA
    fxem.msc.mom.pm[match(names(alf),
                          names(fxem.msc.mom.pm))] <- alf    # long fx with large momentum

    ## momentum - acceleration; 1m over 1y
    fxem.msc.mom.ac <- sig; fxem.msc.mom.ac[] <- NA
    fx.1 <- fxd[max(1, nrow(fxd)-20):nrow(fxd), , 'fx']
    fx1 <- (fx.1[nrow(fx.1), ] - fx.1[1, ]) / fx.1[1, ]
    fx.2 <- fxd[max(1, nrow(fxd)-20-251-20):max(1, nrow(fxd)-20-251), , 'fx']
    fx2 <- (fx.2[nrow(fx.2), ] - fx.2[1, ]) / fx.2[1, ]
    alf <- (fx1 - fx2) / abs(fx2)
    fxem.msc.mom.ac[match(names(alf),
                          names(fxem.msc.mom.ac))] <- alf    # long fx with acceleration



###### all signals and weights
    sigm <- rbind(fxem.opt.iv.lvl, fxem.opt.iv.crv.mom,
                  fxem.xat.eqt, fxem.xat.yld.cv, fxem.xat.yld.mom,
                  fxem.xat.cds.lvl, fxem.xat.cds.mom,
                  fxem.xat.cds.cv, fxem.xat.cds.cv.mom,
                  fxem.eco.cpi.sup, fxem.eco.gdp.sup,
                  fxem.eco.inv.sup, fxem.eco.wge.unc, fxem.eco.irt.sup,
                  fxem.msc.vlt, fxem.msc.wgi,
                  fxem.msc.mom.pm, fxem.msc.mom.ac)
    sigw <- suppressWarnings(apply(sigm, 1, mp.xsw))
    siglist <- list(sig=sigm, w=sigw)
    
    return(siglist)
}
