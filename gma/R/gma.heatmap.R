gma.heatmap <- function(endt=g.date.idx( ,TRUE,-1), perds=NULL, plot.it=TRUE, base="GM") {
    ## Assumes a graphic device is already open, NOT initiated.
    require(sand)
    bnam <- switch(base, MP="DMAP", GM="GMAP", UC="UMAP", base)
    if (inherits(endt, "Date")) endt <- g.date8(endt)           # In case Robert passes in a Date
    ## endt <- g.date.idx( ,T, -1, cal="mnth.end")

    stdts <- c(d1  = endt,
               d10 = g.date.idx(endt, T, -9),
               mtd = g.date.idx(endt, T, , cal="mnth.beg", later=F),
               qtd = g.date.idx(endt, T, , cal= "qtr.beg", later=F),
               ytd = g.date.idx(endt, T, , cal="year.beg", later=F))
    stdts <- if (is.null(perds)) stdts[!duplicated(stdts)] else stdts[perds]
    ## if (length(stdts) > 4) stdts <- stdts[1:4]
    dts <- g.date.idx(min(stdts), T, -1) %usd8% endt

    qd <- list()
    for (perd in names(stdts)) qd[[perd]] <- dts >= stdts[perd]

    ## assets <- c("Equity","Comdty","Rates","Curncy")
    ## premia <- c("Value","Volty","Carry","Momentum","Liquid","Defensive")
    assets <- c("BD","CM","EQ","FX")
    premia <- c("VAL","VOL","CAR","MOM","LIQ","DEF")
    ac <- ai <- av <- pt <- g.mk.array(c("Tot",premia), c("Tot",assets), names(stdts))
    size                 <- g.mk.array(c("Tot",premia), c("Tot",assets))
    pt[] <- F

    y <- pgSql("select * from sand_idx where port like '$base%'")
    y <- subset(y, sandid %in% pgSql("select p_sandid from sand_strategy")[[1]])
    for (ass in colnames(ac)) for (prem in rownames(ac)) {
        port <- sub("_+$","", gsub("_Tot","_", g.p("${base}_${ass}_$prem")))
        if (is.na(i <- match(port, y$port))) next
        p <- sandDperf(y$sandid[i], dts[1], endt, date8=T)
        p <- p[match(dts, p$date), c("date","tna_usd","dret")]
        effdt <- p$date[which(p$tna_usd > 200)[1]]            # Effective start date for strategy
        if (is.na(effdt)) next
        size[prem, ass] <- p$tna_usd[nrow(p)]
        p$ptna <- g.dflt(g.lag(p$tna_usd), p$tna_usd)                                 # Prior TNA
        if (port==base) ptna <- p$ptna
        for (perd in names(stdts)) {
            qdp <- qd[[perd]]
            if (effdt > dts[qdp][1]) {qdp <- qdp & (dts >= effdt);  pt[prem,ass,perd] <- T}
            pp <- p[qdp, ]
            ai[prem, ass, perd] <- x <- prod(1 + pp$dret, na.rm=T) - 1          # Internal return
            ac[prem, ass, perd] <- sum(pp$dret * pp$ptna / ptna[qdp], na.rm=T)          # Contrib
            if (port==base) ac[prem, ass, perd] <- x                    # Only compound top level
            av[prem, ass, perd] <- sqrt(252) * sd(pp$dret, na.rm=T)       # Annualized volatility
        }
    }
    if (!plot.it) return(ac)

    if (FALSE) {                                             # Make it match the official number?
        pp <- g.fund.perf("ALT_RP_G", stdts[["ytd"]], endt)
        offc <- prod(1 + pp$rtn) - 1
        ac[ , ,1] <- ac[ , ,1] + Z(size)/size[1,1] * (offc - ac[1,1,1])
    }

    ## g.printer(file="DMAP_ytd_heatmap.png")
    ## par(oma=c(0,0,2,0), mar=c(2, 2, 2.5, 1), xpd=T)               # if just one plot, eg "ytd"
    par(oma=c(0,0,2,0), mar=c(2, 2, 2.5, 1), xpd=T, mfrow=c(2, ifelse(length(stdts)>2, 2,1)))
    v <- 0:50/50
    palette(c(hsv(1, 1-v, 1), hsv(1/3, v, 1)[-1], "#E0E0E0"))   # 101 colors red to green, + gray
    for (perd in names(stdts)) {
        g.plot.init(xaxs="i", yaxs="i")
        if (perd %in% names(stdts)[c(1,5)])
          mtext(paste(bnam, "Performance Attribution Through", g.fmt(endt,"d")), cex=1.5,outer=T)
        title(perd)
        for (i in 1:7) for (j in 1:5) {
            col <- g.dflt(g.bound(round((ac[i,j,perd] + .01) * 5000 + 1), 1, 101), 102)
            rect((j-1)/5, (7-i)/7, j/5, (8-i)/7, col=col)
            hdr <- if (i==1 && j==1) base else if (i==1) assets[j-1] else premia[i-1]
            if (i==1 || j==1) text((j-.5)/5, (7.8-i)/7, hdr, font=2) else {        # Hdr or ports
                if (is.na(ac[i,j,perd])) next
                port <- g.p("${base}_$ass_$prem", ass=assets[j-1], prem=premia[i-1])
                stbl <- subset(sandId(port, T, T), live)
                txt <- paste(stbl$port, collapse="\n")
                text((j-.5)/5, (7.9-i)/7, txt, cex=.7, col="blue", adj=c(.5,1))
            }
            text((j-.5)/5, (7.4-i)/7, g.fmt(ac[i,j,perd], scl=1e4, dec=0, plus=T, na=""))
            s <- g.fmt(ai[i,j,perd], scl=1e4, dec=0, plus=T,  na="") %&% if (pt[i,j,perd]) "*"
            text((j-.8)/5, (7.1-i)/7, s, cex=.7, col="blue")
            s <- g.fmt(av[i,j,perd], scl=100, dec=1, pct="%", na="") %&% if (pt[i,j,perd]) "*"
            text((j-.2)/5, (7.1-i)/7, s, cex=.7, col="blue")
        }
        segments(0, 6/7, 1, 6/7, lwd=2);  segments(1/5, 0, 1/5, 1, lwd=2)
    }
}
