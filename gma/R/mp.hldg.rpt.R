## First attempt at daily holdings report
mp.hldg.rpt <- function(base="MP", today=g.date8()) {
    require(mt); require(sand); require(gma)
    m.load("m.secref", dt=today)
    kl <- list(items=c("shares","mkt_val_usd","ctg"), typ=c("n","n","c"), scl=c(1,1,1),
               dec=c(0,0,0), hdr=c("Shares","MktVal ($)","Category"))
    yl <- fl <- list()

    yest <- g.date.idx(today, T, -1)
##    csev <- db.sql("select * from csev..erisk_report_sec_info_hist where fund_date='$yest'",
##                   " and fund in ('GC5COM', 'GC5VOL', 'MANFUT') order by fund")

    ## Loop on Asset Class (acls):
    for (acls in c("BD","CM","EQ","FX")) {
        if (base=="UC" && acls=="CM") next
        ports <- switch(acls, BD=c("BDVAL","BDCAR","CDCAR","BDMOM","BDLIQ","BDDEF"),
                              CM=c("CMVALBC","CMVALSD","CMCAR","CMMOM","CMCOT","CMDEF"),
                              EQ=c("EQCAR","EQMOM","EQCOT"),
                              FX=c("FXVAL","FXCAR","FXMOM","FXCOT","FXDEF"))
        ports <- paste0(base, ports)
        h <- sandHoldings(ports, today)
        if (!nrow(h)) next
        h$port <- sandId(h$sandid, T)$port
        h$name <- m.secref$sec.name[match(h$sec_id, m.secref$sec.id)]

        ## Category:
        reg <- c(USD="US", CAD="Canada", EUR="Europe", CHF="Europe", GBP="Europe",
                 AUD="Asia", JPY="Asia", HKD="Asia", SGD="Asia", ZAR="Africa")
        h$ctg <- reg[h$curncy]
        ctgord <- unique(reg)
        if (acls=="CM") {
            uly <- sub("_","", substr(h$symbol,1,2))
            cminfo <- g.sql("select * from gma..cmdty_info")
            h$ctg <- ifelse(uly=="LM", "Base Metals", cminfo$category[match(uly, cminfo$cmdty)])
            ctgord <- c("Petroleum","Natural Gas","Base Metals","Precious Metals",
                        "Livestock","Grains","Oils","Softs")
        }
        if (acls=="FX") {
            h$ctg <- ifelse(substr(h$symbol,1,3) %in% G10, "G10","EM")
            ctgord <- c("G10","EM")
        }
        h <- h[order(match(h$ctg, ctgord), h$symbol),
               c("ctg","symbol","port","name","shares","mkt_val_usd")]

        ## Separate out CDS from "bd" and Dividend Futures from "eq":
        if (acls=="BD") {
            hc <- subset(h, substr(symbol,1,4) %in% c("CDX.","ITRX"))
            h  <- subset(h, symbol %ni% hc$symbol)
            rpt <- g.add.totals(hc, "ctg", top=F)
            fmt <- list(title="CDS", n=1, klist=kl, nscr=2)
            yl <- c(yl, list(rpt));  fl <- c(fl, list(fmt))
        }
        if (acls=="EQ") {
            hd <- subset(h, substr(symbol,1,3) %in% c("ASD","DED","MND"))
            h  <- subset(h, symbol %ni% hd$symbol)
            if (nrow(h) > 0) {
                rpt <- g.add.totals(hd, "ctg", top=F)
                fmt <- list(title="Dividend Futures", n=1, klist=kl, nscr=nrow(hd))
                yl <- c(yl, list(rpt));  fl <- c(fl, list(fmt))
            }
        }

        ## Add BRM data from CSEV database?
        ## idx <- match(h$symbol, csev$symbol)

        rpt <- g.totby(h, c("ctg","symbol","port"))
        titl <- switch(acls, BD="Bonds", CM="Commodities", EQ="Equities", FX="Currencies")
        nscr <- rep(0, nrow(rpt))
        nscr[which(rpt$port  =="Total")] <- 1
        nscr[which(rpt$symbol=="Total")] <- 2
        ## nscr[which(!duplicated(substr(h$symbol,1,2)))[-1] - 1] <- 1
        ## nscr[which(!duplicated(h$ctg))[-1] - 1] <- 2
        shade <- matrix("0", nrow(rpt), ncol(rpt))
        shade[nscr==1, ] <- "s"         # Salmon
        shade[nscr==2, ] <- "y"         # Yellow
        shade[rpt$ctg=="Total", ] <- "Y"
        fmt <- list(title=titl, n=1, klist=kl, nscr=nscr, shade=shade)
        yl <- c(yl, list(rpt, "newpage"));  fl <- c(fl, list(fmt, 0))
    }
    if (!length(yl)) {yl <- data.frame(symbol="None"); fl <- list()}
    titl <- base %&% " Holdings, ex xxVOL/CMLIQ/EQVAL/EQLIQ/EQDEF, as of " %&%
      g.fmt(g.date.idx(today,T,-1), "d")
    g.rpt.tex(yl, fl, tolower(base) %&% ".hldg", supertitle=titl, print.it="pdf")
}
