gma.tlthdg <- function(pod, recip=NULL, html=FALSE, basketid=0, htype="r") {
    require(bbdotnet3); require(sand); require(gma)
    tdel <- .50                                # Target delta; hedge if you go outside this range
    tc <- textConnection("txt", "w", local=TRUE)
    wts <- VOL.WTS.BD

    tna <- sandTna("FI_STR", addInvest=TRUE) * VOL.LEVG["bd"]
    h <- sandHoldings("FI_STR", type=htype)[c("symbol","shares")]
    h$qfut <- nchar(h$symbol) <= 5
    h <- h[order(h$qfut, h$symbol), ];  rownames(h) <- NULL

    sref <- db.sql("select symbol, bb_id, multiplier, maturity_date from geode..secref",
                   " where symbol in $syms", syms=g.comma.list(h$symbol,TRUE))
    for (i in names(sref)[-1]) h[[i]] <- sref[[i]][match(h$symbol, sref$symbol)]
    h$bb.id <- paste(h$bb.id, ifelse(substr(h$symbol, 1,3)=="TLT", "Equity", "Comdty"))

    h$uly <- ifelse(substr(h$symbol,1,3)=="TLT", "TLT", substr(h$symbol, 1,4))
    h$tnm <- ifelse(h$uly=="TLT", "TLT", substr(h$uly, 1,2))
    currs <- c(TLT="USD", US="USD",TY="USD",FV="USD",TU="USD", RX="EUR",OE="EUR",DU="EUR")
    fx <- db.sql("select currency, fx from geode..fx")
    h$fx <- fx$fx[match(g.dflt(currs[h$tnm], "USD"), fx$currency)]

    ho <- subset(h, !qfut & maturity.date != g.date8())              # Options not expiring today
    bb <- getBbRealTime(c("DELTA_MID","OPT_UNDL_PX"), ho$bb.id, pod=pod)
    ho[ ,c("delta","S")] <- bb
    for (i in which(is.na(ho$delta))) {           # Approximate delta for deep ITM or OTM options
        K <- as.double(strsplit(ho$symbol[i], "-")[[1]][2])
        cp <- g.parse(g.parse(ho$symbol[i], "-")$base)$ext
        ho$delta[i] <- if (cp=="C" && ho$S[i] > K) 1 else if (cp=="P" && ho$S[i] < K) -1 else 0
    }

    for (mytnm in unique(ho$tnm)) {
        hoi <- subset(ho, tnm==mytnm)
        wti <- wts[mytnm]
        hoi$wt <- with(hoi, shares * multiplier * fx * S / (wti * tna))
        delta <- with(hoi, sum(delta * wt))
        if (is.na(delta)) {cat(mytnm, "Delta failure!\n"); next}
        ulyMult <- switch(mytnm, FXE=,FXY=,FXB=,TLT=1, hoi$multiplier[1])

        tgtDelta <- if (abs(delta) < tdel) 0 else (-delta + tdel*sign(delta))
        targ <- tgtDelta * wti * tna / (ulyMult * hoi$S[1])           # Target futures contracts
        upos <- with(subset(h, qfut & tnm==mytnm), sum(shares))
        symbol <- hoi$uly[1]
        trd.shs <- round((targ-upos))
        cat("FI_STR", mytnm, ": Option delta is", round(delta,2),
            ", current hedge is", upos, ", desired hedge is", round(targ,1),
            ", the trade is", g.fmt(trd.shs, dec=0, plus=T), symbol, "\n", file=tc)
        if (basketid && trd.shs != 0) {
            y <- data.frame(symbol=symbol, shares=trd.shs, comment="[FI_STR]")
            sandTrd(y, "FI_STR", basketid=basketid, instructions="MKT")
        }
    }
    close(tc)
    if (!is.null(recip)) g.mail(recip, "[GMA] FI_STR Hedge", txt)
    if (html) cat(txt, sep="\n")
}
