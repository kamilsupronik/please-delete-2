suppressMessages({library(gma); library(bbdotnet3); library(zoo); library(parallel)})
options(stringsAsFactors=FALSE, warn=1)

source(system.file("mf.adddata.R", package="gma"))
## Sys.setenv(DBDSN="ciq", DBUSER="gcmquant", DBPWD="Plusm1nusSQL")
## g.connect.sql()
rconn <- g.connect.sql("ciq", "gcmquant", "Plusm1nusSQL", global=F)       # prod
dconn <- g.connect.sql("ciqdev", "gcmquant", "Plusm1nusSQL", global=F)    # dev


endt <- g.date.idx( , TRUE, -1)

## Get BB connections
# no.adj.pod <- tryPod(c("MAX","SH"), test=T)
adj.pod    <- tryPod(c("DB","FB"), test=T)

## Updates the equity database  #moved to alpha/signals/ctry.dataupdate.R
y <- db.sql("SELECT a.eqty [eqty], COALESCE(max(b.date), '19730102') [date]",
            " FROM res.dbo.mfeq_info a LEFT JOIN res.dbo.mfeq b ON a.eqty=b.eqty GROUP BY a.eqty",
            conn=rconn)
y <- subset(y, date < endt & date >= g.date.idx(endt, TRUE, -21))        # Need data but not dead
no.adj.pod <- if (all(g.date.idx(y$date, T, +1) == endt)) adj.pod else tryPod(NO.ROLL.ADJUST.POD)
if (identical(no.adj.pod, FALSE)) stop("Could not connect to an unadjusted pod!")

for (i in 1 %:% nrow(y)) {                                                         # Get new data
    eqty <- y$eqty[i]
    cat(eqty, "\n")
    stdt <- g.date.idx(y$date[i], TRUE, +1)
    # test to see if the ticker exists
    testtk <- eqty
    idx <- nchar(eqty) == 1                        
    if(any(idx)) testtk[idx] <- eqty[idx] %&% " " 
    bbtk <- paste(testtk %&% "1", "Index")
    existtk <- getBbHistorical(stdt, endt, "fut_cur_gen_ticker",bbtk, pod=no.adj.pod)
    if (is.null(existtk) || !nrow(existtk)) {
      ## if ticker does not exist, then skip
    } else {
      a <- add.data(stdt, endt, eqty, "eqty", "Index", "", 1:2, adj.pod, no.adj.pod)  # data grabbing
      dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.mfeq WHERE eqty='${eqty}' ",
                        "AND date > '${stdt}' AND date < '${endt}'"), conn=rconn)   # detect duplicates
      if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.mfeq", append=TRUE, conn=rconn)  # update database

      dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.mfeq WHERE eqty='${eqty}' ",
                        "AND date > '${stdt}' AND date < '${endt}'"), conn=dconn)   # detect duplicates
      if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.mfeq", append=TRUE, conn=dconn)  # update database
    }
}


# ## Updates the bond database #moved to vlt.dataupdate.R
# y <- db.sql("SELECT a.bond [bond], a.yield [yield], COALESCE(max(b.date), '19730102') [date]",
            # " FROM res.dbo.mfbd_info a LEFT JOIN res.dbo.mfbd b ON a.bond=b.bond GROUP BY a.bond, a.yield",
            # conn=rconn)
# y <- subset(y, date < endt & date >= g.date.idx(endt, TRUE, -21))        # Need data but not dead
# no.adj.pod <- if (all(g.date.idx(y$date, T, +1) == endt)) adj.pod else tryPod(NO.ROLL.ADJUST.POD)
# if (identical(no.adj.pod, FALSE)) stop("Could not connect to an unadjusted pod!")

# stir <- c('ER', 'L', 'ED', 'BA', 'IR')     # short term interest rate futures

# for (i in 1 %:% nrow(y)) {                                                         # Get new data
    # bond <- y$bond[i]
    # cat(bond, "\n")
    # stdt <- g.date.idx(y$date[i], TRUE, +1)
    # # test to see if the ticker exists
    # testtk <- bond
    # idx <- nchar(bond) == 1                        
    # if(any(idx)) testtk[idx] <- bond[idx] %&% " " 
    # bbtk <- paste(testtk %&% "1", "Comdty")
    # existtk <- getBbHistorical(stdt, endt, "fut_cur_gen_ticker",bbtk, pod=no.adj.pod)
    # if (is.null(existtk) || !nrow(existtk)) {
      # ## if ticker does not exist, then skip
    # } else {
      # if (bond %in% stir) {
          # suffixes <- 1:10
      # } else {
          # suffixes <- 1:2
      # }
      # a <- add.data(stdt, endt, bond, "bond", "Comdty",
                    # y$yield[i], suffixes, adj.pod, no.adj.pod)  ## data grabbing
      # dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.mfbd WHERE bond='${bond}' ",
                        # "AND date > '${stdt}' AND date < '${endt}'"), conn=rconn)   ## detect duplicates
      # if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.mfbd", append=TRUE, conn=rconn)  ## update database
      # dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.mfbd WHERE bond='${bond}' ",
                        # "AND date > '${stdt}' AND date < '${endt}'"), conn=dconn)   ## detect duplicates
      # if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.mfbd", append=TRUE, conn=dconn)  ## update database
    # }
# }


## Updates the dividend futures database
y <- db.sql("SELECT a.dvd [dvd], COALESCE(max(b.date), '19730102') [date]",
            " FROM res.dbo.dvdfut_info a LEFT JOIN res.dbo.dvdfut b ON a.dvd=b.dvd GROUP BY a.dvd",
            conn=rconn)
y <- subset(y, date < endt & date >= g.date.idx(endt, TRUE, -21))        # Need data but not dead
no.adj.pod <- if (all(g.date.idx(y$date, T, +1) == endt)) adj.pod else tryPod(NO.ROLL.ADJUST.POD)
if (identical(no.adj.pod, FALSE)) stop("Could not connect to an unadjusted pod!")

for (i in 1 %:% nrow(y)) {                                                         # Get new data
    dvd <- y$dvd[i]
    cat(dvd, "\n")
    stdt <- g.date.idx(y$date[i], TRUE, +1)
    if (dvd == 'ASD') suffixes <- 1:6 else suffixes <- 1:10
    a <- add.data(stdt, endt, dvd, "dvd", "Index", "", suffixes, adj.pod, no.adj.pod)  ## data grabbing
    dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.dvdfut WHERE dvd='${dvd}' ",
                      "AND date > '${stdt}' AND date < '${endt}'"), conn=rconn)   ## detect duplicates
    if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.dvdfut", append=TRUE, conn=rconn)  ## update database
    dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.dvdfut WHERE dvd='${dvd}' ",
                      "AND date > '${stdt}' AND date < '${endt}'"), conn=dconn)   ## detect duplicates
    if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.dvdfut", append=TRUE, conn=dconn)  ## update database
}


## update credit data
stdt <- g.date.idx(endt, T, -3, T, cal='usd')

tk <- c('CDX HY CDSI GEN 5Y SPRD CORP','IBOXUMAE MSG1 CURNCY','ITRX XOVER CDSI GEN 5Y CORP',
        'ITRX EUR CDSI GEN 5Y CORP','VIX INDEX','V2X INDEX','BCAUOAS INDEX','LP02OAS INDEX')
alld <- as.data.frame(getBbHistorical(stdt, endt, 'PX_Last', tk, pod=adj.pod))
alld$date <- g.date8(rownames(alld))
## usccc <- read.csv(file="/gcm/sys2home/gcmcommon/DevPod/vixccc.csv")
## usccc$date <- g.date8(usccc$date)
## finald <- merge(x=alld, y=usccc, by='date', all.x=T)
## finald <- as.matrix(finald)
finald <- as.matrix(alld)
finald <- na.locf(finald, na.rm=F)
colnames(finald) <- c('USHYspd', 'USIGspd', 'EUHYspd', 'EUIGspd', 'VIX', 'V2X', 'USccc', 'EUccc', 'date')
finald <- finald[, c('date', 'USHYspd', 'USIGspd', 'EUHYspd', 'EUIGspd', 'VIX', 'V2X', 'USccc', 'EUccc')]

existd <- db.sql("SELECT * from res..credit WHERE date ",
                 "between '$stdt' and '$endt' order by date", conn=rconn)
lastd <- which(finald[,'date']==existd$date[nrow(existd)])
if (lastd < nrow(finald)) {
    updated <- as.data.frame(finald[(lastd+1):nrow(finald), ,drop=FALSE])
    if (!is.null(updated)) {
        g.put.sql(updated, "res.dbo.credit", append=TRUE, conn=rconn)
        g.put.sql(updated, "res.dbo.credit", append=TRUE, conn=dconn)
    }
}


## Updates the commodities production data
y <- db.sql("SELECT a.comdty [comdty], a.supply [supply], a.demand [demand], ",
            " a.inventory [inventory], a.storage [storage], max(b.date) [date]",
            " FROM res.dbo.cmdata_info a LEFT JOIN res.dbo.cmdata b ON a.comdty=b.comdty",
            " GROUP BY a.comdty, a.supply, a.demand, a.inventory, a.storage", conn=rconn)

for (i in 1 %:% nrow(y)) {                                                         # Get new data
    cmdty <- y$comdty[i]
    cat(cmdty, "\n")
    cp.date <- g.date.idx( , TRUE, -1, cal="cal.mnth.end")
    co.date <- g.date.idx(cp.date, T, -3, cal="cal.mnth.end")
    nln <- sum(sum(y[i,]!=""))
    if (cmdty == "NG") {
        a <- getBbHistorical(co.date, endt, "px_last", y[i, c(2,3,5)], "Index", adj.pod)
        a <- na.locf(a, na.rm=F)
    } else {
        a <- getBbHistorical(co.date, cp.date, "px_last", y[i, 2:(nln-1)], "Index", adj.pod)
    }
    a <- as.data.frame(a[nrow(a), ,drop =F])
    if (nrow(a)==0) next
    a$date <- endt
    rownames(a) <- NULL
    a$asset <- y[i, 1]
    if (ncol(a) == 5) {
        if (cmdty == "NG") {      # NG has storage data
            a$inventory <- NA
            colnames(a) <- c('supply','demand','storage','date','asset','inventory')
            a <- a[, c('date','asset','supply','demand','inventory','storage')]
        } else {
            colnames(a) <- c('supply','demand','inventory','date','asset')
            a <- a[, c('date','asset','supply','demand','inventory')]
            a$storage <- NA
        }
    } else {
        colnames(a) <- c('supply','demand','date','asset')
        a <- a[, c('date','asset','supply','demand')]
        a$inventory <- NA
        a$storage <- NA
    }
    dup <- db.sql(g.p("SELECT * FROM res.dbo.cmdata WHERE comdty='${cmdty}' ",
                      "AND date >= '${cp.date}' AND date <= '${endt}'"), conn=rconn)   ## detect duplicates
    a.new <- a[!(g.date8(a$date) %in% dup$date), ]
    if (nrow(dup)!=0) {
        a.new[is.na(a.new)] <- dup[nrow(dup), is.na(a.new)]
    }
    if (nrow(a.new)!=0) g.put.sql(a.new, "res.dbo.cmdata", append=TRUE, conn=rconn)

    dup <- db.sql(g.p("SELECT * FROM res.dbo.cmdata WHERE comdty='${cmdty}' ",
                      "AND date >= '${cp.date}' AND date <= '${endt}'"), conn=dconn)   ## detect duplicates
    a.new <- a[!(g.date8(a$date) %in% dup$date), ]
    if (nrow(dup)!=0) {
        a.new[is.na(a.new)] <- dup[nrow(dup), is.na(a.new)]
    }
    if (nrow(a.new)!=0) g.put.sql(a.new, "res.dbo.cmdata", append=TRUE, conn=dconn)
}


# ## Updates the macro data  #moved to vlt.dataupdate.R
# y <- db.sql("SELECT a.ctry [ctry], a.gdplevel [gdplevel], a.gdpqoq [gdpqoq], ",
            # " a.gdpyoy [gdpyoy], max(b.date) [date]",
            # " FROM res.dbo.macro_info a LEFT JOIN res.dbo.macro_data b ON a.ctry=b.ctry",
            # " GROUP BY a.ctry, a.gdplevel, a.gdpqoq, a.gdpyoy", conn=rconn)

# for (i in 1 %:% nrow(y)) {                                                         # Get new data
    # ctry <- y$ctry[i]
    # cat(ctry, "\n")
    # cp.date <- g.date.idx( , TRUE, -1, cal="cal.qtr.end")
    # co.date <- g.date.idx(cp.date, T, -2, cal="cal.qtr.end")
    # a <- getBbHistorical(co.date, cp.date, "px_last", y[i, 2:(ncol(y)-1)], "Index", adj.pod)
    # a <- as.data.frame(a[nrow(a), ,drop =F])
    # a$date <- endt
    # rownames(a) <- NULL
    # a$asset <- y[i, 1]
    # colnames(a) <- c('gdplevel','gdpqoq','gdpyoy','date','ctry')
    # a <- a[, c('date','ctry','gdplevel','gdpqoq','gdpyoy')]
    
    # dup <- db.sql(g.p("SELECT * FROM res.dbo.macro_data WHERE ctry='${ctry}' ",
                      # "AND date >= '${cp.date}' AND date <= '${endt}'"), conn=rconn)   ## detect duplicates
    # a.new <- a[!(g.date8(a$date) %in% dup$date), ]
    # if (nrow(dup)!=0) {
        # a.new[is.na(a.new)] <- dup[nrow(dup), is.na(a.new)]
    # }
    # if (nrow(a.new)!=0) g.put.sql(a.new, "res.dbo.macro_data", append=TRUE, conn=rconn)

    # dup <- db.sql(g.p("SELECT * FROM res.dbo.macro_data WHERE ctry='${ctry}' ",
                      # "AND date >= '${cp.date}' AND date <= '${endt}'"), conn=dconn)   ## detect duplicates
    # a.new <- a[!(g.date8(a$date) %in% dup$date), ]
    # if (nrow(dup)!=0) {
        # a.new[is.na(a.new)] <- dup[nrow(dup), is.na(a.new)]
    # }
    # if (nrow(a.new)!=0) g.put.sql(a.new, "res.dbo.macro_data", append=TRUE, conn=dconn)
# }


## Updates the commodities extra futures database
y <- db.sql("SELECT max(date) [date], cmdty FROM res.dbo.cmet GROUP BY cmdty", conn=rconn)
y <- subset(y, date < endt & date >= g.date.idx(endt, TRUE, -21))        # Need data but not dead
no.adj.pod <- if (all(g.date.idx(y$date, T, +1) == endt)) adj.pod else tryPod(NO.ROLL.ADJUST.POD)
if (identical(no.adj.pod, FALSE)) stop("Could not connect to an unadjusted pod!")

for (i in 1 %:% nrow(y)) {                                                         # Get new data
    cmdty <- y$cmdty[i]
    cat(cmdty, "\n")
    stdt <- g.date.idx(y$date[i], TRUE, +1)
    a <- add.comdty(stdt, endt, cmdty, 7:8, adj.pod, no.adj.pod)
    
    dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.cmet WHERE cmdty='${cmdty}' ",
                      "AND date > '${stdt}' AND date < '${endt}'"), conn=rconn)   ## detect duplicates
    if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.cmet", append=TRUE, conn=rconn)  ## update database
    
    dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.cmet WHERE cmdty='${cmdty}' ",
                      "AND date > '${stdt}' AND date < '${endt}'"), conn=dconn)   ## detect duplicates
    if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.cmet", append=TRUE, conn=dconn)  ## update database
}


## Updates the fx_eco CA over GDP data
y <- db.sql("SELECT a.ctry [ctry], a.ca_over_gdp [ca_over_gdp], max(b.date) [date]",
            " FROM res.dbo.fx_eco_info a LEFT JOIN res.dbo.fx_eco_data b ON a.ctry=b.ctry",
            " GROUP BY a.ctry, a.ca_over_gdp", conn=rconn)

for (i in 1 %:% nrow(y)) {                                                         # Get new data
    ctry <- y$ctry[i]
    cat(ctry, "\n")
    cp.date <- g.date.idx( , TRUE, -1, cal="cal.qtr.end")
    co.date <- g.date.idx(cp.date, T, -2, cal="cal.qtr.end")
    a <- getBbHistorical(co.date, cp.date, "px_last", y[i, 2:(ncol(y)-1)], "Index", adj.pod)
    a <- as.data.frame(a[nrow(a), ,drop =F])
    a$date <- g.date8(rownames(a))
    rownames(a) <- NULL
    a$asset <- y[i, 1]
    colnames(a) <- c('ca_over_gdp','date','ctry')
    a <- a[, c('date','ctry','ca_over_gdp')]
    
    dup <- db.sql(g.p("SELECT * FROM res.dbo.fx_eco_data WHERE ctry='${ctry}' ",
                      " AND date >= '${co.date}' AND date <= '${endt}'"), conn=rconn)   ## detect duplicates    
    a.new <- a[!(g.date8(a$date) %in% dup$date) , ]
    if (nrow(dup)!=0) {
        a.new[is.na(a.new)] <- dup[nrow(dup), is.na(a.new)]
    }
    if (nrow(a.new)!=0) g.put.sql(a.new, "res.dbo.fx_eco_data", append=TRUE, conn=rconn)

    dup <- db.sql(g.p("SELECT * FROM res.dbo.fx_eco_data WHERE ctry='${ctry}' ",
                      " AND date >= '${co.date}' AND date <= '${endt}'"), conn=dconn)   ## detect duplicates    
    a.new <- a[!(g.date8(a$date) %in% dup$date) , ]
    if (nrow(dup)!=0) {
        a.new[is.na(a.new)] <- dup[nrow(dup), is.na(a.new)]
    }
    if (nrow(a.new)!=0) g.put.sql(a.new, "res.dbo.fx_eco_data", append=TRUE, conn=dconn)
}


# ## update country level CDS data - DB's pod only  #moved to alpha/signals/ctry.dataupdate.R
# y <- db.sql("SELECT a.idx [idx], a.cds1y [cds1y], a.cds3y [cds3y], a.cds5y [cds5y], ",
            # " a.cds10y [cds10y], max(b.date) [date]",
            # " FROM res.dbo.ctry_cds_info a LEFT JOIN res.dbo.ctry_cds b ON a.idx=b.idx",
            # " GROUP BY a.idx, a.cds1y, a.cds3y, a.cds5y, a.cds10y", conn=rconn)

# for (i in 1 %:% nrow(y)) {                                                         # Get new data
    # idx <- y$idx[i]
    # cat(idx, "\n")
    # stdt <- g.date.idx(endt, T, -21, T)
    # a <- getBbHistorical(stdt, endt, "px_last", y[i, 2:(ncol(y)-1)], "Curncy",
                         # tryPod(c("DB","VDB","GL"), test=T))    # should only use DB's pod; use GL when DB not here
    # if (nrow(a)!=0) {
        # ## a <- as.data.frame(a[nrow(a), ,drop =F])
        # a <- as.data.frame(a)
        # a$date <- rownames(a)
        # rownames(a) <- NULL
        # a$idx <- y[i, 1]
        # colnames(a) <- c('cds1y','cds3y','cds5y','cds10y','date','idx')
        # a <- a[, c('date','idx','cds1y','cds3y','cds5y','cds10y')]
        
        # dup <- db.sql(g.p("SELECT * FROM res.dbo.ctry_cds WHERE idx='${idx}' ",
                          # "AND date >= '${stdt}' AND date <= '${endt}'"), conn=rconn)    # detect duplicates
        # a.new <- a[!(g.date8(a$date) %in% dup$date), ]
        # if (nrow(a.new)!=0) g.put.sql(a.new, "res.dbo.ctry_cds", append=TRUE, conn=rconn)

        # dup <- db.sql(g.p("SELECT * FROM res.dbo.ctry_cds WHERE idx='${idx}' ",
                          # "AND date >= '${stdt}' AND date <= '${endt}'"), conn=dconn)    # detect duplicates
        # a.new <- a[!(g.date8(a$date) %in% dup$date), ]
        # if (nrow(a.new)!=0) g.put.sql(a.new, "res.dbo.ctry_cds", append=TRUE, conn=dconn)
    # }
#}


# ## update country yield data  #moved to alpha/signals/ctry.dataupdate.R
# y <- db.sql("SELECT a.ctry [ctry], a.yld3m [yld3m], a.yld2y [yld2y], a.yld10y [yld10y], ",
            # " max(b.date) [date]",
            # " FROM res.dbo.ctry_macro_info a LEFT JOIN res.dbo.ctry_macro_yld b ON a.ctry=b.ctry",
            # " GROUP BY a.ctry, a.yld3m, a.yld2y, a.yld10y", conn=rconn)

# for (i in 1 %:% nrow(y)) {                                                         # Get new data
    # ctry <- y$ctry[i]
    # cat(ctry, "\n")
    # stdt <- g.date.idx(endt, T, -21, T)
    # a <- getBbHistorical(stdt, endt, "px_last", y[i, 2:(ncol(y)-1)], "Index", adj.pod)
    # if (nrow(a)!=0) {
        # ## a <- as.data.frame(a[nrow(a), ,drop =F])
        # a <- as.data.frame(a)
        # a$date <- rownames(a)
        # rownames(a) <- NULL
        # a$ctry <- y[i, 1]
        # colnames(a) <- c('yld3m','yld2y','yld10y','date','ctry')
        # a <- a[, c('date','ctry','yld3m','yld2y','yld10y')]
        
        # dup <- db.sql(g.p("SELECT * FROM res.dbo.ctry_macro_yld WHERE ctry='${ctry}' ",
                          # "AND date >= '${stdt}' AND date <= '${endt}'"), conn=rconn)   # detect duplicates
        # a.new <- a[!(g.date8(a$date) %in% dup$date), ]
        # if (nrow(a.new)!=0) g.put.sql(a.new, "res.dbo.ctry_macro_yld", append=TRUE, conn=rconn)

        # dup <- db.sql(g.p("SELECT * FROM res.dbo.ctry_macro_yld WHERE ctry='${ctry}' ",
                          # "AND date >= '${stdt}' AND date <= '${endt}'"), conn=dconn)   # detect duplicates
        # a.new <- a[!(g.date8(a$date) %in% dup$date), ]
        # if (nrow(a.new)!=0) g.put.sql(a.new, "res.dbo.ctry_macro_yld", append=TRUE, conn=dconn)
    # }
# }


# ## Update countries' economic indicators #moved to alpha/signals/ctry.dataupdate.R
# y <- db.sql("SELECT a.ctry [ctry], a.gdp [gdp], a.production [production], a.consumption [consumption], ",
            # " a.investment [investment], a.cpi [cpi], a.wage [wage], ",
            # " a.unemployment [unemployment], max(b.date) [date]",
            # " FROM res.dbo.ctry_macro_info a LEFT JOIN res.dbo.ctry_macro_eco b ON a.ctry=b.ctry",
            # " GROUP BY a.ctry, a.gdp, a.production, a.consumption, a.investment, a.cpi, a.wage, ",
            # " a.unemployment", conn=rconn)
# no.adj.pod <- if (all(g.date.idx(y$date, T, +1) == endt)) adj.pod else tryPod(NO.ROLL.ADJUST.POD)
# if (identical(no.adj.pod, FALSE)) stop("Could not connect to an unadjusted pod!")

# for (i in 1 %:% nrow(y)) {                                                         # Get new data
    # ctry <- y$ctry[i]
    # cat(ctry, "\n")
    # co.date <- g.date.idx(endt, T, -4, cal="cal.qtr.end")
    # a <- getBbHistorical(co.date, endt, "px_last", y[i, 2:(ncol(y)-1)], "Index", no.adj.pod)
    # a <- na.locf(a, na.rm=F)
    # a <- as.data.frame(a)
    # a$date <- rownames(a)
    # rownames(a) <- NULL
    # a$ctry <- y[i, 1]
    # colnames(a) <- c('gdp','production','consumption','investment','cpi','wage','unemployment','date','ctry')
    # a <- a[, c('date','ctry','gdp','production','consumption','investment','cpi','wage','unemployment')]
    
    # dup <- db.sql(g.p("SELECT * FROM res.dbo.ctry_macro_eco WHERE ctry='${ctry}' ",
                      # "AND date >= '${co.date}' AND date <= '${endt}'"), conn=rconn)   ## detect duplicates
    # if (dup$date[nrow(dup)] != endt) {
        # dt.miss <- dates.usd[g.inorder(dup$date[nrow(dup)], dates.usd, endt)]
        # a.last <- a[nrow(a), ]
        # for (n in 1 %:% (length(dt.miss)-1)) {
            # a.last <- rbind(a.last, a.last[nrow(a.last), , drop=F])
        # }
        # a.last$date <- dt.miss
        # a.new <- a.last[!(g.date8(a.last$date) %in% dup$date), ]
        # if (nrow(a.new)!=0) g.put.sql(a.new, "res.dbo.ctry_macro_eco", append=TRUE, conn=rconn)
    # }

    # dup <- db.sql(g.p("SELECT * FROM res.dbo.ctry_macro_eco WHERE ctry='${ctry}' ",
                      # "AND date >= '${co.date}' AND date <= '${endt}'"), conn=dconn)   ## detect duplicates
    # if (dup$date[nrow(dup)] != endt) {
        # dt.miss <- dates.usd[g.inorder(dup$date[nrow(dup)], dates.usd, endt)]
        # a.last <- a[nrow(a), ]
        # for (n in 1 %:% (length(dt.miss)-1)) {
            # a.last <- rbind(a.last, a.last[nrow(a.last), , drop=F])
        # }
        # a.last$date <- dt.miss
        # a.new <- a.last[!(g.date8(a.last$date) %in% dup$date), ]
        # if (nrow(a.new)!=0) g.put.sql(a.new, "res.dbo.ctry_macro_eco", append=TRUE, conn=dconn)
    # }
# }


# ## Update implied vol spread and implied correlation  #moved to alpha/signals/ctry.dataupdate.R
# y <- db.sql("SELECT max(date) from res.dbo.ctry_iv", conn=rconn)
# dts <- dates.usd[g.inorder(y[1,1], dates.usd, endt)]
# ctry <- db.sql("SELECT distinct(ctry) from res.dbo.ctry_iv", conn=rconn)
# ctry <- ctry[, 1]
# expiry <- c(30, 90, 360)
# names(expiry) <- c('m1', 'm3', 'm12')
# iv.id <- c(FR=10564,DE=10565,IT=19124,NL=15711,ES=10563,SE=15807,NO=21783,
           # CH=10567,GB=18367,AU=225,HK=229,JP=231,SG=239,US=9327,CA=17777)
# iv.id <- iv.id[match(ctry, names(iv.id))]
# for (n in 2 %:% length(dts)) {
  # dt <- dts[n]
  # for (m in 1:length(expiry)) {
    # newiv <- newic <- data.frame(array(NA, c(length(ctry), 5),
                                       # list(1:length(ctry),
                                            # c('date','ctry','maturity','metrics','data'))))
    
    # ## implied vol spread
    # xx <- suppressWarnings(mclapply(ctry, mp.iv.get.ctry.stock.iv,
                                    # endt=dt, expiry=expiry[m]))
    # names(xx) <- ctry
    # ivs <- unlist(lapply(xx,
                         # function(x)
                         # mean(x[['stock.ivol.cp']][, 1] - x[['stock.ivol.cp']][, 2], na.rm=T)))
    # newiv$date <- dt
    # newiv$ctry <- names(xx)
    # newiv$maturity <- names(expiry)[m]
    # newiv$metrics <- "ivs"
    # newiv$data <- ivs
    # if (nrow(newiv)!=0) {
        # g.put.sql(newiv, "res.dbo.ctry_iv", append=TRUE, conn=rconn)
        # g.put.sql(newic, "res.dbo.ctry_iv", append=TRUE, conn=dconn)
    # }
    
    # ## implied correlation
    # newic$date <- dt
    # newic$maturity <- names(expiry)[m]
    # newic$metrics <- "impc"
    # for (x in 1:length(iv.id)) {
      # newic$data[x] <- suppressWarnings(mp.iv.calc.imp.cor(iv.id[x], names(iv.id)[x],
                                                           # endt=dt, expiry=expiry[m]))
      # newic$ctry[x] <- names(iv.id)[x]
    # }
    # if (nrow(newic)!=0) {
        # g.put.sql(newic, "res.dbo.ctry_iv", append=TRUE, conn=rconn)
        # g.put.sql(newic, "res.dbo.ctry_iv", append=TRUE, conn=dconn)
    # }
  # }
# }

# ## Update mspb   #moved to alpha/signals/ctry.dataupdate.R
# tday <- g.date.idx( , TRUE)
# dir <- g.p("/gcm/gcm_quantres/research/data/mspb", format(g.Date(tday), "/%Y/%m/%d/"))

# ## change file names
# file.rename(from=list.files(path=dir, pattern="PBXSR212X", full.names = TRUE), 
            # to=gsub("Geode-PBXSR212XReport", "US.eq.ls.lvg.daily", 
                    # list.files(path=dir, pattern="PBXSR212X", full.names = TRUE)))
# file.rename(from=list.files(path=dir, pattern="PBXSR217X", full.names = TRUE), 
            # to=gsub("Geode-PBXSR217XReport", "country.exposures", 
                    # list.files(path=dir, pattern="PBXSR217X", full.names = TRUE)))
# file.rename(from=list.files(path=dir, pattern="SECTEXP01", full.names = TRUE), 
            # to=gsub("Geode-SECTEXP01Report", "sector.exposures.Europe", 
                    # list.files(path=dir, pattern="SECTEXP01", full.names = TRUE)))
# file.rename(from=list.files(path=dir, pattern="SECTEXP02", full.names = TRUE), 
            # to=gsub("Geode-SECTEXP02Report", "sector.exposures.NorthAmerica", 
                    # list.files(path=dir, pattern="SECTEXP02", full.names = TRUE)))
# file.rename(from=list.files(path=dir, pattern="PBXSR220X", full.names = TRUE), 
            # to=gsub("Geode-PBXSR220XReport", "regional.leverage", 
                    # list.files(path=dir, pattern="PBXSR220X", full.names = TRUE)))

# ## append new data
# data <- read.csv(list.files(path=dir, pattern="country.exposures", full.names = TRUE))
# colnames(data) <- c("date",	"ctry", "Country",	"gross.exposure", "net.exposure")
# data[, "date"] <- g.date.idx(g.date8(data[, 'date']), as.date=T)
# data$Country <- NULL
# data <- data[which(nchar(data[, 'ctry'])==3 & data[, 'ctry']!="N/A"),]  #remove code: "XS" & "N/A"

# ## check new updates
# y <- db.sql("SELECT max(date) [date], ctry FROM res.dbo.ctry_mspb GROUP BY ctry", conn=rconn)
# for (i in 1 %:% nrow(y)) {                                                         
  # ctry <- y$ctry[i]
  # stdt <- y$date[i]
  # ## read new data
  # a <- data[which(data[, "ctry"]==ctry & data[,"date"]>=stdt),]
  # ## detect duplicates
  # dup <- db.sql(g.p("SELECT * FROM res.dbo.ctry_mspb WHERE ctry='${ctry}' ",
                    # "AND date > '${stdt}' AND date < '${tday}'"), conn=rconn)
  # ## update database
  # if (nrow(a)!=0 & nrow(dup)==0) {
    # a$date <- tday
    # g.put.sql(a, "res.dbo.ctry_mspb", append=TRUE, conn=rconn)
  # }
  
  # ## detect duplicates
  # dup <- db.sql(g.p("SELECT * FROM res.dbo.ctry_mspb WHERE ctry='${ctry}' ",
                    # "AND date > '${stdt}' AND date < '${tday}'"), conn=dconn)
  # ## update database
  # if (nrow(a)!=0 & nrow(dup)==0) {
    # a$date <- tday
    # g.put.sql(a, "res.dbo.ctry_mspb", append=TRUE, conn=dconn)
  # } 
# }


