## A step-through script to roll LME commodity forwards:
require(sand)
comdty <- "PB"                          # Lead
h <- subset(g.holdings("CTCOM"), substr(symbol,1,4)=="LM" %&% comdty)
old <- h$symbol
shs <- h$shares
sref <- db.sql("select symbol, maturity_date from geode..secref where symbol like 'LMPBDP%'",
               " and symbol not like '%old'")
new <- sref$symbol[which.min(abs(g.date.idx(sref$maturity.date) - g.date.idx() - 63))]
if (old==new) stop("No roll available") else cat(old, "->", new, "\n")
g.trade("CTCOM", c(old,new), c(-shs,shs))

h <- subset(sandHoldings("COM"), substr(symbol,1,4)=="LM" %&% comdty)
shs <- h$shares
sandTrd(data.frame(symbol=c(old,new), shares=c(-shs,shs)), "COM", trade=T, instructions="MKT")
