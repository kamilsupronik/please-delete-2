## Create setup requests on Monday.
suppressMessages({library(gma)})
options(stringsAsFactors=FALSE, warn=1)
if ((today <- g.date8()) %ni% dates.usd || g.dow() != "Mon") q()

atypes <- c(eq="INDEXFUT", bd="TREASFUT", cm="COMDTYFUT", fx="CURRFWD")
recip <- c("GCM_Ops","Wayne.Ryan", "David.Brahm","Eric.Matteson")

## Read keptticker; convert currencies to forwards, and metals to LME forwards:
y <- get(load(m.rdd.path(g.date8(), "/gcm/strategies/gma/carry", "keptticker.RData", F)))
x <- g.sql("select symbol from geode..secref where symbol like 'AUDUSD%.MS'")[[1]]
x <- x[which.max(as.numeric(paste0(substr(x,11,12), substr(x,7,8))))]    # Latest fwd
if (any(q <- y$class=="fx")) y$symbol[q] <- y$symbol[q] %&% substring(x, 4)
if (any(q <- y$class=="cm")) y$symbol[q] <- gma.ctr2hld(y$symbol[q])      # LAU6->LMAHDP 20160921
y$symbol[y$symbol=="BRLUSD122917.MS"] <- "BRLUSD122817.MS"
y$symbol[y$symbol=="COPUSD122917.MS"] <- "COPUSD122817.MS"
y$symbol[y$symbol=="KRWUSD122917.MS"] <- "KRWUSD122817.MS"

## Look up in secref:
sref <- db.sql("select symbol from geode..secref where tradable=1 and symbol in $txt",
               txt=g.comma.list(y$symbol, T))
y <- subset(y, symbol %ni% sref$symbol)
if (!nrow(y)) q()

## Mail the request:
tc <- textConnection("txt", "w")
cat("Ops, please set up these futures for CARRY:\n\n", file=tc)
cat(paste(y$symbol, atypes[y$class], sep="  "), sep="\n", file=tc)
close(tc)
g.mail(recip, "CARRY Futures Setup", txt)
