options(stringsAsFactors=FALSE, warn=1)

### list of mandates
mandate.list <- c('main', 'ucits')

### loop through mandates
for (n in 1:length(mandate.list)) {
    suppressMessages({library(zoo); library(analytics); library(gma); library(bbdotnet3)})
    mandate <- mandate.list[n]
    ## get the last date in the directory
    if (mandate == 'main') {                    # which mandate to run
        existd <- g.date8(GetRddDates(bDir = "/gcm/gcm_strategies/gma/mp",
                                      fName = "eq.liq.w.RData"))               # all folders available
    } else if (mandate == 'ucits') {
        existd <- g.date8(GetRddDates(bDir = "/gcm/gcm_strategies/gma/mp/ucits",
                                      fName = "pm.RData"))               
    }
    curdate <- g.date.idx(,T,)                                         # current date
    alld <- dates.usd[g.inorder(g.date.idx(tail(existd,1),T,1),
                                dates.usd, curdate)]                   # all dates available
    orgd <- dates.usd[g.inorder(20171201, dates.usd, curdate)]   # same as alld with diff purpose

    dofweek <- weekdays(g.Date(orgd))            # find week days of all dates
    nreb <- which(dofweek == "Tuesday")          # car and val rebalance on Tuesday
    rebd <- orgd[nreb]                           # rebalancing day is Tuesday

    ## Change rebal date to Wednesdays for Tuesdays that fall on July 4th, Christmas, New Years
    yrl <- unique(format(g.Date(orgd), "%Y"))         # find all years
    mdl <- c("0101", "0704", "1225")                  # special holidays
    allhol <- array(data=NA, dim=c(1, length(yrl)*length(mdl)),
                    dimnames=list("hld", 1:(length(yrl)*length(mdl))))  # holiday dates
    for (x in 1:length(yrl)){
        allhol[(x*3-2):(x*3)] <- paste(yrl[x], mdl, sep="")        # all possible holiday dates
    }
    allhol <- allhol[allhol<curdate]                                # check historical holidays
    anytue <- allhol[which(weekdays(g.Date(allhol))=="Tuesday")]    # any Tue falls on special dates
    if (length(anytue) != 0) {
        altday <- g.date.idx(g.date8(anytue), T, 1, cal="cal")      # change to Wednesday
        rebd <- sort(c(rebd, altday))                               # sort rebalancing date
    }
    rebd <- rebd[rebd>=existd[1]]                                   # update rebalancing date

    ## main section to call all the other process
    for (kkx in 1 %:% length(alld)) {
        cat(alld[kkx], "\n")

        ## weekly bucket - four portfolios with monthly signal
        if (length(rebd) == 1) {          # only for when portfolio first started
            pf1d <- rebd[1]
            pf2d <- pf3d <- pf4d <- NA
        } else if (length(rebd) == 2){    # only for when portfolio first started
            pf1d <- rebd[1]
            pf2d <- rebd[2]
            pf3d <- pf4d <- NA
        } else if (length(rebd) == 3){    # only for when portfolio first started
            pf1d <- rebd[1]
            pf2d <- rebd[2]
            pf3d <- rebd[3]
            pf4d <- NA
        } else {                          # this is the main case
            pf1d <- rebd[seq(1, length(rebd), 4)]    # first bucket dates
            pf2d <- rebd[seq(2, length(rebd), 4)]    # second bucket dates
            pf3d <- rebd[seq(3, length(rebd), 4)]    # third bucket dates
            pf4d <- rebd[seq(4, length(rebd), 4)]    # fourth bucket dates
        }
        pflist <- list(pf1d, pf2d, pf3d, pf4d)    

        nowdate <- alld[kkx]              # file generation date

        pmrun <- 1               # this is 1 on Tuesdays, i.e. rebal date
        pfid <- which(c(any(nowdate==pf1d),any(nowdate==pf2d),
                        any(nowdate==pf3d),any(nowdate==pf4d))==TRUE)  # which bucket it is today
        if (is.na(match(nowdate,rebd))) pmrun <- 0                     # not rebalancing date

        ## find last rebalancing dates for sub pf
        pret <- 0
        if (length(pfid) != 0) {
            tpfid <- which(pflist[[pfid]]==nowdate)       # temp id to track bucket id
            if (tpfid > 1) {                             
                pret <- c(tail(pf1d[pf1d < nowdate],1),
                          tail(pf2d[pf2d < nowdate],1),
                          tail(pf3d[pf3d < nowdate],1),
                          tail(pf4d[pf4d < nowdate],1))   # last rebalancing date for all bucket pf
            } else {
                pret <- 0              # if no prior record, keep other bucket pf weights zero
            }
        }

        if (mandate == 'main') {                                   # which mandate to run
            source(system.file("mp.run.R", package="gma"))         # signal code
            source(system.file("mp.ticker.R", package="gma"))      # ticker generation code
        } else if (mandate == 'ucits') {
            source(system.file("mp.ticker.ucits.R", package="gma"))
        }
        
        g.clean(keep=c("alld","rebd","n","mandate","mandate.list","kkx"))     # keep it clean
    }
    g.clean(keep=c("mandate.list"))
}
