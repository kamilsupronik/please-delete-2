suppressMessages({library(gma); library(bbdotnet3)})
options(stringsAsFactors=FALSE, warn=1)

## Sys.setenv(DBDSN="ciq", DBUSER="gcmquant", DBPWD="Plusm1nusSQL")
## g.connect.sql()
rconn <- g.connect.sql("ciq", "gcmquant", "Plusm1nusSQL", global=F)       # prod
dconn <- g.connect.sql("ciqdev", "gcmquant", "Plusm1nusSQL", global=F)    # dev

endt <- g.date.idx( , TRUE, -1)

### Get BB connections
adj.pod <- tryPod(c("DB","FB"))


### Updates the COT financial database
adt <- db.sql("SELECT max(date) FROM res.dbo.cotfin", conn=rconn)  # last available date
stdt <- g.date.idx(adt, T, 1, T)


###### fx tickers
### set-up
tf.tick <- "TFF1"     # AM and LF
nw.fx <- c(CAD='A', CHF='B', GBP='C', JPY='D', EUR='E', AUD='F', NZD='I') # AM and LF
am.tick <- c(POS='AI', TDR='AT')
lf.tick <- c(POS='LF', TDR='LT')
nw.tick <- c(LONG='L', SHORT='S', SPREAD='D')
lg.tick <- c(LONG='L', SHORT='S', SPREAD='P')

### asset manager data
am.t.tick <- apply(as.matrix(paste(tf.tick,nw.fx,sep='')),
                                   1, paste, am.tick, sep='')
am.pos.ticker <- rbind(paste(am.t.tick[1, ], nw.tick[1], sep=''),
                       paste(am.t.tick[1, ], nw.tick[2], sep=''),
                       paste(am.t.tick[1, ], nw.tick[3], sep=''))
am.trd.ticker <- rbind(paste(am.t.tick[2, ], nw.tick[1], sep=''),
                       paste(am.t.tick[2, ], nw.tick[2], sep=''),
                       paste(am.t.tick[2, ], nw.tick[3], sep=''))
colnames(am.pos.ticker) <- colnames(am.trd.ticker) <- names(nw.fx)
rownames(am.pos.ticker) <- rownames(am.trd.ticker) <- names(nw.tick)

### leveraged fund data
lf.t.tick <- apply(as.matrix(paste(tf.tick,nw.fx,sep='')),
                                   1, paste, lf.tick, sep='')
lf.pos.ticker <- rbind(paste(lf.t.tick[1, ], nw.tick[1], sep=''),
                       paste(lf.t.tick[1, ], nw.tick[2], sep=''),
                       paste(lf.t.tick[1, ], nw.tick[3], sep=''))
lf.trd.ticker <- rbind(paste(lf.t.tick[2, ], nw.tick[1], sep=''),
                       paste(lf.t.tick[2, ], nw.tick[2], sep=''),
                       paste(lf.t.tick[2, ], nw.tick[3], sep=''))
colnames(lf.pos.ticker) <- colnames(lf.trd.ticker) <- names(nw.fx)
rownames(lf.pos.ticker) <- rownames(lf.trd.ticker) <- names(nw.tick)

### data grab
am.pos.ln <- getBbHistorical(stdt, endt, "px_last", am.pos.ticker[1,], "Index", pod=adj.pod)
am.pos.sh <- getBbHistorical(stdt, endt, "px_last", am.pos.ticker[2,], "Index", pod=adj.pod)
am.pos.sp <- getBbHistorical(stdt, endt, "px_last", am.pos.ticker[3,], "Index", pod=adj.pod)

am.trd.ln <- getBbHistorical(stdt, endt, "px_last", am.trd.ticker[1,], "Index", pod=adj.pod)
am.trd.sh <- getBbHistorical(stdt, endt, "px_last", am.trd.ticker[2,], "Index", pod=adj.pod)
am.trd.sp <- getBbHistorical(stdt, endt, "px_last", am.trd.ticker[3,], "Index", pod=adj.pod)

lf.pos.ln <- getBbHistorical(stdt, endt, "px_last", lf.pos.ticker[1,], "Index", pod=adj.pod)
lf.pos.sh <- getBbHistorical(stdt, endt, "px_last", lf.pos.ticker[2,], "Index", pod=adj.pod)
lf.pos.sp <- getBbHistorical(stdt, endt, "px_last", lf.pos.ticker[3,], "Index", pod=adj.pod)

lf.trd.ln <- getBbHistorical(stdt, endt, "px_last", lf.trd.ticker[1,], "Index", pod=adj.pod)
lf.trd.sh <- getBbHistorical(stdt, endt, "px_last", lf.trd.ticker[2,], "Index", pod=adj.pod)
lf.trd.sp <- getBbHistorical(stdt, endt, "px_last", lf.trd.ticker[3,], "Index", pod=adj.pod)

colnames(am.pos.ln) <- colnames(am.pos.sh) <- colnames(am.pos.sp) <-
  colnames(am.trd.ln) <- colnames(am.trd.sh) <- colnames(am.trd.sp) <-
  colnames(lf.pos.ln) <- colnames(lf.pos.sh) <- colnames(lf.pos.sp) <-
  colnames(lf.trd.ln) <- colnames(lf.trd.sh) <- colnames(lf.trd.sp) <-
  names(nw.fx)

fx.cftc <- list(am.pos.ln=am.pos.ln, am.pos.sh=am.pos.sh, am.pos.sp=am.pos.sp,
                am.trd.ln=am.trd.ln, am.trd.sh=am.trd.sh, am.trd.sp=am.trd.sp,
                lf.pos.ln=lf.pos.ln, lf.pos.sh=lf.pos.sh, lf.pos.sp=lf.pos.sp,
                lf.trd.ln=lf.trd.ln, lf.trd.sh=lf.trd.sh, lf.trd.sp=lf.trd.sp)


###### eq tickers
## set-up
tf.tick <- "TFF"     # AM and LF
im.tick <- c(DJIA='CBT', SPX='IMM', NDQ='IMM', R2K='CFF', EAFE='CFF', EM='CFF')
nw.eq <- c(DJIA='1K', SPX='1N', NDQ='1R', R2K='2T', EAFE='2X', EM='2Y')  # AM and LF
lg.eq <- c(DJIA='1D', SPX='0E', NDQ='3N', R2K='6T', EAFE='4B', EM='4C')  # non-comm
am.tick <- c(POS='AI', TDR='AT')
lf.tick <- c(POS='LF', TDR='LT')
nc.tick <- c(POS='NC', TDR='TN')
nw.tick <- c(LONG='L', SHORT='S', SPREAD='D')
lg.tick <- c(LONG='L', SHORT='S', SPREAD='P')

## asset manager data
am.t.tick <- apply(as.matrix(paste(tf.tick,nw.eq,sep='')),
                                   1, paste, am.tick, sep='')
am.pos.ticker <- rbind(paste(am.t.tick[1, ], nw.tick[1], sep=''),
                       paste(am.t.tick[1, ], nw.tick[2], sep=''),
                       paste(am.t.tick[1, ], nw.tick[3], sep=''))
am.trd.ticker <- rbind(paste(am.t.tick[2, ], nw.tick[1], sep=''),
                       paste(am.t.tick[2, ], nw.tick[2], sep=''),
                       paste(am.t.tick[2, ], nw.tick[3], sep=''))
colnames(am.pos.ticker) <- colnames(am.trd.ticker) <- names(nw.eq)
rownames(am.pos.ticker) <- rownames(am.trd.ticker) <- names(nw.tick)

## leveraged fund data
lf.t.tick <- apply(as.matrix(paste(tf.tick,nw.eq,sep='')),
                                   1, paste, lf.tick, sep='')
lf.pos.ticker <- rbind(paste(lf.t.tick[1, ], nw.tick[1], sep=''),
                       paste(lf.t.tick[1, ], nw.tick[2], sep=''),
                       paste(lf.t.tick[1, ], nw.tick[3], sep=''))
lf.trd.ticker <- rbind(paste(lf.t.tick[2, ], nw.tick[1], sep=''),
                       paste(lf.t.tick[2, ], nw.tick[2], sep=''),
                       paste(lf.t.tick[2, ], nw.tick[3], sep=''))
colnames(lf.pos.ticker) <- colnames(lf.trd.ticker) <- names(nw.eq)
rownames(lf.pos.ticker) <- rownames(lf.trd.ticker) <- names(nw.tick)


## data grab
am.pos.ln <- getBbHistorical(stdt, endt, "px_last", am.pos.ticker[1,], "Index", pod=adj.pod)
am.pos.sh <- getBbHistorical(stdt, endt, "px_last", am.pos.ticker[2,], "Index", pod=adj.pod)
am.pos.sp <- getBbHistorical(stdt, endt, "px_last", am.pos.ticker[3,], "Index", pod=adj.pod)

am.trd.ln <- getBbHistorical(stdt, endt, "px_last", am.trd.ticker[1,], "Index", pod=adj.pod)
am.trd.sh <- getBbHistorical(stdt, endt, "px_last", am.trd.ticker[2,], "Index", pod=adj.pod)
am.trd.sp <- getBbHistorical(stdt, endt, "px_last", am.trd.ticker[3,], "Index", pod=adj.pod)

lf.pos.ln <- getBbHistorical(stdt, endt, "px_last", lf.pos.ticker[1,], "Index", pod=adj.pod)
lf.pos.sh <- getBbHistorical(stdt, endt, "px_last", lf.pos.ticker[2,], "Index", pod=adj.pod)
lf.pos.sp <- getBbHistorical(stdt, endt, "px_last", lf.pos.ticker[3,], "Index", pod=adj.pod)

lf.trd.ln <- getBbHistorical(stdt, endt, "px_last", lf.trd.ticker[1,], "Index", pod=adj.pod)
lf.trd.sh <- getBbHistorical(stdt, endt, "px_last", lf.trd.ticker[2,], "Index", pod=adj.pod)
lf.trd.sp <- getBbHistorical(stdt, endt, "px_last", lf.trd.ticker[3,], "Index", pod=adj.pod)

colnames(am.pos.ln) <- colnames(am.pos.sh) <- colnames(am.pos.sp) <-
  colnames(am.trd.ln) <- colnames(am.trd.sh) <- colnames(am.trd.sp) <-
  colnames(lf.pos.ln) <- colnames(lf.pos.sh) <- colnames(lf.pos.sp) <-
  colnames(lf.trd.ln) <- colnames(lf.trd.sh) <- colnames(lf.trd.sp) <-
  names(nw.eq)

eq.cftc <- list(am.pos.ln=am.pos.ln, am.pos.sh=am.pos.sh, am.pos.sp=am.pos.sp,
                am.trd.ln=am.trd.ln, am.trd.sh=am.trd.sh, am.trd.sp=am.trd.sp,
                lf.pos.ln=lf.pos.ln, lf.pos.sh=lf.pos.sh, lf.pos.sp=lf.pos.sp,
                lf.trd.ln=lf.trd.ln, lf.trd.sh=lf.trd.sh, lf.trd.sp=lf.trd.sp)


###### bd tickers
## set-up
tf.tick <- "TFF"     # AM and LF
im.tick <- "CBT"
nw.bd <- c(US='2E', WN='2F', TU='2G', TY='2H', FV='2I')  # AM and LF
lg.bd <- c(US='3T', WN='3B', TU='42', TY='4T', FV='55')  # non-comm
am.tick <- c(POS='AI', TDR='AT')
lf.tick <- c(POS='LF', TDR='LT')
nc.tick <- c(POS='NC', TDR='TN')
nw.tick <- c(LONG='L', SHORT='S', SPREAD='D')
lg.tick <- c(LONG='L', SHORT='S', SPREAD='P')

## asset manager data
am.t.tick <- apply(as.matrix(paste(tf.tick,nw.bd,sep='')),
                                   1, paste, am.tick, sep='')
am.pos.ticker <- rbind(paste(am.t.tick[1, ], nw.tick[1], sep=''),
                       paste(am.t.tick[1, ], nw.tick[2], sep=''),
                       paste(am.t.tick[1, ], nw.tick[3], sep=''))
am.trd.ticker <- rbind(paste(am.t.tick[2, ], nw.tick[1], sep=''),
                       paste(am.t.tick[2, ], nw.tick[2], sep=''),
                       paste(am.t.tick[2, ], nw.tick[3], sep=''))
colnames(am.pos.ticker) <- colnames(am.trd.ticker) <- names(nw.bd)
rownames(am.pos.ticker) <- rownames(am.trd.ticker) <- names(nw.tick)

## leveraged fund data
lf.t.tick <- apply(as.matrix(paste(tf.tick,nw.bd,sep='')),
                                   1, paste, lf.tick, sep='')
lf.pos.ticker <- rbind(paste(lf.t.tick[1, ], nw.tick[1], sep=''),
                       paste(lf.t.tick[1, ], nw.tick[2], sep=''),
                       paste(lf.t.tick[1, ], nw.tick[3], sep=''))
lf.trd.ticker <- rbind(paste(lf.t.tick[2, ], nw.tick[1], sep=''),
                       paste(lf.t.tick[2, ], nw.tick[2], sep=''),
                       paste(lf.t.tick[2, ], nw.tick[3], sep=''))
colnames(lf.pos.ticker) <- colnames(lf.trd.ticker) <- names(nw.bd)
rownames(lf.pos.ticker) <- rownames(lf.trd.ticker) <- names(nw.tick)

## data grab
am.pos.ln <- getBbHistorical(stdt, endt, "px_last", am.pos.ticker[1,], "Index", pod=adj.pod)
am.pos.sh <- getBbHistorical(stdt, endt, "px_last", am.pos.ticker[2,], "Index", pod=adj.pod)
am.pos.sp <- getBbHistorical(stdt, endt, "px_last", am.pos.ticker[3,], "Index", pod=adj.pod)

am.trd.ln <- getBbHistorical(stdt, endt, "px_last", am.trd.ticker[1,], "Index", pod=adj.pod)
am.trd.sh <- getBbHistorical(stdt, endt, "px_last", am.trd.ticker[2,], "Index", pod=adj.pod)
am.trd.sp <- getBbHistorical(stdt, endt, "px_last", am.trd.ticker[3,], "Index", pod=adj.pod)

lf.pos.ln <- getBbHistorical(stdt, endt, "px_last", lf.pos.ticker[1,], "Index", pod=adj.pod)
lf.pos.sh <- getBbHistorical(stdt, endt, "px_last", lf.pos.ticker[2,], "Index", pod=adj.pod)
lf.pos.sp <- getBbHistorical(stdt, endt, "px_last", lf.pos.ticker[3,], "Index", pod=adj.pod)

lf.trd.ln <- getBbHistorical(stdt, endt, "px_last", lf.trd.ticker[1,], "Index", pod=adj.pod)
lf.trd.sh <- getBbHistorical(stdt, endt, "px_last", lf.trd.ticker[2,], "Index", pod=adj.pod)
lf.trd.sp <- getBbHistorical(stdt, endt, "px_last", lf.trd.ticker[3,], "Index", pod=adj.pod)

colnames(am.pos.ln) <- colnames(am.pos.sh) <- colnames(am.pos.sp) <-
  colnames(am.trd.ln) <- colnames(am.trd.sh) <- colnames(am.trd.sp) <-
  colnames(lf.pos.ln) <- colnames(lf.pos.sh) <- colnames(lf.pos.sp) <-
  colnames(lf.trd.ln) <- colnames(lf.trd.sh) <- colnames(lf.trd.sp) <-
  names(nw.bd)

bd.cftc <- list(am.pos.ln=am.pos.ln, am.pos.sh=am.pos.sh, am.pos.sp=am.pos.sp,
                am.trd.ln=am.trd.ln, am.trd.sh=am.trd.sh, am.trd.sp=am.trd.sp,
                lf.pos.ln=lf.pos.ln, lf.pos.sh=lf.pos.sh, lf.pos.sp=lf.pos.sp,
                lf.trd.ln=lf.trd.ln, lf.trd.sh=lf.trd.sh, lf.trd.sp=lf.trd.sp)


###### cm tickers
## set-up
tf.tick <- c(BO='CFF',C='CFF',CC='CFF',CL='CFF',CO='ICF',CT='CFF',
             FC='CFF',GC='CFF',HG='CFF',HO='CFF',KC='CFF',KW='CFF',
             LC='CFF',LH='CFF',NG='CFF',PA='DFU',PL='DFU',QS='ICF',
             S='CFF',SB='CFF',SI='CFF',SM='CFF',W='CFF',XB='CFF')
im.tick <- c(BO='CBT',C='CBT',CC='CSC',CL='NYM',CO='',CT='NYC',
             FC='CME',GC='CEI',HG='CEI',HO='NYM',KC='CSC',KW='KCB',
             LC='CME',LH='CME',NG='NYM',PA='NYM',PL='NYM',QS='',
             S='CBT',SB='CSC',SI='CEI',SM='CBT',W='CBT',XB='NYM')
nw.cm <- c(BO='DH',C='DC',CC='DK',CL='DQ',CO='UB',CT='DJ',
           FC='DF',GC='DU',HG='DT',HO='DN',KC='DM',KW='DB',
           LC='DE',LH='DD',NG='DO',PA='3N',PL='3O',QS='UA',
           S='DG',SB='DL',SI='DS',SM='DI',W='DA',XB='DR')              # MM
lg.cm <- c(BO='2S',C='1C',CC='1C',CL='1C',CO='',CT='1C',
           FC='1F',GC='1G',HG='1C',HO='1H',KC='1F',KW='TW',
           LC='1L',LH='3H',NG='1N',PA='2P',PL='3P',QS='',
           S='1S',SB='1S',SI='1S',SM='3S',W='1W',XB='2X')              # non-comm
mm.tick <- c(POS='MM', TDR='MT')
nc.tick <- c(POS='NC', TDR='TN')
nw.tick <- c(LONG='L', SHORT='S', SPREAD='D')
lg.tick <- c(LONG='L', SHORT='S', SPREAD='P')

## managed money data
mm.t.tick <- apply(as.matrix(paste(tf.tick,nw.cm,sep='')),
                                   1, paste, mm.tick, sep='')
mm.pos.ticker <- rbind(paste(mm.t.tick[1, ], nw.tick[1], sep=''),
                       paste(mm.t.tick[1, ], nw.tick[2], sep=''),
                       paste(mm.t.tick[1, ], nw.tick[3], sep=''))
mm.trd.ticker <- rbind(paste(mm.t.tick[2, ], nw.tick[1], sep=''),
                       paste(mm.t.tick[2, ], nw.tick[2], sep=''),
                       paste(mm.t.tick[2, ], nw.tick[3], sep=''))
colnames(mm.pos.ticker) <- colnames(mm.trd.ticker) <- names(nw.cm)
rownames(mm.pos.ticker) <- rownames(mm.trd.ticker) <- names(nw.tick)

## data grab
mm.pos.ln <- getBbHistorical(stdt, endt, "px_last", mm.pos.ticker[1,], "Index", pod=adj.pod)
mm.pos.sh <- getBbHistorical(stdt, endt, "px_last", mm.pos.ticker[2,], "Index", pod=adj.pod)
mm.pos.sp <- getBbHistorical(stdt, endt, "px_last", mm.pos.ticker[3,], "Index", pod=adj.pod)

mm.trd.ln <- getBbHistorical(stdt, endt, "px_last", mm.trd.ticker[1,], "Index", pod=adj.pod)
mm.trd.sh <- getBbHistorical(stdt, endt, "px_last", mm.trd.ticker[2,], "Index", pod=adj.pod)
mm.trd.sp <- getBbHistorical(stdt, endt, "px_last", mm.trd.ticker[3,], "Index", pod=adj.pod)

colnames(mm.pos.ln) <- colnames(mm.pos.sh) <- colnames(mm.pos.sp) <-
  colnames(mm.trd.ln) <- colnames(mm.trd.sh) <- colnames(mm.trd.sp) <-
  names(nw.cm)

cm.cftc <- list(mm.pos.ln=mm.pos.ln, mm.pos.sh=mm.pos.sh, mm.pos.sp=mm.pos.sp,
                mm.trd.ln=mm.trd.ln, mm.trd.sh=mm.trd.sh, mm.trd.sp=mm.trd.sp)


###### update to database
if (nrow(fx.cftc[[1]]) !=0) {      # check if data is empty
    ### cotfin table
    n <- 1        # initialize data format with first member of the list
    nm.t <- names(fx.cftc)[[n]]
    fx.t <- g.array.to.ragged(fx.cftc[[n]], keys=c('date', 'asset'),
                              item=nm.t)
    eq.tt <- eq.cftc[[n]]
    colnames(eq.tt) <- c("DM", "ES", "NQ", "RTY", "MFS", "MES")   # change to futures ticker
    eq.t <- g.array.to.ragged(eq.tt, keys=c('date', 'asset'),
                              item=nm.t)
    bd.t <- g.array.to.ragged(bd.cftc[[n]], keys=c('date', 'asset'),
                              item=nm.t)
    bbdata <- rbind(fx.t, eq.t, bd.t)

    for (n in 2:12) {    # all have same length and format
        nm.t <- names(fx.cftc)[[n]]
        fx.t <- g.array.to.ragged(fx.cftc[[n]], keys=c('date', 'asset'),
                                  item=nm.t)
        eq.tt <- eq.cftc[[n]]
        colnames(eq.tt) <- c("DM", "ES", "NQ", "RTY", "MFS", "MES")   # change to futures ticker
        eq.t <- g.array.to.ragged(eq.tt, keys=c('date', 'asset'),
                                  item=nm.t)
        bd.t <- g.array.to.ragged(bd.cftc[[n]], keys=c('date', 'asset'),
                                  item=nm.t)
        dt.t <- rbind(fx.t, eq.t, bd.t)
        bbdata <- merge(bbdata, dt.t, by = c('date','asset'), all=T)
    }

    if (nrow(bbdata) !=0) {
        g.put.sql(bbdata, "res.dbo.cotfin", append=TRUE, conn=rconn)
        g.put.sql(bbdata, "res.dbo.cotfin", append=TRUE, conn=dconn)
    }

    ### cotcom table
    n <- 1        # initialize data format with first member of the list
    nm.t <- names(cm.cftc)[[n]]
    cm.t <- g.array.to.ragged(cm.cftc[[n]], keys=c('date', 'asset'),
                              item=nm.t)
    bbdata <- cm.t

    for (n in 2:6) {    # all have same length and format
        nm.t <- names(cm.cftc)[[n]]
        cm.t <- g.array.to.ragged(cm.cftc[[n]], keys=c('date', 'asset'),
                                  item=nm.t)
        dt.t <- cm.t
        bbdata <- merge(bbdata, dt.t, by = c('date','asset'), all=T)
    }

    if (nrow(bbdata) !=0) {
        g.put.sql(bbdata, "res.dbo.cotcom", append=TRUE, conn=rconn)
        g.put.sql(bbdata, "res.dbo.cotcom", append=TRUE, conn=dconn)
    }
}
