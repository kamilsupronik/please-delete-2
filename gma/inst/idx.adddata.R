## add data to empty table
add.idxdata <- function(stdt, endt=g.date.idx(, TRUE, -1), bbticker, bbidx, pod){
    ## add an index to the SQL table
    ## bbticker: Bloomberg ticker for the futures
    ## bbidx: category for bb for each futures: Index for equity futures, Comdty for bonds; first letter cap
    ## Note: Will not add names for which we do not have prices
    require(bbdotnet3)
    require(zoo)
    stdt <- g.read.date(stdt); endt <- g.read.date(endt)
    stopifnot(!is.na(bbticker) && length(bbticker)==1)

    suffixes <- ""
    if (bbidx == "Comdty") suffixes <- 1                # implied vol is the same for 1 and 2 contracts
    tick2 <- tick1 <- bbticker
    idx <- nchar(bbticker) == 1                                            # "S" 
    if(any(idx)) tick2[idx] <- bbticker[idx] %&% " "                       # "S "
    ticks <- paste(tick2 %&% suffixes, bbidx)                              # "S Index"

    ## index and related fields
    flds <- c("PX_LAST", "EQY_DVD_YLD_12M", "PX_TO_BOOK_RATIO")

    bbu <- getBbHistorical(stdt, endt, flds, ticks, pod=pod)
    dimnames(bbu)[[3]] <- flds
    if (is.null(bbu) || !nrow(bbu)) {cat("No data for", bbticker, "\n"); return()}

    idx <- rownames(bbu)
    
    ## Create a date x curncy x item array "z":
    dts <- g.read.date(idx)            # Always weekdays, some holidays (eg CO traded on 2/16/09)
    zp <- zy <- zb <- g.mk.array(date=dts, idx=tick1)
    for (j in seq(along=tick1)) {
        colm <- ticks

        zp[ ,j] <- bbu[ , colm, "PX_LAST"]
        zp <- na.locf(zp, na.rm=F)
        zy[ ,j] <- bbu[ , colm, "EQY_DVD_YLD_12M"]
        zy <- na.locf(zy, na.rm=F)
        zb[ ,j] <- bbu[ , colm, "PX_TO_BOOK_RATIO"]
        zb <- na.locf(zb, na.rm=F)
    }

    ## Turn into ragged array:
    ap <- g.array.to.ragged(zp, item="price")
    ay <- g.array.to.ragged(zy, item="divy")
    ab <- g.array.to.ragged(zb, item="PtoB")

    a <- cbind(ap, ay[,3,drop=F], ab[,3,drop=F])
    
    a$date <- g.Date(a$date)
    a <- a[order(a$date), ]; rownames(a) <- NULL
    
    a
}
