#!/usr/bin/perl

@funds = ("GSCOM","CMDACPLT","CMCENCFC","GC5VOL","PCAPSV");
$opts1 = join(" ", map("<OPTION>$_</OPTION>", @funds));
@insts = ("MKT","CLOSE");
$opts2 = join(" ", map("<OPTION>$_</OPTION>", @insts));
$opts2 =~ s/OPTION/OPTION SELECTED/;                                         # Just the first one

print "Content-type: text/html\n\n";
print <<EOF;
  <TITLE>GMA Change Trade Instruction</TITLE>
  <A HREF=/><IMG align=right border=0 src=/logo.gif></A>
  <H1 ALIGN="CENTER"> GMA Change Trade Instruction </H1>
  <H2 ALIGN="LEFT"> Run this to change a trade instruction in Sand to match CRD </H2>

  <form method="POST" action="instruction2.cgi">
  Fund:     <select name=fund size=5> $opts1 </select> &nbsp
  Ticker:   <input name=tick size=10> &nbsp
  NewInst:  <select name=inst size=2> $opts2 </select> &nbsp
  Password: <input type=password name=pwd size=3> &nbsp
  <input type=submit value="Go!">
  </form>
  <HR><EM> David E. Brahm
  (<A HREF="mailto:David.Brahm@geodecapital.com"> David.Brahm@geodecapital.com </A>)
  &nbsp Last update 5/5/16
EOF
