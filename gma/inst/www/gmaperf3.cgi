#!/usr/bin/perl
$| = 1;				                     # Don't buffer Perl output
print "Content-type: text/html\n\n<PRE>\n";
foreach $pair (split(/&/, <>)) {
  ($key, $val) = split(/=/, $pair, 2);
  $in{$key} .= $val;
}

require("/gcm/gcm_sys2home/gcmweb/.stuff.pl");                                # Linux environment
$script = "/tmp/rwrap$$.R";
open(SCRIPT, ">$script");
print SCRIPT <<EOF;
  ## There are currently no inputs
EOF
print SCRIPT <<'EOF';		           # Begin R script
suppressMessages({library(gma); library(bbdotnet3)})
options(stringsAsFactors=FALSE, warn=1)
pod <- tryPod(c("DB","GL"))                            # Good to have CBOT,CEC,ICEA,ICEF,LME,NYMX
if (pod != "DB") cat("Warning: pod =", pod, ", some prices delayed.\n")

## Helper function:
bbrt <- function(nitm, citm, ids) {
    bb <- getBbRealTime(c(nitm,citm), ids, pod=pod)
    suppressWarnings(bb <- data.frame(bb))          # E.g. no "some row.names duplicated" warning
    suppressWarnings(for (i in nitm) bb[ ,i] <- as.double(bb[ ,i]))
    bb
}

wo <- get(load(m.rdd.path(g.date.idx( ,T,-1), "/gcm/strategies/gma", "wo.sav.RData")))
nitm <- c("last_price","px_yest_close")
citm <- c("inverse_quoted")
bb <- bbrt(nitm, citm, rownames(wo) %&% " Curncy")
for (i in nitm) bb[ ,i] <- ifelse(bb$inverse_quoted=="Y", bb[ ,i], 1/bb[ ,i])
rtn <- bb$last_price / bb$px_yest_close - 1
x <- data.frame(strat=colnames(wo), rtn=as.vector(rtn %*% wo))
kl <- list(items="rtn", typ="n", dec=0, scl=1e4, hdr="Rtn (bp)")
fmt <- list(title="G10 Currency Strategy Perf", n=1, klist=kl, ll="", lr="")
g.rpt.html(x, fmt)
EOF
close(SCRIPT);			           # End R script
system("/gcm/gcm_sys2home/gcmweb/local/bin/R --vanilla --slave < $script 2>&1");
$? && die "Aborted in R with errcode $?";   # Check for successful R completion
unlink $script;
