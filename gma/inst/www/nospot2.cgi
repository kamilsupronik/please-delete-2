#!/usr/bin/perl
$| = 1;				                     # Don't buffer Perl output
print "Content-type: text/html\n\n<PRE>\n";
foreach $pair (split(/&/, <>)) {
  ($key, $val) = split(/=/, $pair, 2);
  $in{$key} .= $val;
}
if (crypt($in{pwd},"db") ne "db/HmZh8z2lsg") {print "Wrong password.\n"; exit};

require("/gcm/gcm_sys2home/gcmweb/.stuff.pl");                                # Linux environment
$script = "/tmp/rwrap$$.R";
open(SCRIPT, ">$script");
print SCRIPT <<'EOF';                                                            # Begin R script
.<-suppressMessages({require(gma); require(sand)})
options(stringsAsFactors=FALSE, warn=1)
today <- g.date8()
yest  <- g.date.idx(today,T,-1)
fx <- pgSql("select fromcur, fx from fx where date='$yest' and tocur='USD'")
fxeur <- fx$fx[fx$fromcur=="EUR"]
doit <- TRUE                                                    # Set this to FALSE for debugging

map <- c(GC2HAL="038CDAXE8", GDMN="038CDCXB0",                   # Morgan Stanley account numbers
         GC5COM="038CDAXH1", GC5VOL="038CDAXJ7", MANFUT="038CDNS97", HEUTF="038CDNLO1",
         GMAP="038CAC2I2")

for (fund in c("GMAP")) {                                            # "GC5VOL","GC5COM","MANFUT"
    ## Morgan Stanley currency positions:
    y <- g.sql("SELECT symbol, current_quantity as shares, gross_mkt_value_usd as mktval",
    ##         "       , sub_acct_number",
               "  FROM hold..mspb_positions",
               " WHERE main_acct_number='$x' and product_type='CASH'", x=map[fund])
    y$symbol[y$symbol=="MVRXX"] <- "USD"
    ym <- data.frame(curncy=sort(unique(y$symbol)))
    for (i in c("shares","mktval")) ym[[i]] <- tapply(y[[i]], y$symbol, sum)
    ym <- subset(ym, curncy != "USD" & abs(mktval) > 2);  rownames(ym) <- NULL
    if (nrow(ym)==0) next
    ym$target <- 0

    trdIn <- switch(fund, GC5VOL="MPEQVOL", GC5COM="MPEQVAL", MANFUT="MPEQMOM", GMAP="GMEQVAL")
    sidx <- pgSql("select sandid, port from sand_idx where fund='$fund' and live")
    sidx <- sidx[order(match(sidx$port, trdIn, 0)), ]   # Put this one last, for real spot trades
    sidx <- subset(sidx, substr(port,3,4) %in% c("BD","CD","EQ"));  rownames(sidx) <- NULL
    for (i in 1:nrow(sidx)) {
        sid <- sidx$sandid[i];  port <- sidx$port[i];  aspr <- substring(port,3)
        sc <- pgSql("select * from sand_cash where sandid=$sid and date='$yest'")
        sc <- subset(sc, curncy != "USD" & net != 0)
        sc$target <- rep(0, nrow(sc))                 # Sand target for this port, initially zero
        h <- sandHoldings(sid, today, type="i")       # Ignore today's trades
        if (aspr=="CDCAR" && any(q <- substr(h$symbol,1,4)=="ITRX")) {
            hx <- h[q, ];  h <- h[!q, ]
            x <- sum((hx$price_usd / fxeur - 1) * hx$shares)
            sc$target[sc$curncy=="EUR"] <- sc$target[sc$curncy=="EUR"] - x
            ym$target[ym$curncy=="EUR"] <- ym$target[ym$curncy=="EUR"] - x
        }
        x <- tapply(h$price_loc * h$shares, h$curncy, sum)
        if (aspr %ni% c("BDVOL","EQVOL")) sc$target <- sc$target - Z(x[sc$curncy]) # Offset hldgs
        if (aspr %in% c("EQVAL","EQLIQ","EQDEF"))    # Real curncy posns only in real stock funds
          ym$target <- ym$target - as.double(Z(x[ym$curncy]))
        if (i==nrow(sidx)) {                                        # These get the actual trades
            yc <- data.frame(symbol=ym$curncy %&% "USD", shares=round(ym$target - ym$shares),
                             comment=paste0("[", sandId(sid,T)$port, "]"))
            if (doit) sandTrd(yc, sid, instructions="MKT", trade=TRUE)
            sc$target <- sc$target - Z(yc$shares[match(sc$curncy, ym$curncy)])  # Reduce by trade
        }
        ## Insert "yf" into sand_cashflow so that Sand reaches target fx holdings:
        yf <- data.frame(curncy=sc$curncy, net=as.double(sc$target - sc$net))
        yffx <- fx$fx[match(yf$curncy, fx$fromcur)]
        yf <- rbind(yf, data.frame(curncy="USD", net=-sum(yf$net * yffx)))       # Convert to USD
        yf$sandid <- sid;  yf$date <- today;  yf$trans_type <- "invest";  rownames(yf) <- NULL
        if (doit) pgInsert("sand_cashflow", yf)
    }                                                                              # End "i" loop
    cat(fund, "done.<BR>\n")
}                                                                                 # End fund loop
if (FALSE) {                                                          # Clear out and start again
    allsids <- pgSql("select sandid from sand_idx where live and port not like 'JUNK%'",
                     " and fund in ('GC5COM','GC5VOL','MANFUT','FARGG','GMAP')",
                     " and port not like '%CASH' order by sandid")[[1]]
    stxt <- g.comma.list(allsids, T,F)
    pgSql("select * from sand_cashflow where date='$today' and sandid in $stxt")
    pgSql("delete from sand_cashflow where date='$today' and sandid in $stxt")
}
EOF
close(SCRIPT);			           # End R script
system("/gcm/gcm_sys2home/gcmweb/local/bin/R --vanilla --slave < $script 2>&1");
$? && die "Aborted in R with errcode $?";   # Check for successful R completion
unlink $script;
