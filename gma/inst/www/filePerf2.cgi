#!/usr/bin/perl
$| = 1;				                     # Don't buffer Perl output
print "Content-type: text/html\n\n<PRE>\n";
foreach $pair (split(/&/, <>)) {
  ($key, $val) = split(/=/, $pair, 2);
  $key =~ s/%(..)/pack("c",hex($1))/ge;
  $val =~ s/%(..)/pack("c",hex($1))/ge;
  $val =~ s/\+/ /g;
  $in{$key} .= "\",\"" if (defined($in{$key}));
  $in{$key} .= $val;
}
if (crypt($in{pwd},"db") ne "db/HmZh8z2lsg") {print "Wrong password.\n"; exit};

## Linux environment:
chdir "/gcm/gcm_sys2home/gcmweb/apache/htdocs/DB/Output";
$ENV{"R_LIBS"} = "/gcm/gcm_sys2home/gcmweb/code/rlib";
$ENV{DBUSER}="gcmweb";  $ENV{DBPWD}="G6qzOr4z";  $ENV{DBDSN}="prod";
$ENV{PGUSER}="gcmweb";  $ENV{PGHOST}="gcmgda200ap.fmr.com";
$ENV{LD_LIBRARY_PATH} = $ENV{LD_LIBRARY_PATH} . ":/gcm/gcm_gmn/prefix/usr/lib";

$script = "/tmp/rwrap$$.R";
open(SCRIPT, ">$script");
print SCRIPT <<EOF;
  fund <- "$in{fund}"
  endt <- "$in{endt}"
  png  <- "$in{png}"=="on"
EOF
print SCRIPT <<'EOF';		           # Begin R script
.<-suppressMessages(require(gma))
options(stringsAsFactors=FALSE, warn=1)

endt <- g.read.date(endt)
g.printer(file=ifelse(png, "bobePerf.png", "bobePerf.pdf"))
bobePerf(fund, endt, file=T, png=png)
.<-dev.off()

EOF
close(SCRIPT);			           # End R script
system("/gcm/gcm_sys2home/gcmweb/local/bin/R --vanilla --slave < $script 2>&1");
$? && die "Aborted in R with errcode $?";   # Check for successful R completion
unlink $script;
if ($in{png} eq "on") {
  print "<META HTTP-EQUIV=Refresh CONTENT='0; URL=../Output/bobePerf.png'>\n";
} else {
  print "<META HTTP-EQUIV=Refresh CONTENT='0; URL=../Output/bobePerf.pdf'>\n";
}
