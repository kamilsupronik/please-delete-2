#!/usr/bin/perl
$| = 1;				                     # Don't buffer Perl output
print "Content-type: text/html\n\n<PRE>\n";
foreach $pair (split(/&/, <>)) {
  ($key, $val) = split(/=/, $pair, 2);
  $in{$key} .= $val;
}
if (crypt($in{pwd},"db") ne "db/HmZh8z2lsg") {print "Wrong password.\n"; exit};

require("/gcm/gcm_sys2home/gcmweb/.stuff.pl");                                # Linux environment
$script = "/tmp/rwrap$$.R";
open(SCRIPT, ">$script");
print SCRIPT <<EOF;
  blot <- "$in{blot}"=="on"
  fl <- list()
EOF
foreach $key (GC5COM,          GC5CUR,         GC5VOL_Hedge,  GC5VOL_Roll,
	      CMDTY_STR_Hedge,                 FI_STR_Hedge,  FI_STR_Roll,
	      PREROLL_Trade,                                  CMDTY_STR_Roll)
  {print SCRIPT "fl[['$key']] <- ", $in{$key} ne "" ? TRUE : FALSE, "\n"}

print SCRIPT <<'EOF';                                                            # Begin R script
.<-suppressMessages({library(gma); library(bbdotnet3); library(sand)})
options(stringsAsFactors=FALSE, warn=1)
pod <- tryPod(PODS)                                    # Good to have CBOT,CEC,ICEA,ICEF,LME,NYMX
bid <- if (blot) basketId() else 0
if (blot) .<-sandUpdateTrades("GC5COM")

if (fl$GC5COM)          gma.comtrd("GC5COM", pod, html=T, basketid=bid)
if (fl$GC5CUR)          gma.curtrd("GC5CUR", pod, html=T, basketid=bid)
if (fl$GC5VOL_Hedge)    gma.volhdg("GC5VOL", pod, html=T, basketid=bid)
if (fl$GC5VOL_Roll)     gma.voltrd("GC5VOL", pod, html=T, basketid=bid)

if (fl$CMDTY_STR_Hedge) gma.cstrhdg(pod, html=T, basketid=bid, recip=if(blot) "Eric.Matteson")
if (fl$CMDTY_STR_Roll)  gma.cstrtrd(pod, html=T, basketid=bid)
if (fl$FI_STR_Hedge)    gma.tlthdg (pod, html=T, basketid=bid)
if (fl$FI_STR_Roll)     gma.tlttrd (pod, html=T, basketid=bid)
if (fl$PREROLL_Trade)   gma.preroll(     html=T, basketid=bid)

if (blot && !is.null(bids <- sandPlaceTrades(bid))) {
    .<-sandUpdateTrades("GC5COM")  # Make sure Sand knows trades in CRD, in case you delete later
    y <- pgSql("select sandid, symbol, shares, fill, instructions, state, basketid, trd_id",
               "  from sand_trd_queue where basketid in $xx", xx=g.comma.list(bids,T,F))
    if (any(q <- y$state=="is")) print(y[q, ])                             # Display interstrats!
}

EOF
close(SCRIPT);			           # End R script
system("/gcm/gcm_sys2home/gcmweb/local/bin/R --vanilla --slave < $script 2>&1");
$? && die "Aborted in R with errcode $?";   # Check for successful R completion
unlink $script;
