#!/usr/bin/perl
$| = 1;				                     # Don't buffer Perl output
print "Content-type: text/html\n\n<PRE>\n";
foreach $pair (split(/&/, <>)) {
  ($key, $val) = split(/=/, $pair, 2);
  $in{$key} .= $val;
}
if ($in{pret} && !$in{relt}) {print "Do not check PreTrd without RelTrd!"; die}

require("/gcm/gcm_sys2home/gcmweb/.stuff.pl");                                # Linux environment
$script = "/tmp/rwrap$$.R";
open(SCRIPT, ">$script");
print SCRIPT <<EOF;
  mfund <- "$in{mfund}"
  sorto <- "$in{sorto}"
  relt  <- "$in{relt}"=="on"
  pret  <- "$in{pret}"=="on"
EOF
print SCRIPT <<'EOF';		           # Begin R script
suppressMessages({library(gma); library(bbdotnet3)})
options(stringsAsFactors=FALSE, warn=1)

## Helper functions:
add.orders <- function(h, fname) {
    for (i in which(ords$fund==fname)) if (idx <- match(ords$symbol[i], h$symbol, 0)) {
        h$wt[idx] <- h$wt[idx] * (1 + ords$shares[i] / h$shares[idx])       # Scale the weight up
        h$shares[idx] <- h$shares[idx] + ords$shares[i]
    } else {
        ha <- ords[i, intersect(names(h), names(ords))]
        ha$wt <- ha$shares * ords[i,"cls"] * ha$multiplier / tna
        for (j in setdiff(names(h), names(ha))) ha[[j]] <- NA
        h <- rbind(h, ha)
    }
    h
}
add.risk <- function(h, sigtyp) {                 # Risk characteristics, as in gma.risk.report.R
    idx <- match(h$underlying, rownames(cov.mat))
    cmat <- cov.mat[idx, idx, drop=FALSE]
    stopifnot(all.equal(rownames(cmat), h$underlying))
    sigmaP <- sqrt(h$wt %*% cmat %*% h$wt)                              # Daily sdev of portfolio

    h$signal <- signal[[sigtyp]][match(h$underlying, names(signal[[sigtyp]]))]
    h$var.1d <- 2 * sqrt(diag(cmat)) * abs(h$wt)                                    # 2 sigma |w|
    h$vol.c  <- sqrt(252) / sigmaP * drop(cmat %*% h$wt) * h$wt                     # dSig/dw * w
    h$dtr    <- g.date.idx(g.read.date(h$roll.date), later=FALSE) - g.date.idx()
    if (sigtyp=="curncy") {
        h$ivol   <- ivol$ivol[match(h$underlying, ivol$curncy)]
        h$ivol.p <- with(h, ivol * wt)
    }
    h
}
bbrt <- function(nitm, citm, ids) {
    bb <- getBbRealTime(c(nitm,citm), ids, pod=pod)
    suppressWarnings(bb <- data.frame(bb))          # E.g. no "some row.names duplicated" warning
    suppressWarnings(for (i in nitm) bb[ ,i] <- as.double(bb[ ,i]))
    bb
}
add.totals <- function(tbl, adds=c("gl","bp","wt","mgl","vol.c","delwt")) {
    tt <- data.frame(symbol="Total")
    for (j in intersect(adds, names(tbl))) tt[[j]] <- sum(tbl[[j]], na.rm=T)
    for (j in setdiff(names(tbl), names(tt))) tt[[j]] <- if (is.character(tbl[[j]])) "" else NA
    rbind(tbl, tt)
}
mkcols <- function(tbl) {
    reds <- c("gl","bp","wt","shares","mgl","vol.c","ivol.p","delwt")
    bkg   <- matrix("transparent", nrow(tbl), ncol(tbl))
    bkg  [tbl$wt > 0, ] <- "#E8FFE8"    # Longs in green
    bkg  [tbl$wt < 0, ] <- "#FFE8E8"    # Shorts in red
    bkg  [nrow(bkg),  ] <- "#FFFFCC"    # Total in yellow
    color <- matrix("black",       nrow(tbl), ncol(tbl))
    color[nrow(bkg), ] <- "#0000AA"
    color[tbl$symbol %in% ords$symbol, names(tbl) %in% reds] <- "red"
    list(bkg=bkg, color=color)
}

## Formats:
kn <- data.frame(items=c("bp","wt","vol.c","gl","mgl","shares","cls","ttp","upr","signal","dtr",
                   "ivol","ivol.p"),
                 typ="n", scl=c(10000, rep(100,2), rep(1,10)), dec=c(0,2,2,0,0,0,2,2,2,1,0,2,2),
                 hdr=c("Bps","Wt(%)","Vol.C(%)","G/L($)","MTD($)","Shs","Close","Price","UPrc",
                       "Signal","DTR","Ivol","Ivol.p"))
kc <- data.frame(items="multiplier", typ="c", scl=1, dec=0, hdr="Mult")
kd <- data.frame(items="roll.date",  typ="d", scl=1, dec=0, hdr="Roll")
kl <- as.list(rbind(kn, kc, kd))

## Auxiliary tables:
fx   <- db.sql("select currency, fx, multiply from geode..fx")
ctry <- db.sql("select country_name as ctry, currency as curr from geode..country")

## bbdotnet3 stuff:
now <- Sys.time()
ndt <- format(now, "%D")                                                          # Just the date
pod <- tryPod(c("DB","GL"))                            # Good to have CBOT,CEC,ICEA,ICEF,LME,NYMX
if (pod != "DB") cat("Warning: pod =", pod, ", some prices delayed.\n")

## GMA data (sod):
strats <- get.strats(mfund)
for (i in c("gma.holdings","cov.mat","gma.signal","curncy","index","hsim"))
  load(g.p("$ddir/latest/$i.RData", ddir=GMA.DATA.DIR))
gma.holdings$fund <- tolower(gma.holdings$fund)                           # "endowcur" not "CTFX"
ivol <- subset(curncy, !is.na(ivol))

## Orders in Granite:
ords <- NULL
if (relt) for (idummy in 1) {                                                  # Allows for break
    flds <- c("fund","symbol","cusip","shares","state","side","cancelled","rebill")
    states <- c(if(pret)"pre", "released","accepted","placed","xfer","done") # Not deleted,denied
    for (j in strats$fname) ords <- rbind(ords, g.orders(j, fields=flds))
    ords$shares <- ords$shares * ifelse(ords$side %in% c("B","C"), 1, -1)
    ords <- subset(ords, state %in% states & cancelled=="" & rebill=="", flds[1:5])
    ords <- subset(ords, !(nchar(symbol)==6 & substr(symbol,4,6)=="USD"))    # Ignore spot trades
    if (!nrow(ords)) {cat("No Granite orders.\n"); break}

    ## Get more data from secref:
    stxt <- paste(sQuote(ords$symbol), collapse=",")
    res <- db.sql("SELECT s.symbol, s.security_name as name, s.price as cls, s.multiplier,",
                  "        s.bb_id, s.maturity_date, x.first_notice_dt, x.last_trd_dt",
                  "  FROM geode..secref s",
                  "  LEFT JOIN xref..comdtyfut_def x on s.sec_id=x.sec_id",
                  " WHERE s.symbol in ($stxt)", Date=TRUE)
    res$roll.date <- pmin(res$last.trd.dt,
                          g.date.idx(res$maturity.date,   T, -1),
                          g.date.idx(res$first.notice.dt, T, -1), na.rm=T)
    idx <- match(ords$symbol, res$symbol)
    for (j in c("name","cls","multiplier","bb.id","roll.date")) ords[[j]] <- res[[j]][idx]
    if (any(q <- is.na(idx))) cat("Not found in secref:", ords$symbol[q], "\n")
    ords$bb.id[q] <- sub("_", " ", ords$symbol[q])               # This works for COM (e.g. CLV9)

    ## Fill in more blanks:
    otyp <- strats$strat[match(ords$fund, strats$fname)]                            # com/cur/vol
    lme  <- otyp=="com" & substr(ords$symbol, 1,2)=="LM"
    ords$underlying <- ifelse(lme,         substr(ords$symbol, 1,4),                       # LMSN
                       ifelse(otyp=="com", sub("_"," ", substr(ords$symbol, 1,2)),         # CL
                       ifelse(otyp=="cur", substr(ords$symbol, 4,6),                       # AUD
                       "SPX")))                                                            # SPX
    for (i in which(otyp=="cur" & is.na(idx))) {               # You can guess some currency data
        cur <- substr(ords$symbol[i], 4,6)
        mymat <- "20" %&% substr(ords$symbol[i], 11,12) %&% substr(ords$symbol[i], 7,10)
        ords$name      [i] <- ifelse(cur=="EUR", "Euro", ctry$ctry[match(cur, ctry$curr)])
        ords$cls       [i] <- fx$fx[match(cur, fx$currency)]
        ords$multiplier[i] <- 1
        ords$roll.date [i] <- g.date.idx(mymat, T, -1, Class="Date")
    }
    ords$bb.id <- paste(ords$bb.id, c(com="Comdty",cur="Curncy",vol="Equity")[otyp])
    if (any(q <- otyp=="vol" & substr(ords$symbol,1,2)=="ES"))
      ords$bb.id[q] <- sub("Equity","Index", ords$bb.id[q])

    ## Flip sign and denomination (USD -> LCL) of currency trades, make symbol=currency:
    if (any(q <- otyp=="cur" & substr(ords$symbol, 1,3)=="USD")) {
        ords$symbol[q] <- substr(ords$symbol[q], 4,6)
        ords$shares[q] <- -ords$shares[q] / fx$fx[match(ords$symbol[q], fx$currency)]
    }

    ## Report trades:
    fmt <- list(title="Adding Granite Orders:", n=1, klist=kl, ll="", lr="")
    itms <- c("fund","symbol","cusip","shares","state","name","cls","multiplier")
    g.rpt.html(ords[itms], fmt)
}
if (is.null(ords)) ords <- data.frame(fund="", symbol="")

#######################################  Commodity:  ############################################

tna <- strats$tna[strats$strat=="com"]
h <- subset(gma.holdings, fund==mfund %&% "com",                                         # Curve?
            c("symbol","shares","sec.name","multiplier","bb.id","roll.date","w.eq","underlying"))
h <- g.rename(h, list(sec.name="name", w.eq="wt"))
h <- add.orders(h, strats$fname[strats$strat=="com"])
h <- add.risk(h, "comdty")                        # Risk characteristics, as in gma.risk.report.R

## Bloomberg data:
nitm <- c("last_price","contract_value","px_mid","px_yest_close","px_close_mtd")
citm <- c("time","last_update_bid","last_update_ask")
bb <- bbrt(nitm, citm, h$bb.id)

## Fun with datetimes:
bb$time <- as.POSIXct(strptime(bb$time, "%m/%d/%Y %I:%M:%S %p"))            # Now they're POSIXct
tbid <- g.dflt(as.POSIXct(strptime(bb$last_update_bid, "%T"      )),
               as.POSIXct(strptime(bb$last_update_bid, "%m/%d/%Y")))
task <- g.dflt(as.POSIXct(strptime(bb$last_update_ask, "%T"      )),
               as.POSIXct(strptime(bb$last_update_ask, "%m/%d/%Y")))
bb$qtim <- pmin(tbid, task)
bb$tage <- round(as.double(difftime(now, bb$time, ,"s")))                 # Trade age, in seconds
bb$qage <- round(as.double(difftime(now, bb$qtim, ,"s")))                 # Quote age, in seconds
bb$time <- ifelse(format(bb$time,"%D")==ndt, format(bb$time,"%T"), format(bb$time,"%m/%d"))
bb$qtim <- ifelse(format(bb$qtim,"%D")==ndt, format(bb$qtim,"%T"), format(bb$qtim,"%m/%d"))

## Pick "trd" or "mid" to display:
q <- with(bb, (is.na(tage) | tage > 300) & !is.na(px_mid) & g.dflt(qage < tage, T))     # Use mid
h$ttp  <- ifelse(q, bb$px_mid, bb$last_price)
h$time <- ifelse(q, bb$qtim, bb$time)
h$src  <- ifelse(q, "mid", "trd")

## Secref / Bloomberg consistency:
mymult <- g.dflt(bb$contract_value / bb$last_price, h$multiplier)
if (any(q <- is.na(h$multiplier))) h$multiplier[q] <- mymult[q]
if (any(q <- abs(h$multiplier / mymult - 1) > .001)) cat("Bad multiplier:", h$symbol[q], "\n")

h$cls  <- bb$px_yest_close
h$mcls <- bb$px_close_mtd
h$gl   <- h$shares * h$multiplier * (h$ttp - h$cls)
h$bp   <- h$gl / tna
h$mgl  <- h$shares * h$multiplier * (h$ttp - h$mcls)
if (any(q <- is.na(h$gl))) cat("Missing Bloomberg data, can't calculate G/L:", h$symbol[q], "\n")

## Build the report (ro, fo):
ord <- switch(sorto, GL=order(-h$gl), Side=order(-sign(h$wt), h$dtr), Signal=order(-h$signal))
itms <- c("symbol","name","gl","bp","wt","shares","multiplier","cls","ttp","src","time","mgl",
          "vol.c","signal","dtr")
ro <- add.totals(h[ord, itms])
cols <- mkcols(ro)
ftna <- g.fmt(tna, scl=1e-6, dec=2, pct="M")
fo <- list(title=g.p("Commodity Carry ($mfund, tna=$ftna)"), n=1, klist=kl, ll="", lr="",
           bkg=cols$bkg, bkgh="#DDDDDD", color=cols$color, fontr=nrow(ro))

#######################################  Currency:  #############################################

tna <- strats$tna[strats$strat=="cur"]
h <- subset(gma.holdings, fund==mfund %&% "cur",
            c("symbol","shares","sec.name","multiplier","roll.date","w.eq"))
h <- g.rename(h, list(sec.name="name", w.eq="wt"))
h$symbol <- h$underlying <- substr(h$symbol, 4,6)                        # USDIDR063009.MS -> IDR
h$name   <- ifelse(h$symbol=="EUR", "Euro", ctry$ctry[match(h$symbol, ctry$curr)])
h <- add.orders(h, strats$fname[strats$strat=="cur"])
h <- add.risk(h, "curncy")                        # Risk characteristics, as in gma.risk.report.R

idx <- match(h$underlying, rownames(cov.mat))
corm <- cov2cor(cov.mat[idx, idx, drop=FALSE])
cmat <- sweep(sweep(corm, 1, h$ivol/100, "*"), 2, h$ivol/100, "*")                   # Annualized
curIvol <- 100 * sqrt(h$wt %*% cmat %*% h$wt)                    # Need this later for risk table

## Bloomberg data:
nitm <- c("last_price","px_yest_close","px_close_mtd")
citm <- c("inverse_quoted")
bb <- bbrt(nitm, citm, h$symbol %&% " Curncy")
for (i in nitm) bb[ ,i] <- ifelse(bb$inverse_quoted=="Y", bb[ ,i], 1/bb[ ,i])

h$ttp  <- bb$last_price
h$cls  <- bb$px_yest_close
h$mcls <- bb$px_close_mtd
h$gl   <- h$shares * (h$ttp - h$cls)
h$bp   <- h$gl / tna
h$mgl  <- h$shares * (h$ttp - h$mcls)

## Build the report (ru, fu):
ord <- switch(sorto, GL=order(-h$gl), Side=order(-sign(h$wt), h$dtr, h$symbol),
                     Signal=order(-h$signal))
itms <- c("symbol","name","gl","bp","wt","shares","cls","ttp","mgl","ivol","ivol.p",
          "vol.c","signal","dtr")
ru <- add.totals(h[ord, itms])
ru[nrow(ru), "ivol.p"] <- curIvol
cols <- mkcols(ru)
ftna <- g.fmt(tna, scl=1e-6, dec=2, pct="M")
fu <- list(title=g.p("Currency Carry ($mfund, tna=$ftna)"), n=1, klist=kl, ll="", lr="",
           bkg=cols$bkg, bkgh="#DDDDDD", color=cols$color, fontr=nrow(ru))

######################################  Volatility:  ############################################

tna <- strats$tna[strats$strat=="vol"]
h <- subset(gma.holdings, fund==mfund %&% "vol",
            c("symbol","shares","sec.name","multiplier","bb.id","roll.date","w.eq","underlying"))
h <- g.rename(h, list(sec.name="name", w.eq="wt"))
h <- add.orders(h, strats$fname[strats$strat=="vol"])
h$underlying[h$underlying %in% c("SPY","SPX")] <- "spx"
h$underlying[nchar(h$symbol)==4 & substr(h$symbol, 1,2) %in% c("ES","SP")] <- "spx"
## h <- add.risk(h, "curncy")                        # Risk characteristics, as in gma.risk.report.R

## Options on SPY:
ho <- subset(h, regexpr("\\.", symbol) > 0)
ho$cp     <-         substr(ho$symbol, 5,5)
ho$strike <- as.double(substr(ho$symbol, 7,8))                           # These are SPY, not SPX
nitm <- c("last_price","px_yest_close","opt_delta_mid","opt_undl_px")
bb <- bbrt(nitm, NULL, ho$bb.id)
ho[ ,c("ttp","cls","delta","upr")] <- bb
ho$wt <- ho$upr * ho$shares * ho$multiplier / tna
ho$delwt <- ho$delta * ho$wt

## Underlying, i.e. SPY holdings:
hu <- subset(h, symbol %in% "SPY")                                   # Underlying (hedge, if any)
if (!nrow(hu)) for (i in setdiff(names(ho), names(hu))) hu[[i]] <- numeric(0) else {
    nitm <- c("last_price","px_yest_close")
    bb <- bbrt(nitm, NULL, hu$bb.id)
    hu[ ,c("ttp","cls")] <- bb
    hu$upr <- hu$ttp
    hu$delta <- hu$multiplier <- 1
    hu$delwt <- hu$wt <- hu$ttp * hu$shares / tna
    for (i in setdiff(names(ho), names(hu))) hu[[i]] <- NA
}

## Index futures, e.g. ESU9:
hf <- subset(h, nchar(h$symbol)==4 & substr(h$symbol, 1,2) %in% c("ES","SP"))
if (!nrow(hf)) for (i in setdiff(names(ho), names(hf))) hf[[i]] <- numeric(0) else {
    nitm <- c("last_price","px_yest_close","px_close_2d")
    citm <- c("px_close_dt")                                    # After 5pm, today is tomorrow...
    bb <- bbrt(nitm, citm, hf$bb.id)
    q <- g.read.date(bb$px_close_dt)==g.date8()
    bb$px_yest_close[q] <- bb$px_close_2d[q]
    hf[ ,c("ttp","cls")] <- bb[ ,1:2]
    hf$upr <- hf$ttp
    hf$delta <- 1
    ## hf$wt <- hf$ttp * hf$shares * hf$multiplier / tna
    hf$delwt <- hf$delta * hf$wt
    for (i in setdiff(names(ho), names(hf))) hf[[i]] <- NA
}

h <- rbind(ho, hu, hf)
h$gl  <- h$shares * h$multiplier * (h$ttp - h$cls)
h$bp  <- h$gl / tna
h$dtr <- g.date.idx(g.read.date(h$roll.date), later=FALSE) - g.date.idx()

## Get index history:
ts <- subset(index, index %in% h$underlying)
mr.prc <- ts[rev(!duplicated(rev(ts$index))), ]            # Most recent close for each index
ts$rtn <- ifelse(duplicated(ts$index), ts$close / g.lag(ts$close) - 1, NA)    # 1-day returns
index.vol <- tapply(ts$rtn, ts$index, var, na.rm=TRUE)
h$u.var   <- index.vol[match(h$underlying, names(index.vol))]
h$vol.c   <- sqrt(252) * h$delwt * sqrt(h$u.var) * sign(sum(h$delwt))    # Force sum positive

## Build the report (rv, fv):
itms <- c("symbol","bb.id","wt","gl","bp","shares","cls","ttp","multiplier","delta","delwt",
          "upr","vol.c","dtr")
rv <- add.totals(h[ , itms])
cols <- mkcols(rv)
ftna <- g.fmt(tna, scl=1e-6, dec=2, pct="M")
fv <- list(title=g.p("Volatility Trade ($mfund, tna=$ftna)"), n=1, klist=kl, ll="", lr="",
           bkg=cols$bkg, bkgh="#DDDDDD", color=cols$color, fontr=nrow(rv))

######################################## Risk Table: ############################################

strts <- strats$strat
rr <- data.frame(g.mk.array(c("ann.vol","ann.vol.cont","ivol"), c("com","cur","vol","all")))
cmat <- cov(hsim[as.double(rownames(hsim)) >= g.date.idx( , T, -21), ], use="p")      # One month
sigmaP <- sqrt(strats$wt %*% cmat %*% strats$wt)
rr["ann.vol",      strts] <- x <- 100*sqrt(252) * sqrt(diag(cmat))
rr["ann.vol",      "all"] <- sum(x * strats$wt)                                 # Wtd Avg, per HM
rr["ann.vol.cont", strts] <- 100*sqrt(252) / sigmaP * drop(cmat %*% strats$wt) * strats$wt
rr["ann.vol.cont", "all"] <- 100*sqrt(252) * sigmaP
rr["ivol",         "cur"] <- curIvol                   # Was calculated in the "Currency" section
klr <- as.list(data.frame(items=c("com","cur","vol","all"), typ="n", scl=1, dec=2,
                          hdr=c("Com","Cur","Vol","All")))
fr <- list(title="1M Strategy Risk (Only ivol includes trades)", n=1, klist=klr, ll="", lr="")

########################################  Output:  ##############################################

g.rpt.html(list(ro,ru,rv,rr), list(fo,fu,fv,fr))
cat("<H3 ALIGN=RIGHT>Last update:", date(), "</H3>")
q()
EOF
close(SCRIPT);			           # End R script
system("/gcm/gcm_sys2home/gcmweb/local/bin/R --vanilla --slave < $script 2>&1");
$? && die "Aborted in R with errcode $?";   # Check for successful R completion
unlink $script;
