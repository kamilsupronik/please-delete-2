#!/usr/bin/perl
$| = 1;				                     # Don't buffer Perl output
print "Content-type: text/html\n\n<PRE>\n";

chdir "/gcm/gcm_sys2home/gcmweb/apache/htdocs/DB/Output";
require("/gcm/gcm_sys2home/gcmweb/.stuff.pl");                                # Linux environment
$script = "/tmp/rwrap$$.R";
open(SCRIPT, ">$script");
print SCRIPT <<'EOF';                                                            # Begin R script

.<-suppressMessages({library(sand); library(gma)})
g.printer(file="ytdHeatMap.pdf")
gma.heatmap(perds="ytd", plot.it=TRUE)
.<-dev.off()

EOF
close(SCRIPT);			           # End R script
system("/gcm/gcm_sys2home/gcmweb/local/bin/R --vanilla --slave < $script 2>&1");
$? && die "Aborted in R with errcode $?";   # Check for successful R completion
unlink $script;
print "<META HTTP-EQUIV=Refresh CONTENT='0; URL=../Output/ytdHeatMap.pdf'>\n";
