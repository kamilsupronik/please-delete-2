#!/usr/bin/perl
$| = 1;				                     # Don't buffer Perl output
print "Content-type: text/html\n\n<PRE>\n";
foreach $pair (split(/&/, <>)) {
  ($key, $val) = split(/=/, $pair, 2);
  $in{$key} .= $val;
}
if (crypt($in{pwd},"db") ne "db/HmZh8z2lsg") {print "Wrong password.\n"; exit};

require("/gcm/gcm_sys2home/gcmweb/.stuff.pl");                                # Linux environment
$script = "/tmp/rwrap$$.R";
open(SCRIPT, ">$script");

print SCRIPT <<EOF;
  fund <- "$in{fund}"
  tick <- toupper("$in{tick}")
  inst <- "$in{inst}"
EOF

print SCRIPT <<'EOF';                                                            # Begin R script
.<-suppressMessages(require(sand))
options(stringsAsFactors=FALSE, warn=1, width=150)

cat(fund, tick, inst, "\n")
sids <- pgSql("select sandid from sand_idx where fund='$fund' and port not like 'JUNK%'")[[1]]
stxt <- g.comma.list(sids, T, F)
wcls <- g.p("trade_date='$today' and state='crd' and sandid in $stxt and symbol='$tick'",
            " and fill is NULL", today=g.date8())
y <- pgSql("select * from sand_trd_queue where $wcls")
if (!nrow(y)) {cat("No trades found.\n"); q()} else cat("Old inst:", y$instructions, "\n\n")
.<-pgSql("update sand_trd_queue set instructions='$inst' where $wcls")
pgSql("select * from sand_trd_queue where $wcls")

EOF
close(SCRIPT);			           # End R script
system("/gcm/gcm_sys2home/gcmweb/local/bin/R --vanilla --slave < $script 2>&1");
$? && die "Aborted in R with errcode $?";   # Check for successful R completion
unlink $script;
