#!/usr/bin/perl
$| = 1;				                     # Don't buffer Perl output
print "Content-type: text/html\n\n<PRE>\n";
foreach $pair (split(/&/, <>)) {
  ($key, $val) = split(/=/, $pair, 2);
  $in{$key} .= $val;
}
if (crypt($in{pwd},"db") ne "db/HmZh8z2lsg") {print "Wrong password.\n"; exit};

require("/gcm/gcm_sys2home/gcmweb/.stuff.pl");                                # Linux environment
$script = "/tmp/rwrap$$.R";
open(SCRIPT, ">$script");
print SCRIPT <<EOF;
  idc1 <- $in{idc1}
  idc2 <- $in{idc2}
  idp1 <- $in{idp1}
  idp2 <- $in{idp2}
  sprc <- $in{sprc}
EOF

open(SCRIPT, ">>$script");
print SCRIPT <<'EOF';                                                            # Begin R script
.<-suppressMessages(library(sand))
options(width=200)
## yest <- g.date.idx( ,T,-1)                   # If you are filling in expirations for yesterday

for (port in c("MPEQVOL","GMEQVOL","UCEQVOL")) {
    h <- subset(sandHoldings(port), sec_id %in% c(idc1,idc2,idp1,idp2))            # (port, yest)
    if (nrow(h) == 0) next
    if (nrow(h) %ni% 2:4) stop("Holdings failure")
    idx <- substr(h$symbol[1], 1,3)
    idx <- switch(idx, WSX="SX5", idx)
    mult <- switch(idx, SPX=100, NKY=1000, HSI=50, SX5=,UKX=,AS5=,SMI=10)
    y <- data.frame(sec_id=h$sec_id, symbol=h$symbol, shares=-1*h$shares, fill=-1*h$shares,
                    ave_fill_price=0, curncy=h$curncy, commission=0, fees=0,
                    state="is", instructions="CLOSE", comment="Expired")
    ## y$symbol <- sub(".old$", "", y$symbol)
    ## y$sec_type <- "O";  y$sub_type <- "INDEXOPT"
    for (i in 1:nrow(y)) {
        K <- as.double(strsplit(h$symbol[i], "-")[[1]][2])
        prc <- if (substr(g.parse(h$symbol[i])$ext, 1,1)=="C") sprc - K else K - sprc
        if (prc > 0) y$ave_fill_price[i] <- mult * prc
    }
    ## Compare with g.orders Ops entries for options with value (but no multiplier there).
    bid <- sandTrd(y, port)                                       # bid <- sandTrd(y, port, yest)
    cat(port, "\n"); print(y)
}
## sandUpdateEod(c(145,428,528), yest, updateTrades=F)
## sandUpdateSod(c("GC5VOL","FARGG","GMAP"))

EOF
close(SCRIPT);			           # End R script
system("/gcm/gcm_sys2home/gcmweb/local/bin/R --vanilla --slave < $script 2>&1");
$? && die "Aborted in R with errcode $?";   # Check for successful R completion
unlink $script;
