#!/usr/bin/perl
$| = 1;				                     # Don't buffer Perl output
print "Content-type: text/html\n\n<PRE>\n";

require("/gcm/gcm_sys2home/gcmweb/.stuff.pl");                                # Linux environment
$script = "/tmp/rwrap$$.R";
open(SCRIPT, ">$script");
print SCRIPT <<'EOF';                                                            # Begin R script

.<-suppressMessages({library(sand); library(bbdotnet3)})
tna <- sandTna("GM")
sids <- pgSql("select sandid from sand_idx where fund='GMAP'")[[1]]
h <- sandHoldings(sids, type="o")
h <- subset(h, !grepl(".old", h$symbol))
rt <- g.rtpr(sort(unique(h$sec_id)), items="tret", pod="DB,GL,MAX,SH")

assets <- c("Tot","BD","CM","EQ","FX")
premia <- c("Tot","VAL","VOL","CAR","MOM","LIQ","DEF")
ap <- g.mk.array(premia, assets, value=0)
hl <- list()
for (ass in assets) for (prem in premia) {
    strat <- sub("_+$","", gsub("_Tot","_", g.p("GM_${ass}_$prem")))
    h <- try(sandHoldings(strat, type="o"), silent=T)     # Include overnight Asian option trades
    if (inherits(h, "try-error") || !nrow(h)) next
    h$tret <- rt$tret[match(h$sec_id, rt$sec.id)]
    h$pnl.wt <- Z(h$mkt_val_usd * h$tret) / tna                              # Contribution to MP
    ap[prem, ass] <- sum(h$pnl.wt)
    hl[[strat]] <- h[order(-h$pnl.wt), c("symbol","shares","w_eq","tret","pnl.wt")]
}

## Heatmap:
y <- data.frame(prem=premia); for (ass in assets) y[[ass]] <- ap[ ,ass]
kl <- list(items=assets, typ=rep("n",5), dec=rep(0,5), scl=rep(1e4,5), hdr=assets)
v <- 0:50/50
plt <- c(hsv(1, 1-v, 1), hsv(1/3, v, 1)[-1], "#E0E0E0")         # 101 colors red to green, + gray
bkg <- matrix("transparent", nrow(y), ncol(y))
for (i in 1:nrow(y)) for (j in 2:ncol(y))
  bkg[i,j] <- plt[g.dflt(g.bound(round((y[i,j] + .01) * 5000 + 1), 1, 101), 102)]
fmt <- list(title="GM Intraday", nscr=c(1,rep(0,5)), ll="", lr="", klist=kl, bkg=bkg)
g.rpt.html(y, fmt)

## Top 10/Bottom 10:
kl <- list(items=c("symbol","shares","w_eq","tret","pnl.wt"),
           typ=c("c",rep("n",4)), scl=c(1,1,100,100,1e4), dec=c(0,0,2,2,2),
           hdr=c("Symbol","Shares","Wt(%)","Rtn(%)","Contr(bp)"))
for (strat in names(hl)) {
    y <- hl[[strat]]
    nscr <- rep(0,nrow(y));  nscr[which(y$pnl.wt < 0)[1] - 1] <- 1
    if (nrow(y) > 20) {
        ym <- data.frame(symbol="Other", shares=NA, w_eq=NA, tret=NA,
                         pnl.wt=sum(y$pnl.wt[11:(nrow(y)-10)]))
        y <- rbind(y[1:10, ], ym, y[nrow(y)+1-10:1, ])
        nscr <- c(rep(0,9), 1, 1, rep(0,10))
    }
    g.rpt.html(y, list(title=strat, n=1, nscr=nscr, ll="", lr="", klist=kl))
}

EOF
close(SCRIPT);			           # End R script
system("/gcm/gcm_sys2home/gcmweb/local/bin/R --vanilla --slave < $script 2>&1");
$? && die "Aborted in R with errcode $?";   # Check for successful R completion
unlink $script;
