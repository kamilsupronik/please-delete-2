#!/usr/bin/perl

# "endt" is the last (calendar) month end:
($mo, $yr) = (localtime)[4,5];
$yr += 1900;
if ($mo==0) {$mo=12; $yr--}	       # $mo is already (month-1) by convention
$da = (31,28,31,30,31,30,31,31,30,31,30,31)[$mo-1];
if ($mo==2 && ($yr % 4)==0) {$da = 29}
foreach $i ($mo,$da) {if ($i < 10) {$i = "0$i"}}
$endt = "$yr$mo$da";

print "Content-type: text/html\n\n";
print <<EOF;
  <TITLE>Performance Stats</TITLE>
  <A HREF=/><IMG align=right border=0 src=/logo.gif></A>
  <H1 ALIGN="CENTER">Performance Stats</H1>
  <form method="POST" action="bobePerf2.cgi">
  "Fund" can be any fund or index in the gcm_performance..gcm_dperf table (GC5VOL, CMDACPLT,
    SP500), any Sand portfolio or strategy in the sand_idx table (VOL, PLT_BNCH), any
    MP backtest in the gma..allr table (fx_mom, eq_val), or any swap in the
    hal/data/mktData object.<P>
  Password:  <input type=password name=pwd size=3> &nbsp
  Fund:      <input name=fund size=12> &nbsp
  End:       <input name=endt size=8 value='$endt'> &nbsp &nbsp
  <input type=submit value="Go!">
  </form>
  <HR> <B>"Fund" Examples:</B>
  <BR><B>Funds:</B>
    GC5COM, GC5VOL, MANFUT, HEUTF, GC2HAL, GDMN, GDF1, UPREFM, FBOPCA, FBOPHA, FBVPCA, FBVPHA,
      FBVPOZ 
  <BR><B>Composites:</B>
    GUC, ALT_RP_G, ALT_GDA_G
  <BR><B>Indexes:</B>
    SP500, R2000, R1000G, R1000V, ACWIXUS, BAML3MTBILL, CBOEPUT, BCOMTR, FTPFDNAREIT, LAG, MXWO,
      TBILL
  <BR><B>GMA Sand Portfolios:</B>
    COM, CUR_G10, VOL, CMDTY_STR, FI_STR, DIV_FUT, MF_BD, MF_CM, MF_EQ, MF_FX, CAR_BD, CAR_CM,
      CAR_EQ, CAR_FX
  <BR><B>MP Sand Strategies:</B>
    MP,  MP_BD, MP_CM, MP_EQ, MP_FX,  MP__VAL, MP__VOL, MP__CAR, MP__MOM, MP__LIQ,<br>
    MP_BD_VAL, MP_CM_VAL,            MP_FX_VAL
    MP_BD_VOL, MP_CM_VOL, MP_EQ_VOL, MP_FX_VOL
    MP_BD_CAR, MP_CM_CAR, MP_EQ_CAR, MP_FX_CAR
    MP_BD_MOM, MP_CM_MOM, MP_EQ_MOM, MP_FX_MOM
               MP_CM_LIQ
  <BR><B>Other Sand Portfolios:</B>
    GSCOM_BNCH, PLT_BNCH, CEN_BNCH, GSCOM_O_B, GSCOM_C_PC, GSCOM_PC_B
  <BR><B>Other Sand Strategies:</B>
    GC5COM_F, GC5VOL_F, MANFUT_F, GSCOM_F, CMDACPLT_F, CMCENCFC_F, ALT, GSCOM_OINT
  <BR><B>Backtests:</B>
    fx_mom, cm_mom, eq_mom, bd_mom, fx_car, cm_car, bd_car, dv_car, cd_car, fx_vol, cm_vol,
    eq_vol, bd_vol, fx_val, cm_val, eq_val, bd_val, cm_liq, bd_liq
  <HR><EM> David E. Brahm
  (<A HREF="mailto:David.Brahm\@geodecapital.com"> David.Brahm\@geodecapital.com </A>)
  &nbsp Last update 7/26/18
EOF
