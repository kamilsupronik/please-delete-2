## Running simulation of currency and commodity trades.
.<-suppressMessages({library(gma); library(sand); library(chalk)})
options(stringsAsFactors=FALSE, warn=1)
if (!((today <- g.date8()) %in% dates.usd)) q()

stdt <- 20151231                             # Can start COM and CUR at 19871231, VOL at 20001110
endt <- g.date.idx( ,T,-1)                                        # GC5 actually started 20080530
calc.ls <- FALSE

if (FALSE) {   # For MIMC: run through the code below, then print last month's simulation returns
    qd <- dts %/% 100 == g.date.idx( ,T,-1,cal="mnth.end") %/% 100
    cat(" COM:", g.fmt(prod(1 + Z(y1$r.mix  + y1$rfd)[qd]) - 1, scl=100, dec=2, pct="%"), "\n",
         "CUR:", g.fmt(prod(1 + Z(y2$r.mix  + y2$rfd)[qd]) - 1, scl=100, dec=2, pct="%"), "\n",
         "VOL:", g.fmt(prod(1 + Z(y3$r.vlty + y3$rfd)[qd]) - 1, scl=100, dec=2, pct="%"), "\n")
    for (i in grep("^r\\.", names(y2), value=T))
      cat(i, round(1e4 * (prod(1 + Z(y2[[i]] + y2$rfd)[qd]) - 1)), "\n")
}

dts <- dates.usd[g.inorder(stdt, dates.usd, endt)]
Nd  <- length(dts)
rf  <- db.sql("select * from gma..riskfree where date >= '$stdt'")   # USGG1M good after 20010806
rfd <- g.nafill(g.dflt(as.list(rf[2:3]))[match(dts, rf$date)] / (100 * 252))    # Risk Free Daily
if (any(is.na(rfd))) stop("Missing rfd values")

###################################  Commodity:  ################################################
## At least 16 commodity prices available starting 19881206.

thrs <- .02                                                        # Signal threshold for trading
mystdt <- g.date.idx(stdt, T, -83)                      # Go back 3 months + 20 days for momentum
cmdtys <- c("CL","XB","HO","NG","HG","S","BO","SM","SB","CC","KC","C","W","LC","CT","LH",
            "CO","FC","QS","KW","LA","LL","LN","LX","LT")
swts <- c(inv=1, mom=0)
tcst <- rcst <- .00075

txt <- paste(sQuote(c(cmdtys,"HU")), collapse=",")
y <- db.sql("SELECT * FROM gma..cmdty WHERE cmdty in ($txt) AND cnum in (1,2)",
            " and date between '$mystdt' and '$endt' order by date, cmdty, cnum")
y$dprc <- Z(y$dprc)
y$expm <- 12 * as.double(substr(y$expiry, 5,6)) + match(substr(y$expiry, 1,3), toupper(month.abb))
y$expm <- y$expm - ifelse(y$expm > 400, 1200, 0)                              # Dec 99: 1200 -> 0
y <- subset(y, !((cmdty=="HU" & date > 20051031) | (cmdty=="XB" & date <= 20051031)))  # HU -> XB
y$cmdty[y$cmdty=="HU"] <- "XB"              # Unleaded gas was HU, became XB with JAN'06 contract

z <- g.ragged.to.array(y, c("date","cmdty","cnum"), c("price","dprc","expm")) # date/cmdty/n/item
z[,,,"price"] <- g.dflt(z[,,,"price"], g.lag(z[,,,"price"]), g.lag(z[,,,"price"],2))   # London
z[,,,"expm" ] <- g.dflt(z[,,,"expm" ], g.lag(z[,,,"expm" ]), g.lag(z[,,,"expm" ],2))   # holidays
z[,,,"dprc" ] <- g.dflt(z[,,,"dprc" ], z[,,,"price"] - g.lag(z[,,,"price"]))
## z <- g.dflt(z, g.lag(z), g.lag(z,2))              # Fill London holidays and gaps up to 2 days
m <- apply(Z(z[ , , ,"dprc"]), 2:3, cumsum)                                 # Cumulate the dprc's
m[is.na(z[ , , ,"dprc"])] <- NA
z <- abind(z, adjprc=sweep(m, 2:3, Z(m[nrow(m), , ] - z[nrow(z), , ,"price"])))       # Normalize
z <- z[rownames(z) %in% dates.usd, cmdtys, , ]
stopifnot(all(dts %in% rownames(z)))
if (any(is.na(z[as.double(rownames(z)) > 20001229, , ,"price"]))) stop("Missing commodity prices")
xt <- N(z[ , ,2,"expm"] - z[ , ,1,"expm"])       # Actual # months between contracts, from 1 to 5

## Calculate daily returns:
wo <- g.mk.array(cmdtys, c(names(swts),"mix"), data=0)                            # "old" weights
y1 <- data.frame(date=dts, n=0, levg=.5, rfd=rfd)
for (strat in colnames(wo)) y1[["r." %&% strat]] <- 0+NA
if (calc.ls) y1$r.shrt <- y1$r.long <- 0+NA
wp1 <- rr1 <- g.mk.array(dts, cmdtys)           # Save the prior weights and returns for analysis

for (idt in 1 : (Nd-1)) {
    if (interactive() && idt %% 500 == 0) cat(dts[idt], "\n")
    i <- match(dts[idt], rownames(z))
    res <- gma.comsig(z, xt, i, woInv=wo[ ,"inv"], thrs)                      # prop=T, swts=swts
    y1$n[idt] <- res$n
    wn <- cbind(res$w, mix=res$wmix)                                              # "new" weights
    y1$levg[idt] <- 0.5
    for (strat in colnames(wn)) {
        rc <- sum(wn[ ,strat] * (z[i+1, ,2,"adjprc"] - z[i, ,2,"adjprc"]) / z[i, ,2,"price"],
                  na.rm=T)
        thru <- sum(abs(wn[ ,strat]-wo[ ,strat]), na.rm=T)           # Transaction and roll costs
        roll <- wo[ ,strat] * wn[ ,strat] > 0 & z[i, , 2, "expm"] != z[i-1, , 2, "expm"]
        rr <- if (idt==1) 0 else  -tcst * thru - rcst * sum(abs(wn[ ,strat]) * roll, na.rm=T)
        y1[["r." %&% strat]][idt+1] <- rc + rr
    }
    if (calc.ls) {                                                          # Long/short analysis
        wl <- pmax(wn[ ,"mix"], 0);  ws <- pmin(wn[ ,"mix"], 0)
        rcl <- sum(wl * (z[i+1, ,2,"adjprc"] - z[i, ,2,"adjprc"]) / z[i, ,2,"price"], na.rm=T)
        rcs <- sum(ws * (z[i+1, ,2,"adjprc"] - z[i, ,2,"adjprc"]) / z[i, ,2,"price"], na.rm=T)

        ql <- wn[ ,"mix"] > 0 | (wn[ ,"mix"]==0 & wo[ ,"mix"] > 0)
        qs <- wn[ ,"mix"] < 0 | (wn[ ,"mix"]==0 & wo[ ,"mix"] < 0)
        thrul <- sum(abs(wn[ql,strat]-wo[ql,strat]), na.rm=T)
        thrus <- sum(abs(wn[qs,strat]-wo[qs,strat]), na.rm=T)
        rolll <- wo[ql,strat] * wn[ql,strat] > 0 & z[i,ql, 2, "expm"] != z[i-1,ql, 2, "expm"]
        rolls <- wo[qs,strat] * wn[qs,strat] > 0 & z[i,qs, 2, "expm"] != z[i-1,qs, 2, "expm"]
        rrl <- if (idt==1) 0 else -tcst * thrul - rcst * sum(abs(wn[ql,strat]) * rolll, na.rm=T)
        rrs <- if (idt==1) 0 else -tcst * thrus - rcst * sum(abs(wn[qs,strat]) * rolls, na.rm=T)
        y1$r.long[idt+1] <- rcl + rrl
        y1$r.shrt[idt+1] <- rcs + rrs
    }
    wp1[idt+1, ] <- wn[ ,"mix"]
    rr1[idt+1, ] <- (z[i+1, ,2,"adjprc"] - z[i, ,2,"adjprc"]) / z[i, ,2,"price"]
    wo <- wn                                                         # "new" weights become "old"
}

yt <- sandDperf("COM", stdt, endt, date8=T)         # g.fund.perf("CTCOM", stdt, endt, usDates=T)
yt$dret[yt$date==stdt] <- 0
## stopifnot(all.equal(yt$date, y1$date))
y1$r.true <- Z(yt$dret[match(y1$date, yt$date)] - y1$rfd)

####################################  Currency:  ################################################

mystdt <- g.date.idx(stdt, T, -253)                           # Go back a year corrs and momentum
curs <- G10                                                             # G10 is defined in AAA.R
tcst <- .0002

txt <- paste(sQuote(curs), collapse=",")
y <- db.sql("SELECT date, curncy, fx, i1, i2, ivol, ppp, yld, yld10, gap FROM gma..curncy",
            " WHERE curncy in ($txt) AND date between '$mystdt' and '$endt'")
z <- g.ragged.to.array(y, c("date","curncy"))                              # date x curncy x item
for (j in curs) z[ ,j,"i1"] <- g.dflt(z[ ,j,"i1"], z[ ,j,"i2"])     # Fill missing "i1" with "i2"
z <- z[rownames(z) %in% dates.usd, curs, c("fx","i1","ivol","ppp","yld","yld10","gap")]

## Invert some fx rates:
info <- db.sql("select * from gma..curncy_info")
invert <- as.logical(info$invert[match(curs, info$curncy)])
for (j in which(invert)) z[ ,j,"fx"] <- 1 / z[ ,j,"fx"]

## Fill in missing data, including i2 -> i1, and set dates.  Data ok back to stdt=19871231.
if ("USD" %in% curs) z[ ,"USD","ivol"] <- 0
for (j in curs) for (k in c("fx","i1","ivol")) z[,j,k] <- g.nafill(z[,j,k], inside=T)
for (j in curs) for (k in c("yld","yld10"))    z[,j,k] <- g.nafill(z[,j,k], inside=T, maxfill=3)
## if (today < 20130420) z[ ,"NZD","yld"] <- g.nafill(z[ ,"NZD","yld"])                # Temp fix

ztst <- z[g.dflt(match(19990208,rownames(z)), 1):nrow(z), , ]    # Data should be good since then
ztst[ ,"NOK","yld"] <- g.dflt(ztst[ ,"NOK","yld"],  ztst[ ,"NOK","yld10"] - PR["NOK"])
if (any(q <- is.na(ztst[ , ,c("fx","i1","ivol","ppp","yld","gap")]))) {
    msg <- matrix(sapply(1:3, function(i) dimnames(q)[[i]][which(q, TRUE)[,i]]), ncol=3)
    stop("Missing curr data: ", paste(apply(msg, 1, paste, collapse=":"), collapse=", "))
}

## Calculate daily returns from currency fluctuations (rc) and interest (ri):
wo <- g.mk.array(curs, c("car","val","yld","mom","grt","trm","mix"), data=0)      # "old" weights
y2 <- data.frame(date=dts, rfd=rfd, levg=1)
for (strat in colnames(wo)) y2[["r." %&% strat]] <- 0+NA
wp2 <- rr2 <- g.mk.array(dts, curs)             # Save the prior weights and returns for analysis

for (idt in 1 : (Nd-1)) {
    if (interactive() && idt %% 500 == 0) cat(dts[idt], "\n")
    i <- match(dts[idt], rownames(z))
    vols <- z[i, ,"ivol"]
    r <- z[i-(252:1), ,"fx"] / z[i-(252:1)-1, ,"fx"] - 1
    suppressWarnings(corm <- cor(r, use="p"))
    if ("USD" %in% curs) corm["USD", ] <- corm[ ,"USD"] <- 0;  corm["USD","USD"] <- 1
    cmat <- sweep(sweep(corm, 1, vols/100, "*"), 2, vols/100, "*")            # Covariance matrix
    if (any(is.na(cmat))) {cmat <- NULL;  if (dts[idt] > 19990208) cat(dts[idt], "No cmat\n")}
    res <- gma.cursig(z, i, woYld=wo[ ,"yld"], nl=3, ns=3, cmat=cmat,
                      swts=c(car=.2, val=.17, yld=.2, mom=.17, grt=.2, trm=.06))       # Old swts
    wn <- cbind(res$w, mix=res$wmix)                                              # "new" weights
    y2$levg[idt] <- res$levg
    for (strat in colnames(wn)) {
        rc <- sum(wn[ ,strat] * (z[i+1, ,"fx"]/z[i, ,"fx"] - 1), na.rm=T)           # FX fwd rtns
        ri <- sum(wn[ ,strat] * z[i, , "i1"]/100, na.rm=T) / 252                    # Int returns
        y2[["r." %&% strat]][idt+1] <- rc + ri - tcst * sum(abs(wn[,strat]-wo[,strat]), na.rm=T)
    }
    wp2[idt+1, ] <- wn[ , "mix"]
    rr2[idt+1, ] <- z[i+1, ,"fx"]/z[i, ,"fx"] - 1 + z[i, , "i1"]/25200
    wo <- wn                                                         # "new" weights become "old"
}                                                                                # End "idt" loop
## See Projects/Gma/CurrStrats/strats2.R for plotting code

yt <- sandDperf("CUR", stdt, endt, date8=T)          # g.fund.perf("CTFX", stdt, endt, usDates=T)
yt$dret[yt$date==stdt] <- 0
## stopifnot(all.equal(yt$date, y2$date))
y2$r.true <- Z(yt$dret[match(y2$date, yt$date)] - y2$rfd)

###################################  Volatility:  ###############################################

y3 <- data.frame(date=dts, rfd=rfd, r.vlty=0, upos=0, delta=0)
x <- db.sql("SELECT t_date, price_close FROM ivolatility..stockprice",
            " WHERE stock_id='9327' and t_date >= '$stdt'")
y3$upr <- x$price.close[match(y3$date, x$t.date)]

cdate <- stdt
while (cdate < endt) {                                                 # Loop over creation dates
    idt <- match(cdate, y3$date)
    upr.c <- y3$upr[idt]                                        # Index price on creation date
    opt <- db.sql("SELECT exp_date, call_put, strike, ticker, bid, price, iv, delta",
                  "  FROM ivolatility..optionvalue",
                  " WHERE stock_id='9327' and t_date='$cdate' and price > 0 and iv > 0",
                  " ORDER BY exp_date, call_put, strike")
    if (!nrow(opt) || any(is.na(opt$price)) || !all(opt$call.put %in% c("C","P"))) stop("BadOpt")

    ## Restrict to those expiring next month:
    ## expiry <- min(opt$exp.date[opt$exp.date > g.date.idx(cdate, T, +15)]) # expiry is Saturday
    mth1 <- g.date.idx(cdate, T, cal="cal.mnth.beg")                # First day of the next month
    dow <- as.double(format(g.Date(mth1), "%w"))                 # Day of the week (0=Sun, 6=Sat)
    expiry <- g.date.idx(mth1, T, 20 - dow + 7*(dow==6), cal="cal")       # Sat following 3rd Fri
    sdate <- g.date.idx(expiry-7, T, later=F)    # Friday a week before expiry (or Thu if Fri=hd)
    prior <- g.date.idx(expiry, T, later=F)
    if (expiry %ni% opt$exp.date && prior %in% opt$exp.date) expiry <- prior

    opt <- subset(opt, exp.date==expiry)
    opt.c <- subset(opt, call.put=="C")
    opt.p <- subset(opt, call.put=="P")
    if (!nrow(opt.c) || !nrow(opt.p)) stop("Predicted expiration date not found in iVolatility")

    ## Choose a call and a put that are out-of-the-money by "fix.val" on cdate:
    iv.atm <- mean(c(opt.c$iv[which.min(abs(opt.c$strike - upr.c))],
                     opt.p$iv[which.min(abs(opt.p$strike - upr.c))]))
    fix.val <- .5 * iv.atm / sqrt(12)                               # In vlty1.R, this is just 2%
    opt.c <- opt.c[which.min(abs(opt.c$strike - upr.c*(1+fix.val))), ]
    opt.p <- opt.p[which.min(abs(opt.p$strike - upr.c*(1-fix.val))), ]

    ## Tickers change on 5/17/10, to e.g. "SPX   100619C01180000":
    if (cdate==20100514) {opt.c$ticker <- sub("SPT", "SPX", opt.c$ticker)
                          opt.p$ticker <- sub("SPQ", "SPX", opt.p$ticker)}

    ## Sell at the "bid":
    pc.prev <- g.dflt(opt.c$bid, .9*opt.c$price);  pp.prev <- g.dflt(opt.p$bid, .9*opt.p$price)
    for (idt in (match(cdate, y3$date)+1) : g.dflt(match(sdate, y3$date), Nd) ) {
        if (is.na(y3$upr[idt])) {y3$upos[idt] <- y3$upos[idt-1];  y3$r.vlty[idt] <- 0;  next}
        dt  <- y3$date[idt]
        upr <- y3$upr [idt]

        ## Some exp.date's change: 20070616 -> 20070615, 20070720 -> 20070721, so don't use that
        str <- g.p("SELECT price, delta, ask, exp_date FROM ivolatility..optionvalue",
                   " WHERE stock_id='9327' and t_date='%dt' and price > 0 and iv > 0",
                   "   and call_put='$cp' and strike=$strk and ticker='$tick'", esc="%")
        xc <- db.sql(str, cp="C", strk=opt.c$strike, tick=opt.c$ticker)
        xp <- db.sql(str, cp="P", strk=opt.p$strike, tick=opt.p$ticker)
        if (nrow(xc) > 1) xc <- xc[1, ];  if (nrow(xp) > 1) xp <- xp[1, ]

        suc <- opt.c$strike - upr               # Fill prices if strike is far in or out of money
        sup <- opt.p$strike - upr
        if (!nrow(xc) && suc > +30) xc <- data.frame(price=0, delta=0, ask=0)
        if (!nrow(xp) && sup < -30) xp <- data.frame(price=0, delta=0, ask=0)
        if (!nrow(xc) && suc < -30) xc <- data.frame(price=-suc, delta=+1, ask=-suc)
        if (!nrow(xp) && sup > +30) xp <- data.frame(price= sup, delta=-1, ask= sup)

        if (!nrow(xc)) {                                                      # Use Black-Scholes
            xc <- g.bsprc("c", upr, opt.c$strike, t = diff(g.date.idx(c(dt, expiry))) / 252,
                          rfr = y3$rfd[idt]*252, dy=0, sigma=iv.atm)
            xc$ask <- xc$price
            cat(dt, opt.c$strike, "Call option missing, using g.bsprc\n")
        }
        if (!nrow(xp)) {
            xp <- g.bsprc("p", upr, opt.p$strike, t = diff(g.date.idx(c(dt, expiry))) / 252,
                          rfr = y3$rfd[idt]*252, dy=0, sigma=iv.atm)
            xp$ask <- xp$price
            cat(dt, opt.p$strike, "Put option missing, using g.bsprc\n")
        }
        if (!nrow(xc) || !nrow(xp)) {cat("Option missing\n"); break}

        ## Calculate P&L (on "sell" date use "ask"):
        optdel <- if (dt==sdate) 0 else -(xc$delta + xp$delta) # Because we're short both options
        y3$upos[idt] <- if (dt==sdate || abs(optdel) < .5) 0 else -optdel + .5*sign(optdel)
        y3$delta[idt] <- optdel + y3$upos[idt]
        pc.curr <- if (dt==sdate) g.dflt(xc$ask, xc$price) else xc$price
        pp.curr <- if (dt==sdate) g.dflt(xp$ask, xp$price) else xp$price
        dv <- (pc.prev - pc.curr) + (pp.prev - pp.curr) +                   # Option price change
              y3$upos[idt-1] * Z(y3$upr[idt] - y3$upr[idt-1]) -                # Delta hedge P&L
              15/100 * abs(y3$upos[idt] - y3$upos[idt-1])                   # $15/future (5+20/2)
        if (y3$date[idt-1]==cdate | dt==sdate) dv <- dv - 0.06               # $3/option contract
        y3$r.vlty[idt] <- dv / upr.c
        pc.prev <- xc$price;  pp.prev <- xp$price         # Save option prices for next day's P&L
    }                                                                              # End idt loop
    cdate <- sdate                                      # Next trade is at this trade's sell date
}                                                                        # End cdate (month) loop

yt <- sandDperf("VOL", stdt, endt, date8=T)        # g.fund.perf("PCAPSV", stdt, endt, usDates=T)
yt$dret[yt$date==stdt] <- 0
stopifnot(all.equal(yt$date, y3$date))
y3$r.true <- yt$dret - y3$rfd

########################################  Plot  #################################################

rng <- range(c(cumprod(1 + Z(y1$r.mix  + y1$rfd)), cumprod(1 + Z(y1$r.true + y1$rfd)), .90,
               cumprod(1 + Z(y1$r.inv  + y1$rfd)), cumprod(1 + Z(y1$r.mom  + y1$rfd)),
               cumprod(1 + Z(y2$r.car  + y2$rfd)), cumprod(1 + Z(y2$r.val  + y2$rfd)),
               cumprod(1 + Z(y2$r.yld  + y2$rfd)), cumprod(1 + Z(y2$r.mom  + y2$rfd)),
               cumprod(1 + Z(y2$r.grt  + y2$rfd)), cumprod(1 + Z(y2$r.trm  + y2$rfd)),
               cumprod(1 + Z(y2$r.true + y2$rfd)),
               cumprod(1 + Z(y3$r.vlty + y3$rfd)), cumprod(1 + Z(y3$r.true + y3$rfd))))
rng[1] <- rng[1] * (levmar <- (rng[1]/rng[2])^.2)                       # Leave room for Leverage

myPlot <- function(rng, titl) {
    g.plot.init(c(1,Nd), rng, log="y")
    pts  <- unique(c(1, which(!duplicated(dts %/% 100))[-1] - 1, Nd))
    if (length(dts) > 1000) pts  <- unique(c(1, which(!duplicated(dts %/% 1e4))[-1] - 1, Nd))
    vpts <- unique(pmax(axTicks(2), 1))               # seq(.90, 1.40, ifelse(quarter, .02, .05))
    if (rng[1] < .8) vpts <- axTicks(2)
    abline(h=vpts, v=pts, col=gray(.9));  abline(h=1)
    axis(1, at=pts, labels=g.fmt(dts[pts], "d"), las=2, cex.axis=.8)
    axis(2, at=vpts); box()
    title(titl %&% ", " %&% paste(g.fmt(c(stdt,endt),"d"), collapse=" - "))
}
myLeg <- function(y, spcs, place="topleft") {
    rtns <- y[-1, grep("^r\\.", names(y)), drop=F]
    atmn <- 252 * colMeans(rtns + y$rfd[-1])
    shrp <- apply(rtns, 2, function(x) sqrt(252) * mean(x)/sd(x))
    txt <- sprintf("%s%s (AnnTotRtn=%4.1f%%, Sharpe=%5.2f)",
                   sub("r\\.","", names(rtns)), spcs, 100*atmn, shrp)
    legend(place, txt, col=cols[names(rtns)], lwd=2, inset=.01, bg="white")
}
cols <- c(r.mix="gray", r.true="black", r.vlty="purple",
          r.inv="blue", r.mom="red",
          r.car="darkblue", r.val="green4", r.yld="orange", r.grt="violet", r.trm="cyan")

plotCom <- function() {
    myPlot(rng, "Commodity Carry Simulations")
    for (k in grep("r\\.", names(y1), value=T))
      lines(cumprod(1 + Z(y1[[k]] + y1$rfd)), col=cols[k], lwd=2)
    myLeg(y1, c("",""))
}
plotCur <- function() {
    myPlot(rng, "Currency Carry Simulations")
    for (k in grep("r\\.", names(y2), value=T))
      lines(cumprod(1 + Z(y2[[k]] + y2$rfd)), col=cols[k], lwd=2)
    cnvt <- function(y) .99*rng[1] + rng[1] * (1/levmar - 1) * y
    vpts <- seq(0,1,.2)
    axis(2, at=cnvt(vpts), labels=g.fmt(vpts, dec=0, scl=100, pct="%"), cex.axis=.8)
    lines(cnvt(y2$levg), lwd=2, col=cols["r.mix"])
    text(10, cnvt(1.07), "Leverage:")
    myLeg(y2, c("   ","    ","    "," ","    ","   ","","  "))
    if (FALSE) {lines(cumprod(1 + Z(y1$r.mix + y1$rfd)), col=cols["r.mix"], lwd=2)    # One plot!
                myLeg(y1, "", "topright")}
}
plotVol <- function() {
    myPlot(rng, "Volatility (Short Strangle) Simulations")
    for (k in c("r.vlty","r.true")) lines(cumprod(1 + Z(y3[[k]] + y3$rfd)), col=cols[k], lwd=2)
    myLeg(y3, c("",""))
}
plotAlt <- function() {
    idt <- c(BXIITSTP=20130711, MLBXGCM1=20130724, BCRIHGDA=20130930, MSQTFXV1=20141014,
             MLBX73CD=20010131)
    fdt <- c(BXIITSTP=99999999, MLBXGCM1=20150112, BCRIHGDA=99999999, MSQTFXV1=99999999,
             MLBX73CD=99999999)
    fee <- c(BXIITSTP=0.0065,   MLBXGCM1=0.0280,   BCRIHGDA=0.0080,   MSQTFXV1=0.0075  ,
             MLBX73CD=0.0161)
    cols <- c("orange","blue","purple","cyan","green4")                         # "red", "green4"
    load("/gcm/strategies/hal/data/mktData.RData")
    m <- data.matrix(mktData[g.date8(mktData$date) %in% dts, names(idt)])
    m <- sweep(m, 2, m[1, ], "/")
    if (nrow(m) != Nd) warning("mktData is not complete")
    for (i in colnames(m)) m[ ,i] <- m[ ,i] - cumsum(c(0, rep(Z(fee[i])/252, nrow(m)-1)))  # Fees
    yt <- db.fund.perf("MP_SWAPS", stdt, endt)                                 # True performance

    xx <- g.dflt(match(fdt["MLBXGCM1"], dts), 1) : nrow(m)                      # Tack on PREROLL
    p <- sandDperf("PREROLL", dts[xx[1]], date8=T)[c("date","dret")]
    p$dret <- p$dret + .0223/252                                  # The Sand "lie" of carry costs
    if (length(x <- setdiff(dts, p$date))) p <- rbind(p, data.frame(date=x, dret=0))  # Ends 2/14
    h <- g.readDf(chalkFile("PreRoll", NULL))                   # Patch in Chalk starting 9/15/16
    h <- h[match(p$date, h$date), ]
    if (any(q <- p$date >= 20160915)) p$dret[q] <- h$rtn[q]
    yy <- m[xx[1], "MLBXGCM1"] * cumprod(1 + Z(p$dret[match(dts[xx], p$date)]))

    arng <- range(m, yy, cumprod(1 + Z(yt$rtn)))                                  # Set new range
    myPlot(arng, "Alternative Beta")
    for (j in 1:ncol(m)) {
        ki <- g.dflt(match(idt[j], dts), 1)
        kf <- g.dflt(match(fdt[j], dts), nrow(m))
        points(ki, m[ki,j], col=cols[j], cex=2)
        if (kf != nrow(m)) points(kf, m[kf,j], col=cols[j], cex=2)
        lines(m[ ,j], col=cols[j], lwd=1)
        lines(ki:kf, m[ki:kf,j], col=cols[j], lwd=3)
    }
    lines(xx, yy, col="red", lwd=2)
    ki <- match(yt$date[1], dts)
    lines(ki:Nd, cumprod(1 + Z(yt$rtn)), lwd=2)
    legend("bottomleft", c(names(idt),"PREROLL","MP_SWAPS"), col=c(cols,"red","black"),
           ncol=3, lwd=2, bg="white", inset=.01)
}

if (g.date8()==g.date.idx( , T, cal="mnth.beg", later=F)) {
    g.printer(file="~/Projects/PerfMtg/com.sim.png", width=9, height=9)
    plotCom()
    g.printer(file="~/Projects/PerfMtg/cur.sim.png", width=9, height=9)
    plotCur()
    g.printer(file="~/Projects/PerfMtg/vol.sim.png", width=9, height=9)
    plotVol()
    g.printer(file="~/Projects/PerfMtg/alt.sim.png")
    plotAlt()
    dev.off()
}

web <- Sys.getenv("web")                            # "/gcm/gcm_sys2home/gcmweb/apache/htdocs/DB"
g.printer(file=g.p("$web/CronOut/gmasim.pdf"))
plotCom()
plotCur()
plotVol()
plotAlt()
.<-dev.off()

######################################  Output:  ################################################

## Currency performance table in email:
qmtd <- dts %/% 100 == dts[Nd] %/% 100
qytd <- dts %/% 1e4 == dts[Nd] %/% 1e4
x <- ((dts %/% 100) %% 100 - 1) %/% 3 + 1                             # Quarter of the year (1-4)
qqtd <- qytd & x == x[Nd]
nms <- grep("r\\.", names(y2), value=T)
yp <- data.frame(strat= sub("r\\.", "", nms),
                 day  = unlist(y2[Nd, nms] + y2$rfd[Nd]),
                 mtd  = apply(1 + y2[qmtd, nms] + y2$rfd[qmtd], 2, prod) - 1,
                 qtd  = apply(1 + y2[qqtd, nms] + y2$rfd[qqtd], 2, prod) - 1,
                 ytd  = apply(1 + y2[qytd, nms] + y2$rfd[qytd], 2, prod) - 1,
                 row.names=NULL)
kl <- data.frame(items=names(yp)[-1], typ="n", scl=100, dec=2);  kl$hdr <- kl$items %&% "(%)"
tc <- textConnection("txt","w")
g.rpt.txt(yp, list(n=1, klist=as.list(kl), nscr=6), tc, sep="  ", verbose=F)
close(tc)

recip <- c("David.Brahm","Eric.Matteson")
link <- "<http://lbos991.fmr.com:8080/DB/CronOut/gmasim.pdf>"
g.mail(recip, "[GMA] Currency and Commodity Carry Simulations since " %&% g.fmt(stdt,"d"),
       c(link,"", txt))

## Update SQL table "gma..histsim":
## g.query.sql(g.p("CREATE TABLE gma.dbo.histsim (date datetime PRIMARY KEY, rfd float,",
##                 " cur_car float, cur_val float, cur_yld float, cur_mom float,",
##                 " cur_grt float, cur_trm float, cur_mix float,",
##                 " com_inv float, com_mom float, com_mix float,",
##                 " vol_spx float, del_spx float, vol_ukx float, del_ukx float, vol_mix float,",
##                 " mix float)"))
mxdt <- db.sql("select max(date) as date from gma..histsim")[[1]]
yh <- subset(y2, date > mxdt,
             c("date","rfd","r.car","r.val","r.yld","r.mom","r.grt","r.trm","r.mix"))
if (nrow(yh) && !is.null(y3$upr)) {
    names(yh) <- sub("r\\.", "cur_", names(yh))
    yh$com_inv <- y1$r.inv [match(yh$date, y1$date)]
    yh$com_mom <- y1$r.mom [match(yh$date, y1$date)]
    yh$com_mix <- y1$r.mix [match(yh$date, y1$date)]
    yh$vol_spx <- y3$r.vlty[match(yh$date, y3$date)]
    yh$del_spx <- y3$delta [match(yh$date, y3$date)]
    yh$vol_ukx <- NA                    # Fix these some day
    yh$del_ukx <- NA
    yh$vol_mix <- NA
    yh$mix     <- (yh$cur_mix + yh$com_mix + yh$vol_spx)/3
    g.put.sql(yh, "gma..histsim", append=T)
}

## Store GMA history, to replace slow calls to db.fund.perf:
## g.query.sql(g.p("CREATE TABLE gma.dbo.perf (date datetime PRIMARY KEY,",
##                 " gma_rtn float, gma_tna float,",
##                 " com_rtn float, com_tna float,",
##                 " cur_rtn float, cur_tna float,",
##                 " vol_rtn float, vol_tna float,",
##                 " alt_rtn float, alt_tna float)"))
pfdt <- g.dflt(g.date.idx(db.sql("select max(date) from gma..perf")[[1]], T, +1), 19871231)
if (pfdt < today) {
    p1 <- db.fund.perf("COM", pfdt, gmaPerf=F); names(p1)[2:3] <- "com_" %&% names(p1)[2:3]
    p2 <- db.fund.perf("CUR", pfdt, gmaPerf=F); names(p2)[2:3] <- "cur_" %&% names(p2)[2:3]
    p3 <- db.fund.perf("VOL", pfdt, gmaPerf=F); names(p3)[2:3] <- "vol_" %&% names(p3)[2:3]
    p4 <- db.fund.perf("ALT", pfdt, gmaPerf=F); names(p4)[2:3] <- "alt_" %&% names(p4)[2:3]
    ## p2$cur_rtn[match(19871231, p2$date)] <- 0
    ## p4$alt_tna[p4$date < 20130620] <- 0
    yy <- cbind(p1["date"], gma_rtn=NA, gma_tna=NA, p1[2:3], p2[2:3], p3[2:3], p4[2:3])
    yy$gma_tna <- yy$com_tna + yy$cur_tna + yy$vol_tna + yy$alt_tna           # set x=0 where w=0
    yy$gma_rtn <- g.wt.sum(data.matrix(yy[c(4,6,8,10)]), data.matrix(yy[c(5,7,9,11)]))
    g.put.sql(yy, "gma..perf", append=T)
}

if (FALSE) {                                                            # Fix a date in gma..perf
    dt <- 20140926
    p1 <- db.fund.perf("COM", dt, dt, gmaPerf=F); names(p1)[2:3] <- "com_" %&% names(p1)[2:3]
    p2 <- db.fund.perf("CUR", dt, dt, gmaPerf=F); names(p2)[2:3] <- "cur_" %&% names(p2)[2:3]
    p3 <- db.fund.perf("VOL", dt, dt, gmaPerf=F); names(p3)[2:3] <- "vol_" %&% names(p3)[2:3]
    p4 <- db.fund.perf("ALT", dt, dt, gmaPerf=F); names(p4)[2:3] <- "alt_" %&% names(p4)[2:3]
    yy <- cbind(p1["date"], gma_rtn=NA, gma_tna=NA, p1[2:3], p2[2:3], p3[2:3], p4[2:3])
    yy$gma_tna <- yy$com_tna + yy$cur_tna + yy$vol_tna + yy$alt_tna           # set x=0 where w=0
    yy$gma_rtn <- g.wt.sum(data.matrix(yy[c(4,6,8,10)]), data.matrix(yy[c(5,7,9,11)]))
    g.sql("select * from gma..perf where date='$dt'")
    g.sql("update gma..perf set gma_rtn=$x1, gma_tna=$x2, com_rtn=$x3, com_tna=$x4,",
          "                     cur_rtn=$x5, cur_tna=$x6, vol_rtn=$x7, vol_tna=$x8,",
          "                     alt_rtn=$x9, alt_tna=$x0 where date='$dt'",
          x1=yy$gma_rtn, x2=yy$gma_tna, x3=yy$com_rtn, x4=yy$com_tna, x5=yy$cur_rtn,
          x6=yy$cur_tna, x7=yy$vol_rtn, x8=yy$vol_tna, x9=yy$alt_rtn, x0=yy$alt_tna)
}

if (FALSE) {                                                         # How accurate is gma..perf?
    pfdt <- 20131231
    p1 <- db.fund.perf("COM", pfdt); names(p1)[2:3] <- "com_" %&% names(p1)[2:3]
    p2 <- db.fund.perf("CUR", pfdt); names(p2)[2:3] <- "cur_" %&% names(p2)[2:3]
    p4 <- db.fund.perf("ALT", pfdt); names(p4)[2:3] <- "alt_" %&% names(p4)[2:3]
    yy <- cbind(p1["date"], fnd_rtn=NA, fnd_tna=NA, p1[2:3], p2[2:3], p4[2:3])
    yy$fnd_tna <- yy$com_tna + yy$cur_tna + yy$alt_tna
    yy$fnd_rtn <- g.wt.sum(data.matrix(yy[c(4,6,8)]), data.matrix(yy[c(5,7,9)]))
    yt <- g.fund.perf("GC5COM", pfdt, usDates=TRUE)
    rtns <- cbind(sand=yy$fnd_rtn, true=yt$rtn); rownames(rtns) <- yt$date
    g.plot.perf(rtns)
}

if (FALSE) {                                          # Interactive: Currency Positions Over Time
    g.plot.init(c(1, Nd), c(.5, 10.5))
    pts <- c(which(!duplicated(y2$date %/% 100))[-1] - 1, nrow(y2))
    abline(h=1:10, v=pts, col=gray(.9))
    axis(1, at=pts,  labels=g.fmt(y2$date[pts],"d"), las=2, cex.axis=.8)
    axis(2, at=1:10, labels=G10); box()
    for (k in 1:10) g.bars(1:nrow(wp2), wp2[ ,k], 1, k, down.col="red")
    box(); title("Currency Weights, Simulation")
    dev.off()
}
if (FALSE) {                                          # Returns history spreadsheet
    kl <- list(items=c("r.vlty","rfd"), typ=rep("n",2), scl=rep(1,2), dec=rep(6,2),
               hdr=c("Return","Risk-Free"))
    g.rpt.xls(y3[c("date","rfd","r.vlty")], list(title="GC5VOL Simulation", n=1, klist=kl),
              "gc5vol")
}
if (FALSE) {                                          # Commodity long/short returns
    ritms <- c("rfd","r.mix","r.long","r.shrt","r.true")
    kl <- list(items=ritms, typ=rep("n",5), scl=rep(1,5), dec=rep(6,5),
               hdr=c("Risk-Free","Model","Long","Short","True"))
    nscr <- which(!duplicated(y1$date[-1] %/% 100))[-1] - 1
    g.rpt.xls(y1[-1, c("date",ritms)], list(title="GC5COM Long/Short", n=1, klist=kl, nscr=nscr),
              "gc5com")
}
if (FALSE) {                                          # y5 in gmasim.RData, use stdt=20061229
    y5 <- cbind(y1[c("date","rfd","r.inv")], y2["r.mix"], y3["r.vlty"])
    save(y5, file="~/Projects/Misc5/Data/gmasim.RData")
}
